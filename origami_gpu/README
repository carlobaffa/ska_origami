Origami is the demonstration phase 1 folding program for GPU PIP for SKA
Pulsar Search Survey.

=============== Base Compilation =========================

To compile the program origami, execute

./compile_origami

This script launches configure with the next options:

 --with-cuda 
 
   this option configure the cuda library path. 
   The CUDA library path is build getting the path prefix of the nvcc 
   executable

 --with-nvml=/usr/lib/nvidia-current
   
   this option let user configure the path of the NVML library. 
   If the NVML library is in a different directory from the one specified,
   change accordingly the value.

 --with-nvcc=/usr/local/cuda/bin/nvcc
   
   this option can be added to the configure script if nvcc is not found in
   the standard $PATH.
   If the executable nvcc is installed in a different directory from the
   one specified, change accordingly the value.

To match the GPU architecture, the exact architecture must be declared in 
src/Makefile.am . The correct architecture can be found in the page
http://arnon.dk/matching-sm-architectures-arch-and-gencode-for-various-nvidia-cards/

=============== Initial run and test =====================

Once compiled origami can be tested using the same data used in
tdt-reference-implementations (fold_2_20.raw)

./origami -C6 -c256 -T 15 -w -v0 -F <path-to-tdt-reference>/fold_2.raw

Result:
------------------------

Input file: <path>//test_data/fold_2.raw
Warning: Main- Found 128 candidates in input file. Requested only 6
fold.ncand: 6 index: 0 rebin: 1 period[0]: 0.000800 candidate[0].period: 0.000401
fold.ncand: 6 index: 1 rebin: 2 period[1]: 0.001600 candidate[3].period: 0.000982
Optimizing.....                                 
------------------------------------------------------------------
                   Optimized values                               
------------------------------------------------------------------
Candidate:    0: index:  161    S/N:   3.17 width: 0.000050 period:  0.000400800  pdot: 5e-08  dm:  8.1000204
Candidate:    1: index:   77    S/N:   3.35 width: 0.000052 period:  0.000594085  pdot: 5e-08  dm:  8.1000204
Candidate:    2: index:  369    S/N:   3.99 width: 0.000229 period:  0.000789170  pdot: 0  dm: 11.4333200
Candidate:    3: index:  318    S/N:   3.18 width: 0.000155 period:  0.000982655  pdot: -5e-08  dm: 10.1000004
Candidate:    4: index:  199    S/N:  81.05 width: 0.000102 period:  0.001123400  pdot: 0  dm: 10.1000004
Candidate:    5: index:  168    S/N:   3.64 width: 0.000102 period:  0.001175640  pdot: -5e-08  dm:  8.1000204
------------------------------------------------------------------

Total exec time      :     0.63  (sec)
Total GPU exec time  :     0.38  (sec)
----------------------------

Or, for 128 candidates and a SKA standard file (4096 frequency channels and
420 seconds length):

./origami -C128 -c4096 -T420 -v0 -w -F ska_4096_420_01.dat


Input file: ska_4096_420_01.dat
fold.ncand: 128 index: 0 rebin: 1 period[0]: 0.000800 candidate[0].period: 0.000401
fold.ncand: 128 index: 1 rebin: 2 period[1]: 0.001600 candidate[3].period: 0.000982
fold.ncand: 128 index: 2 rebin: 4 period[2]: 0.003200 candidate[8].period: 0.001757
fold.ncand: 128 index: 3 rebin: 8 period[3]: 0.006400 candidate[16].period: 0.003306
fold.ncand: 128 index: 4 rebin: 16 period[4]: 0.049000 candidate[32].period: 0.006405
Optimizing.....                                 
------------------------------------------------------------------
                   Optimized values                               
------------------------------------------------------------------
Candidate:    0: index:  351    S/N:   3.05 width: 0.000075 period:  0.000401700  pdot: 5e-08  dm: 8.7666807
Candidate:    1: index:  121    S/N:   3.53 width: 0.000078 period:  0.000594285  pdot: 5e-08  dm: 9.4333401
Candidate:    2: index:  283    S/N:   3.72 width: 0.000102 period:  0.000788770  pdot: 0  dm: 10.1000004
...
Candidate:  126: index:   83    S/N:   3.17 width: 0.001231 period: 0.024611030  pdot: 5e-08  dm: 12.0999804
Candidate:  127: index:  336    S/N:   3.40 width: 0.000806 period: 0.024999700  pdot: -5e-08  dm:  8.1000204
------------------------------------------------------------------

Total exec time      :    99.64  (sec)
Total GPU exec time  :    99.14  (sec)



============== Octave/Matlab Display ====================

The profiles (if saved with -w option) can be displayed with octave/matlab
by the supplied macro display_profile


octave:1> display_profile(4)
filepro___ = profile_003.out
fileproopt = profile_003.opt
...

An example can be found in doc/display_profile.gif


============== Origami Command Line Options =============

> ./origami -h

Usage:  ./origami options [ inputfile ... ]
  -h  --help              Display this usage information.
  -v  --verbose  flag     Print verbose messages                   (Default 0)
  -g  --ngpu     num      the number of gpu in use                 (Default 1)
  -t  --tsamp    time     the sampling time (usec)                 (Default 50)
  -T  --tobs     time     the observ. time (sec)          [1- 600] (Default 60)
  -F  --input    file     the input data file                      (Default ska.dat)
  -n  --nsamp    num      the number of samples                    (Default 12e5)
  -c  --nchan    num      the number freq. channels      [64-4096] (Default 4096) 
  -C  --ncand    num      the number of pulsar candidates [1- 256] (Default 128) 
  -f  --fch1     val      the first channel freq (MHz)             (Default 1550)
  -b  --foff     val      channel bandwith (MHz)                   (Default 0.075)
  -p  --period   val      pulsar period (ms)                       (Default 1.1236)
  -P  --pdot     val      pulsar pdot (ms/s)                       (Default 0.)
  -d  --dm       val      dispersion measure                       (Default 10)
  -B  --nbins    val      max folding bins                [16-256] (Default 128)
  -r  --rebin    val      pre-folding rebin (power of 2)  [1 - 16] (Default 1)
  -R  --Rebin    val      pre-folding rebin strategy      [1 -  2] (Default 2)
  -i  --nsubint  val      folding sub integ               [64-256] (Default 64)
  -s  --nsubband val      folding subbands                [1 -128] (Default 64) 
  -o  --optimize          select optimization dimension   [2,3,5]  (Default 2) 5 = 2D & 3D 
  -N  --nosplit           disable folding phase shift              (Default no split) 
  -e  --enable   val      enable data check for rebin val [1 - 16] (Default 1) 
  -l  --log               logging on file                          (Default off) 
  -w  --write             enable output profile writing            (Default off) 

On 202002 we implement a Datafile Header decode. We decode only
frequency data and sample time. 
NOTE:
These values superseede both default and command line values.

============== Advanced Compilation modes ===============

The script compile_origami_cl let the user compile the origami program
with more configure options.
This version of the origami executable runs the input data corner-turner
operation on CPU, using OpenCL if this is explicitly required by user and if
it is installed on the system.

WARNING: the OpenCL portion is no more maintained in the main trunk version
and will be moved, in a future release, to a specific branch.

The additional configure options are:

--with-cpu_transpose
  
  this option define the macro TRANSPOSE_CPU to enable corner-turner
  operation in CPU

--with-opencl

  this option define the macro WITH_CL to enable OpenCL support for CPU
  parallelization

--with-opencl_libdir=/opt/AMDAPP/lib

  this option let user specify the path to OpenCL library.
  If the OpenCL library is in a different directory from the one specified,
  change accordingly the value.
  Set the LD_LIBRARY_PATH environment variable to the library path.

--with-opencl_incdir=/opt/AMDAPP/include
  
  this option let user specify the path to OpenCL includes




================== Description of various files ======================

asyncAPI.cu             
device_folding.cu       cuda routines to be run on GPU
folding.c               main CPU program
foldutl.c               utility routines for main CPU program
host_folding.cu         cuda routines to be run on CPU
kernel.cu               cuda routines to be run on CPU
Makefile.am             local Makefiles changes
nvml.c                  CUDA interface to telemetry
summary_statistics.cu   mean and std computation by thrust lib



