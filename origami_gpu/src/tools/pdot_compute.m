%
% Compute the basic basic fake binary parameters to obtain a pdot
% Uses binary_papp
% 
% C.Baffa 2019 
%
% Global variables 
PI              = 3.14159265;
SPEED_OF_LIGHT  = 2.99792458e8;
T_SUN           = 4.92544947;

M_SUN           = 2.0e30;
G               = 6.672e-11;

phase           = 0.250;
orbitalPeriod   = 10;
orbitalPeriod   = 4.918;

% we use default values exept for orbital period
% -binary     - create binary system
% -bper       - orbital period in hours (def=10.0)
% -becc       - eccentricity (def=0.0, circular)
% -binc       - inclination in degrees (def=90.0)
% -bomega     - longitude of periastron in degrees (def= 0.0)
% -bphase     - starting orbital phase (number between 0 and 1, def=0.0)
% -bpmass     - pulsar mass in solar units (def=1.4))
% -bcmass     - companion mass in solar units (def=5.0) We use 20
%
% function pApp = binary_papp( rest_period, time, orbitalPeriod)

% plot of period as function of time 0:530s

subplot(3,1,1)
%
time = 0:530;
time = -215:215;
%time = 0:50:orbitalPeriod*3600/2;
papp = [];
der  = [];
for i = time
    papp = [ papp binary_papp(1, i/3600, orbitalPeriod, phase)] ;
    q = (binary_papp(1, (i+1)/3600, orbitalPeriod, phase) -  ...
          binary_papp(1, i/3600, orbitalPeriod, phase)) / 1. ;
    der = [ der q ] ;
endfor

plot(time/3600, papp)
title('Apparent Period as a function of time ');
ylabel('apparent period (ms)')
xlabel('Scan time')

subplot(3,1,2)

plot(time/3600, der)
title('Period derivative as a function of time ');
ylabel('pdot (s/s)')
xlabel('Scan time')
grid on

orPeriod = 2:2:100;
orPeriod = 4:0.5:20;
orPeriod = 4:0.01:5;
orPeriod = 4.9:0.001:4.92;

der1  = [];
for i = orPeriod
    q = (binary_papp(1, 215/3600,i, phase) - binary_papp(1, 214/3600, i, phase)) / 1. ;
    der1 = [ der1 q ] ;
endfor
subplot(3,1,3)
semilogy(orPeriod, der1)
plot(orPeriod, der1)
grid on
title('Endo of scan Period derivative as a function of orbital period mass ');
ylabel('Max pdot (s/s)')
xlabel('orbital period (hours)')



