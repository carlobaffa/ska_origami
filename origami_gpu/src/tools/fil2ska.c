#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
/*
 * fil2ska.c - source taken from SIGPROC sources
 *
 * 15/04/2015: Modified to transform filterbank data file in SKA-like file.
 *
 * */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>



#define HI2BITS 192
#define UPMED2BITS 48
#define LOMED2BITS 12
#define LO2BITS 3
/* global variables describing the data */
char rawdatafile[80], source_name[80];
int machine_id, telescope_id, data_type, nchans, nbits, nifs, scan_number,
  barycentric,pulsarcentric; /* these two added Aug 20, 2004 DRL */
long nsamp = 0;
double tstart,mjdobs,tsamp,tobs, fch1,foff,refdm,az_start,za_start,src_raj,src_dej;
double gal_l,gal_b,header_tobs,raw_fch1,raw_foff;
int nbeams, ibeam;
char isign;
/* added 20 December 2000    JMC */
double srcl,srcb;
double ast0, lst0;
long wapp_scan_number;
char project[8];
char culprits[24];
double analog_power[2];
int ifchan[16], frchan[4096], ifnum, chnum, ns, charout;

/* added frequency table for use with non-contiguous data */
double frequency_table[4096]; /* note limited number of channels */
long int npuls; /* added for binary pulse profile format */


// define the signedness for the 8-bit data type
#define OSIGN 1
#define SIGNED OSIGN < 0
int nbins;
double period;

int strings_equal (char *string1, char *string2) /* includefile */
{
    if (!strcmp(string1,string2)) {
        return 1;
    } else {
        return 0;
    }
}


/* read a string from the input which looks like nchars-char[1-nchars] */
void get_string(FILE *inputfile, int *nbytes, char string[])
{
  int nchar;
  strcpy(string,"ERROR");

  // read the first value (int) that is the number of the next valid chars
  fread(&nchar, sizeof(int), 1, inputfile);

  *nbytes = sizeof(int);
  if (feof(inputfile)) exit(0);
  if (nchar > 80 || nchar < 1) return;
  // read the next valid chars
  fread(string, nchar, 1, inputfile);
  string[nchar]='\0';
  *nbytes+=nchar;
}

/* attempt to read in the general header info from a pulsar data file */
int read_header(FILE *inputfile) /* includefile */
{
  char string[80], message[80];
  int itmp,nbytes,totalbytes,expecting_rawdatafile=0,expecting_source_name=0;
  int expecting_frequency_table=0,channel_index;
  isign=0;


  nbytes = 0;
  /* try to read in the first line of the header */
  get_string(inputfile, &nbytes, string);
  if (!strings_equal(string,"HEADER_START")) {
	/* the data file is not in standard format, rewind and return */
	rewind(inputfile);
	return 0;
  }
  /* store total number of bytes read so far */
  totalbytes = nbytes;

  /* loop over and read remaining header lines until HEADER_END reached */
  while (1) {
    get_string(inputfile, &nbytes,string);
    if (strings_equal(string,"HEADER_END")) {
        break;
    }
    totalbytes += nbytes;
    if (strings_equal(string,"rawdatafile")) {
      expecting_rawdatafile = 1;
    } else if (strings_equal(string,"source_name")) {
      expecting_source_name = 1;
    } else if (strings_equal(string,"FREQUENCY_START")) {
      expecting_frequency_table = 1;
      channel_index = 0;
    } else if (strings_equal(string,"FREQUENCY_END")) {
      expecting_frequency_table = 0;
    } else if (strings_equal(string,"az_start")) {
      fread(&az_start,sizeof(az_start),1,inputfile);
      totalbytes+=sizeof(az_start);
    } else if (strings_equal(string,"za_start")) {
      fread(&za_start,sizeof(za_start),1,inputfile);
      totalbytes+=sizeof(za_start);
    } else if (strings_equal(string,"src_raj")) {
      fread(&src_raj,sizeof(src_raj),1,inputfile);
      totalbytes+=sizeof(src_raj);
    } else if (strings_equal(string,"src_dej")) {
      fread(&src_dej,sizeof(src_dej),1,inputfile);
      totalbytes+=sizeof(src_dej);
    } else if (strings_equal(string,"tstart")) {
      fread(&tstart,sizeof(tstart),1,inputfile);
      totalbytes+=sizeof(tstart);
    } else if (strings_equal(string,"tsamp")) {
      fread(&tsamp,sizeof(tsamp),1,inputfile);
      totalbytes+=sizeof(tsamp);
    } else if (strings_equal(string,"period")) {
      fread(&period,sizeof(period),1,inputfile);
      totalbytes+=sizeof(period);
    } else if (strings_equal(string,"fch1")) {
      fread(&fch1,sizeof(fch1),1,inputfile);
      totalbytes+=sizeof(fch1);
    } else if (strings_equal(string,"fchannel")) {
      fread(&frequency_table[channel_index++],sizeof(double),1,inputfile);
      totalbytes+=sizeof(double);
      fch1=foff=0.0; /* set to 0.0 to signify that a table is in use */
    } else if (strings_equal(string,"foff")) {
      fread(&foff,sizeof(foff),1, inputfile);
      totalbytes+=sizeof(foff);
    } else if (strings_equal(string,"nchans")) {
      fread(&nchans,sizeof(nchans),1,inputfile);
      totalbytes+=sizeof(nchans);
    } else if (strings_equal(string,"telescope_id")) {
      fread(&telescope_id,sizeof(telescope_id),1,inputfile);
      totalbytes+=sizeof(telescope_id);
    } else if (strings_equal(string,"machine_id")) {
      fread(&machine_id,sizeof(machine_id),1,inputfile);
      totalbytes += sizeof(machine_id);
    } else if (strings_equal(string,"data_type")) {
      fread(&data_type,sizeof(data_type),1,inputfile);
      totalbytes += sizeof(data_type);
    } else if (strings_equal(string,"ibeam")) {
      fread(&ibeam,sizeof(ibeam),1,inputfile);
      totalbytes += sizeof(ibeam);
    } else if (strings_equal(string,"nbeams")) {
      fread(&nbeams,sizeof(nbeams),1,inputfile);
      totalbytes += sizeof(nbeams);
    } else if (strings_equal(string,"nbits")) {
      fread(&nbits,sizeof(nbits),1,inputfile);
      totalbytes += sizeof(nbits);
    } else if (strings_equal(string,"barycentric")) {
      fread(&barycentric,sizeof(barycentric),1,inputfile);
      totalbytes += sizeof(barycentric);
    } else if (strings_equal(string,"pulsarcentric")) {
      fread(&pulsarcentric,sizeof(pulsarcentric),1,inputfile);
      totalbytes += sizeof(pulsarcentric);
    } else if (strings_equal(string,"nbins")) {
      fread(&nbins,sizeof(nbins),1,inputfile);
      totalbytes += sizeof(nbins);
    } else if (strings_equal(string,"nsamples")) {
      /* read this one only for backwards compatibility */
      fread(&itmp,sizeof(itmp),1,inputfile);
      totalbytes += sizeof(itmp);
    } else if (strings_equal(string,"nifs")) {
      fread(&nifs, sizeof(nifs),1,inputfile);
      totalbytes += sizeof(nifs);
    } else if (strings_equal(string,"npuls")) {
      fread(&npuls,sizeof(npuls),1,inputfile);
      totalbytes += sizeof(npuls);
    } else if (strings_equal(string,"refdm")) {
      fread(&refdm,sizeof(refdm),1,inputfile);
      totalbytes += sizeof(refdm);
    } else if (strings_equal(string,"signed")) {
      fread(&isign,sizeof(isign),1,inputfile);
      totalbytes += sizeof(isign);
    } else if (expecting_rawdatafile) {
      strcpy(rawdatafile,string);
      expecting_rawdatafile=0;
    } else if (expecting_source_name) {
      strcpy(source_name,string);
      expecting_source_name = 0;
    } else {
      sprintf(message,"read_header - unknown parameter: %s\n", string);
      fprintf(stderr,"ERROR: %s\n",message);
      exit(1);
    }
    if (totalbytes != ftell(inputfile)){
	    fprintf(stderr,"ERROR: Header bytes does not equal file position\n");
	    fprintf(stderr,"String was: '%s'\n",string);
	    fprintf(stderr,"       header: %d file: %ld\n", totalbytes, ftell(inputfile));
	    exit(1);
    }

    if (isign < 0 && OSIGN > 0){
	    fprintf(stderr,"WARNING! You are reading unsigned numbers with a signed version of sigproc\n");
    }
    if (isign > 0 && OSIGN < 0){
	    fprintf(stderr,"WARNING! You are reading signed numbers with a unsigned version of sigproc\n");
    }


  }

  /* add on last header string */
  totalbytes += nbytes;
  printf("Total header dimension in bytes is %d\n", totalbytes);
  if (totalbytes != ftell(inputfile)){
	  fprintf(stderr,"ERROR: Header bytes does not equal file position\n");
	  fprintf(stderr,"       header: %d file: %ld\n", totalbytes, ftell(inputfile));
          return -1;
  }

  /* return total number of bytes read */
  return totalbytes;
}


int main(int argc, char **argv)
{

    FILE *input = NULL;
    FILE *output = NULL;
    int skip_head = 0;       /* number of bytes in header */
//    int numerate,i,j,k,l,n,m,stream,nsperdmp,nsamps,indexing,indexnow;
    int i,j,k;
    int insamp = 0;
    char message[80];
#if SIGNED
    char c;
#else
    unsigned char c;
#endif
    unsigned short s;
    float f[8];
    unsigned char u[8];
    struct stat stbuf;

    /* default case is to read from standard input */
    errno = 0;
    printf("argc: %d argv[0]: %s argv[1]: %s\n", argc, argv[0], argv[1]);
    if (argc > 1) {
        input = fopen(argv[1], "rw");
        if (input) {
            if (stat(argv[1], &stbuf) < 0) {
                printf("Can't open file %s (%d)\n", argv[1], errno );
                return 0;
            }
            skip_head = read_header(input);
        } else {
            printf("Can't open file %s (%d)\n", argv[1], errno );
            return 0;
        }
        if (skip_head == 0) {
            printf("No header found!!\n");
            return -1;
        }
    }
    if (nsamp == 0) {
        nsamp = (long)((stbuf.st_size - skip_head) * 8.0/(nchans * nbits));
        tobs = nsamp * tsamp;
        printf("nsamples: %ld tsamp: %f nbits: %d tobs: %f\n", nsamp, tsamp, nbits, tobs);
    }
    // build the output filename using the input name and changing
    // extension (from fil to dat)
    char outfile[128];
    char *p = strstr(argv[1], ".fil");
    int n = p - argv[1];
    snprintf(outfile, n + 1, "%s", argv[1]);
    strncat(outfile, ".dat", 4);
    printf("Output file: %s\n", outfile);
    output = fopen (outfile, "w");
    if (output == NULL) {
        printf("Can't open %s file for output!\n", outfile);
        fclose(input);
        return 0;
    }
    //double T = 60;
    //long toprocess = T/tsamp * nchans;
    while (!feof(input)) {
        /* unpack the sample(s) if necessary */
        switch (nbits) {
            case 1:
                //printf("nbits: %d\n", nbits);
                fread(&c, 1, 1, input);
                for (i = 0; i < 8; i++) {
                    u[i] = c & 1;
                    c >>= 1;
                }
                fwrite(u, sizeof(char), 8, output);
                insamp += 1;
                //if (insamp == toprocess) {
                //    goto end;
                //}
                ns = 8;
                break;
            case 2:
                fread(&c, 1, 1, input);
                u[0] = c & LO2BITS;
                u[1] = (c & LOMED2BITS) >> 2;
                u[2] = (c & UPMED2BITS) >> 4;
                u[3] = (c & HI2BITS) >> 6;
                fwrite(u, sizeof(char), 4, output);
                insamp += 1;
                //if (insamp == toprocess) {
                //    goto end;
                //}
                //f[0]=(float) j;
                //f[1]=(float) k;
                //f[2]=(float) n;
                //f[3]=(float) m;
                ns = 4;
                break;
            case 4:
                fread(&c,1,1,input);
                //char2ints(c,&j,&k);
                //f[0] = (float) j;
                //f[1] = (float) k;
                ns = 2;

                break;
            case 8:
                fread(&c, nbits/8, 1, input);
                f[0] = (float) c;
                ns = 1;
                break;
            case 16:
                fread(&s, nbits/8, 1, input);
                f[0] = (float) s;
                ns = 1;
                break;
            case 32:
                fread(&f[0],nbits/8,1,input);
                ns = 1;
                break;
            default:
                sprintf(message,"cannot read %d bits per sample...\n", nbits);
                //error_message(message);
                break;
        }

    }
end:
    fclose(output);
    fclose(input);
    return 0;
}
