#
# procedure to generate both testvectors and 
# the relative candidate files
#
# SKA sprint 2.2 AT4-5
#
# 2019 C.Baffa - Licence GPL 2
#
# File are in  ~/testvectors/
# Data Filenames are :
# ska_<scan time>_<channels>_<sampling time>_<period>_<dm>_<pdot>.dat
# and the candidates:
# ska_<scan time>_<channels>_<sampling time>_<period>_<dm>_<pdot>.txt
"Module for the computation Origami test Vectors "

import subprocess

MAXITER = 5
global DATAPATH
global scantime
global channels
global sampling

DATAPATH = "~/testvectors"
scantime = 530.
scantime = 10.1
channels = 4096
sampling = 64.

def filename(per=1.1234, dm=10., pdot=0., ch1=1550):
    "Returns the file name without extention"
    result = ("%s/ska_%.0f_%d_%.0f_%.4f_%.3f_%07.3g_%.0f" % 
              (DATAPATH, scantime, channels, sampling,
               per, dm, pdot, ch1) )
    return result

def datafilename(per=1.1234, dm=10., pdot=0., ch1=1550.):
    "Returns the data file name "
    result = ("%s.dat" % filename(per, dm, pdot, ch1) )
    return result

def candidatefilename(per=1.1234, dm=10., pdot=0., ch1=1550.):
    "Returns the candidate file name "
    result = ("%s.txt" % filename(per, dm, pdot, ch1) )
    return result

def fakeargs(per=1.1234, dm=10., pdot=0., ch1=1550., foff=-0.075):
    " return the arguments to let fake produce data"
    formato= (' -period %.4f  -tsamp %.0f'
              ' -fch1 %.0f -foff %.3f -nchans %d -tobs %.0f '
              ' -nbits 8 -dm %.3f  -snrpeak 1 -pdot %e ')
    result = (formato % (per, sampling, ch1, foff, channels, scantime, dm, pdot) )
    return result

def oriargs(ch1=1550., foff=-0.075):
    " return the arguments to let fake produce data"
    formato= ( ' -f %.1f -b %.4f -c %d -T %.0f ')
    result = (formato % (ch1, foff, channels, scantime) ) 
    return result

def compute_data(per=1.1234, dm=10., pdot=0., ch1=1550., foff=-0.075):
    "compute the data file "
    argomenti = fakeargs(per, dm, pdot, ch1, foff )
    datafile  = datafilename(per, dm, pdot, ch1)
    result = ("fake %s > %s" % (argomenti, datafile)) 
    return result

def compute_ori(ch1=1550., foff=-0.075, per=1.1234, dm=10., pdot=0.):
    "compute the data file "
    argomenti = oriargs(ch1, foff)
    datafile  = datafilename(per, dm, pdot, ch1)
    result = ("./origami  %s -F %s" % (argomenti, datafile)) 
    return result

def batch_datafile(pdot = 0.):
    "Write a shell script to compute a plane in dm - per space "
    dm_collection  = [1, 5.62341, 31.62274, 177.82763 ]
    per_collection = [1.1234, 7.29723, 47.40043]
    ch1_collection = [1250., 1550., 1750.]

    tmp_candidates_name = "/tmp/t.dat"
    tmp_candidates      = open(tmp_candidates_name, "w+")
    for dm in dm_collection:
        for per in per_collection:
            tmp_candidates.write(" %10.6e %10.6e %10.6e\n" % ( per/1.e3, 0.0, dm  ))
    tmp_candidates.close

    batch_filename = ('./vector_batch_pdot_%.3f.sh' % pdot)
    f= open(batch_filename, "w+")
    f.write("#!/bin/bash\n\n")
    f.write("# shell script to compute a plane in dm - per space\n")
    f.write("# \n")
    f.write("# Automatically created: do not edit! Iwill be overwritten")
    f.write("# \n")
    f.write("mkdir -p ~/testvectors\n")
    f.write("# \n\n")
    
    per  = 1.1234
    ch1  = 1550.
    foff = -0.075 
    for dm in dm_collection:
        for ch1 in ch1_collection:
            cmd = compute_data(per, dm, pdot, ch1)
            f.write("%s\n" % cmd)
            f.write("cp /tmp/t.dat %s\n" % (candidatefilename(per, dm, pdot, ch1))) 
            cmd = compute_ori(ch1, foff, per, dm, pdot)
            f.write("%s\n" % cmd)
    f.close
    return

if __name__=='__main__': 
    print(datafilename())
    print(datafilename(dm=5.623))
    print(candidatefilename(dm=5.623))
    print(fakeargs(dm=5.623))
    print(compute_data(dm=5.623))
    batch_datafile()
