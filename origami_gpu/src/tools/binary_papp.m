%
% Compute the pdot from basic fake binary parameters.
% from a M Keith, R Eatough 2006 routine
% 
% C.Baffa 2019
%

function pApp = binary_papp( rest_period, time, orbitalPeriod_i, startPhase_i )

if ((nargin() < 2) || (nargin() > 4))
    printf ("usage:  binary_papp( rest_period, time, orbitalPeriod, [ startPhase] ) \n");
    pApp = NaN;
    return;
endif

if (nargin() < 3) 
    orbitalPeriod  = 10.;
    %printf (" orbitalPeriod  = 10.;\n");
else 
    orbitalPeriod  = orbitalPeriod_i;
endif

if (nargin() < 4)  
    startPhase     = 0.5; 
    %printf (" startPhase     = 0.5;\n");
else 
    startPhase  = startPhase_i;
    %printf (" startPhase     = %f;\n",startPhase);
endif


% Global variables 
PI              = 3.14159265;
SPEED_OF_LIGHT  = 2.99792458e8;
T_SUN           = 4.92544947;

M_SUN           = 2.0e30;
G               = 6.672e-11;

% we use default values exept for orbital period
% -binary     - create binary system
% -bper       - orbital period in hours (def=10.0)
% -becc       - eccentricity (def=0.0, circular)
% -binc       - inclination in degrees (def=90.0)
% -bomega     - longitude of periastron in degrees (def= 0.0)
% -bphase     - starting orbital phase (number between 0 and 1, def=0.0)
% -bpmass     - pulsar mass in solar units (def=1.4))
% -bcmass     - companion mass in solar units (def=5.0)
%

%orbitalPeriod  = 10.; % /* Seconds */
eccentricity    = 0.;
massPulsar      = 1.4; % /* Solar masses */
massCompanion   = 5.;  % /* Solar masses */
massCompanion   = 20; % /* Solar masses */
%startPhase      = 0.5;  % /*0.0 - 1.0 to determine way though orbit at start */
inclination     = 90; % /* Degrees */
omega           = 0.;  % /*Longitude of periastron*/

%
    incl = inclination * (PI/180);

    omegaB = 2.0 * PI / orbitalPeriod;
    t0 = startPhase * orbitalPeriod;
    massFunction = power((massCompanion * sin(incl)),3)/power((massCompanion+massPulsar),2);

    asini=power(( M_SUN *massFunction * G * orbitalPeriod / (4.0*PI*PI)) ,0.333333333333);

% Solve for the Excentric Anomoly.
% meanAnomoly, M = E - e*sin(E) = omageB * (t-t0)
%
    meanAnomaly = omegaB * (time-t0);
    eccentricAnomaly = meanAnomaly;
    for i = 1:10
        eNext = eccentricAnomaly -                                                      ...
                (eccentricAnomaly - eccentricity * sin(eccentricAnomaly) - meanAnomaly) ...
                /                                                                       ... 
                (1.0 - eccentricity*cos(eccentricAnomaly));
        
        if (abs(eNext - eccentricAnomaly) < 1.0e-10) 
            break;
        endif;
        
        eccentricAnomaly=eNext;
    endfor;

    trueAnomaly = 2.0 * atan( sqrt( (1.0 + eccentricity) / (1.0 - eccentricity)) ...
                              * tan(eccentricAnomaly/2.0) );

    velocity = omegaB                                                 ...
            * (asini / (sqrt(1.0 - power(eccentricity,2))))           ...
            * (cos(omega + trueAnomaly) + eccentricity * cos(omega));

    pApp = rest_period * (1.0 + (velocity / SPEED_OF_LIGHT));

endfunction
