#
# procedure to generate both testvectors and 
# the relative candidate files
#
# SKA sprint 2.2 AT4-5
#
# 2019 C.Baffa - Licence GPL 2
#
# File are in  ~/testvectors/
# Data Filenames are :
# ska_<scan time>_<channels>_<sampling time>_<period>_<dm>_<pdot>.dat
# and the candidates:
# ska_<scan time>_<channels>_<sampling time>_<period>_<dm>_<pdot>.txt
"Module for the computation Origami test Vectors "

import subprocess

MAXITER = 5
global DATAPATH
global scantime
global channels
global sampling

DATAPATH = "~/testvectors"
scantime = 530.
channels = 4096
sampling = 64.

def filename(per=1.1234, dm=10., pdot=0.):
    "Returns the file name without extention"
    result = ("%s/ska_%.0f_%d_%.0f_%.4f_%.3f_%e" % 
              (DATAPATH, scantime, channels, sampling,
               per, dm, pdot) )
    return result

def datafilename(per=1.1234, dm=10., pdot=0.):
    "Returns the data file name "
    result = ("%s.dat" % filename(per, dm, pdot) )
    return result

def candidatefilename(per=1.1234, dm=10., pdot=0.):
    "Returns the candidate file name "
    result = ("%s.txt" % filename(per, dm, pdot) )
    return result

def fakeargs(per=1.1234, dm=10., pdot=0.):
    " return the arguments to let fake produce data"
    formato= (' -headerless -period %.4f  -tsamp %.0f'
              ' -fch1 1550 -foff -0.075 -nchans %d -tobs %.0f '
              ' -nbits 8 -dm %.3f  -snrpeak 1 -pdot %e ')
    result = (formato % (per, sampling, channels, scantime, dm, pdot) )
    return result

def compute_data(per=1.1234, dm=10., pdot=0.):
    "compute the data file "
    argomenti = fakeargs(per, dm, pdot)
    datafile  = datafilename(per, dm, pdot)
    result = ("fake %s > %s" % (argomenti, datafile)) 
    return result

def batch_datafile(pdot = 0.):
    "Write a shell script to compute a plane in dm - per space "
    dm_collection  = [1, 5.62341, 31.62274, 177.82763, 1000 ]
    per_collection = [1.1234, 7.29723, 47.40043, 307.8975, 2000]

    batch_filename = ('./vector_batch_pdot_%.3f.sh' % pdot)
    f= open(batch_filename, "w+")
    f.write("#!/bin/bash\n\n")
    f.write("# shell script to compute a plane in dm - per space\n")
    f.write("# \n")
    f.write("# Automatically created: do not edit! Iwill be overwritten")
    f.write("# \n")
    f.write("mkdir -p ~/testvectors\n")
    f.write("# \n\n")

    for dm in dm_collection:
        for per in per_collection:
            cmd = compute_data(per, dm, pdot)
            f.write("%s\n" % cmd)

    f.close
    return

if __name__=='__main__': 
    print(datafilename())
    print(datafilename(dm=5.623))
    print(candidatefilename(dm=5.623))
    print(fakeargs(dm=5.623))
    print(compute_data(dm=5.623))
    batch_datafile()
