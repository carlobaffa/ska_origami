 /***************************************************************************\
|*                                                                           *|
|*      Copyright 2010-2013 NVIDIA Corporation.  All rights reserved.        *|
|*                                                                           *|
|*   NOTICE TO USER:                                                         *|
|*                                                                           *|
|*   This source code is subject to NVIDIA ownership rights under U.S.       *|
|*   and international Copyright laws.  Users and possessors of this         *|
|*   source code are hereby granted a nonexclusive, royalty-free             *|
|*   license to use this code in individual and commercial software.         *|
|*                                                                           *|
|*   NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE     *|
|*   CODE FOR ANY PURPOSE. IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR         *|
|*   IMPLIED WARRANTY OF ANY KIND. NVIDIA DISCLAIMS ALL WARRANTIES WITH      *|
|*   REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF         *|
|*   MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR          *|
|*   PURPOSE. IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL,            *|
|*   INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES          *|
|*   WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN      *|
|*   AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING     *|
|*   OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE      *|
|*   CODE.                                                                   *|
|*                                                                           *|
|*   U.S. Government End Users. This source code is a "commercial item"      *|
|*   as that term is defined at 48 C.F.R. 2.101 (OCT 1995), consisting       *|
|*   of "commercial computer  software" and "commercial computer software    *|
|*   documentation" as such terms are used in 48 C.F.R. 12.212 (SEPT 1995)   *|
|*   and is provided to the U.S. Government only as a commercial end item.   *|
|*   Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through        *|
|*   227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the       *|
|*   source code with only those rights set forth herein.                    *|
|*                                                                           *|
|*   Any use of this source code in individual and commercial software must  *|
|*   include, in the user documentation and internal comments to the code,   *|
|*   the above Disclaimer and U.S. Government End Users Notice.              *|
|*                                                                           *|
|*                                                                           *|
 \***************************************************************************/

#include <stdio.h>
#include <nvml.h>
#include "folding.h"

const char * convertToComputeModeString(nvmlComputeMode_t mode)
{
    switch (mode)
    {
        case NVML_COMPUTEMODE_DEFAULT:
            return "Default";
        case NVML_COMPUTEMODE_EXCLUSIVE_THREAD:
            return "Exclusive_Thread";
        case NVML_COMPUTEMODE_PROHIBITED:
            return "Prohibited";
        case NVML_COMPUTEMODE_EXCLUSIVE_PROCESS:
            return "Exclusive Process";
        default:
            return "Unknown";
    }
}

/*
 * int gpu_temp(int gpu)
 */
int gpu_temp(int gpu)
{
    unsigned int temp;
    nvmlReturn_t result;
    nvmlDevice_t device;
    struct gpu_nvml *g_nvml = 0;

    result = nvmlDeviceGetHandleByIndex(gpu, &device);
    if (NVML_SUCCESS != result) {
        iolog(1, "Failed to get handle for device %d: %s\n", gpu, nvmlErrorString(result));
        return -1;
    }
    result = nvmlDeviceGetTemperature(device, NVML_TEMPERATURE_GPU, &temp);
    if (NVML_SUCCESS != result) {
        errlog("Failed to get temperature for device %d: %s\n", gpu, nvmlErrorString(result));
        return -1;
    }
    g_nvml = &Gpus[gpu].nvml;
    g_nvml->temp = temp;
    return 0;
}

/*
 * int gpu_fanspeed(int gpu)
 */
int gpu_fanspeed(int gpu)
{
    unsigned int fan = 0;
    nvmlReturn_t result;
    nvmlDevice_t device;
    struct gpu_nvml *g_nvml = 0;

    result = nvmlDeviceGetHandleByIndex(gpu, &device);
    if (NVML_SUCCESS != result) {
        iolog(1, "Failed to get handle for device %d: %s\n", gpu, nvmlErrorString(result));
        return -1;
    }
    result = nvmlDeviceGetFanSpeed(device, &fan);
    if (NVML_SUCCESS != result) {
        iolog(2, "Failed to get fan speed for device %d: %s\n", gpu, nvmlErrorString(result));
        return -1;
    }
    g_nvml = &Gpus[gpu].nvml;
    g_nvml->fan_speed = fan;
    return 0;
}

/*
 * int init_nvml(void)
 */
int init_nvml(void)
{
    nvmlReturn_t result;
    unsigned int device_count, i;
    struct gpu_nvml *g_nvml;

    // First initialize NVML library
    result = nvmlInit();
    if (NVML_SUCCESS != result)
    {
        iolog(1, "Failed to initialize NVML: %s\n", nvmlErrorString(result));

        //iolog(1, "Press ENTER to continue...\n");
        //getchar();
        return -1;
    }

    result = nvmlDeviceGetCount(&device_count);
    if (NVML_SUCCESS != result) {
        iolog(1, "Failed to query device count: %s\n", nvmlErrorString(result));
        goto Error;
    }
    iolog(1, "Found %d device%s\n\n", device_count, device_count != 1 ? "s" : "");

    iolog(1, "Listing devices:\n");
    for (i = 0; i < device_count; i++) {
        nvmlDevice_t device;
        char name[NVML_DEVICE_NAME_BUFFER_SIZE];
        nvmlPciInfo_t pci;

        // Query for device handle to perform operations on a device
        // You can also query device handle by other features like:
        // nvmlDeviceGetHandleBySerial
        // nvmlDeviceGetHandleByPciBusId
        result = nvmlDeviceGetHandleByIndex(i, &device);
        if (NVML_SUCCESS != result) {
            iolog(1, "Failed to get handle for device %i: %s\n", i, nvmlErrorString(result));
            goto Error;
        }
        g_nvml = &Gpus[i].nvml;

        result = nvmlDeviceGetName(device, name, NVML_DEVICE_NAME_BUFFER_SIZE);
        if (NVML_SUCCESS != result) {
            iolog(1, "Failed to get name of device %i: %s\n", i, nvmlErrorString(result));
            goto Error;
        }
        strncpy(g_nvml->name, name, sizeof(g_nvml->name));

        // pci.busId is very useful to know which device physically you're talking to
        // Using PCI identifier you can also match nvmlDevice handle to CUDA device.
        result = nvmlDeviceGetPciInfo(device, &pci);
        if (NVML_SUCCESS != result) {
            errlog("Failed to get pci info for device %i: %s\n", i, nvmlErrorString(result));
            goto Error;
        }

        iolog(1, "%d. %s [%s]\n", i, name, pci.busId);
    }
#ifdef NO
        unsigned int fan = 0;
        result = nvmlDeviceGetFanSpeed(device, &fan);
        if (NVML_SUCCESS != result) {
            errlog("Failed to get fan speed for device %i: %s\n", i, nvmlErrorString(result));
            goto Error;
        }
        g_nvml->fan_speed = fan;
        iolog(1, "[%s] Fan %u\n", name, fan);
        unsigned int temp = 0;
        result = nvmlDeviceGetTemperature(device, NVML_TEMPERATURE_GPU, &temp);
        if (NVML_SUCCESS != result) {
            errlog("Failed to get temperature for device %i: %s\n", i, nvmlErrorString(result));
            goto Error;
        }
        iolog(1, "[%s] Temperature (C) %u\n", name, temp);
        g_nvml->temp = temp;
    }
    result = nvmlShutdown();
    if (NVML_SUCCESS != result) {
        iolog(1, "Failed to shutdown NVML: %s\n", nvmlErrorString(result));
        goto Error;
    }
#endif

    iolog(1, "All done.\n");
    return 0;

Error:
    result = nvmlShutdown();
    if (NVML_SUCCESS != result) {
        errlog("Failed to shutdown NVML: %s\n", nvmlErrorString(result));
    }
    return 1;
}

/*
 * int shutdown_nvml(void)
 */
int shutdown_nvml(void)
{
    nvmlReturn_t result;
    result = nvmlShutdown();
    if (NVML_SUCCESS != result) {
        iolog(1, "Failed to shutdown NVML: %s\n", nvmlErrorString(result));
        return -1;
    }
    return 0;
}

