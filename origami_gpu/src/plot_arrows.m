# $Id:$ 
# plot a vector field showing the positional changes of candidates
#
load result.txt
whos
result3dplot
pause
o_dm = result(:,10);
o_period = result(:,8);
o_pdot = result(:,9);
a_dm = dm - o_dm;
a_period = period - o_period;
a_pdot = pdot - o_pdot;
whos
quiver(o_dm, o_period, a_dm, a_period)
subplot(1,1,1)
quiver(o_dm, o_period, a_dm, a_period)
quiver(o_dm, o_period, a_dm, a_period, 0.1)
