/* $Id: host_folding.cu 1175 2019-04-02 14:40:13Z egiani $ */

//#include <folding.h>
#include <../config.h>


/*
 * int map_host_memory(void)
 */
/**
 *
 * Map the allocated host memory into the CUDA address space.
 * The device pointer to the memory may be obtained by calling
 * cudaHostGetDevicePointer().
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise.
 */
int map_host_memory(binning_t *rebin)
{
    int i;
    uint64_t mem_size = 0;
    uint64_t mem_devsize = 0;
    cudaError_t err = cudaSuccess;      /* cuda error code */

    int nbinning = first_valid_binidx(rebin);          // get the first valid binning value
    mem_size = fold.nsamp*NBYTES/fold.nsubints;
    mem_devsize = rebin[0].pitch_dim * fold.nchan * NBYTES;
    iolog(1, "host memory size to map: %lld (bytes) fold.nsamp: %lld fold.nchan: %d fold.nsubints: %d\n",
                                                       mem_size, fold.nsamp, fold.nchan, fold.nsubints);
    // set flag to allocate pinned host memory that is accessible to the device.
    cudaSetDeviceFlags(cudaDeviceMapHost);
    // loop on the number of slot to allow a kind of ping-pong
    for (i = 0; i < MAX_SLOT; i ++) {
        if ((Ska_in[i] = (unsigned char*)calloc(mem_size, sizeof(unsigned char))) == NULL) {;
            errlog("(%d) map_host_memory: Failure in allocatic host memory for input data", i);
            return EXIT_FAILURE;
        }
        if ((err = cudaHostAlloc((void **) &H_in[i], mem_devsize, cudaHostAllocMapped)) != cudaSuccess) {
            errlog("(%d) map_host_memory: Failure in allocating memory for input data (%s)", i, cudaGetErrorString(err));
            return EXIT_FAILURE;
        }
        if ((err = cudaHostGetDevicePointer((void **)&D_in[i], H_in[i], 0)) != cudaSuccess) {
            errlog("(%d) map_host_memory: cudaHostGetDevicePointer error (%s)",i, cudaGetErrorString(err));
            return EXIT_FAILURE;
        }
    }
    iolog(1, "host memory map success!!");
    return EXIT_SUCCESS;
}

/*
 * int alloc_device_global(binning_t *rebin)
 */
/**
 *
 * Allocate device global memory to store the profiles in frequency and
 * time of each candidate.
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise
 *
 */
__host__ int alloc_device_global(binning_t *rebin)
{
    uint64_t mem_size = 0;
    uint64_t nsamp_per_subint;
    size_t  pitch_size = 0;
    cudaError_t err = cudaSuccess;      /* cuda error code */

    d_out = 0;  //pointer to device memory for data storing
    nsamp_per_subint = fold.nsamp/fold.nsubints;
    int nrow = nsamp_per_subint /fold.nchan;
    //allocate global device memory to store input converted data
    //iolog(1," alloc_device_global: nsamp_per_subints:%lld mem_size:%lld", nsamp_per_subint, mem_size);

    // here we compute the OPTIMAL pitch (or width) of a 3D memory block. 
    // Later we store the REAL values, which can be different from the optimal one.
    if ((nrow % 128) == 0) {
        rebin[0].pitch_dim = ((int)(nrow /128)) * 128;
    } else {
        rebin[0].pitch_dim = ((int)(nrow /128) + 1) * 128;
    }
    iolog(1, "rebin[0].pitch_dim: %d\n", rebin[0].pitch_dim);
    int start_rebin = first_valid_binidx(rebin);
    if (start_rebin > 5) {
        start_rebin = 5;
    }
    First_rebin_idx = start_rebin;

    // allocate device memory to store input converted data
    for (int i = 0; i < MAX_NREBIN; i ++) {
        if ((is_binentry_valid(i, rebin) == MYTRUE) || (i == start_rebin)) {       // if rebin is a valid element
            nrow = nsamp_per_subint /(fold.nchan * rebin[i].rebin);
            if ((err = cudaMallocPitch((void **)&rebin[i].d_out, &pitch_size,
                                       nrow * sizeof(float), fold.nchan)) == cudaSuccess) {
                rebin[i].msize = pitch_size * fold.nchan;
                if ((err = cudaMemset((void*)rebin[i].d_out, 0, rebin[i].msize)) != cudaSuccess) {
                    errlog("Error in setting to 0 global device memory for input data! (%s)",
                                                                  cudaGetErrorString(err));
                    return EXIT_FAILURE;
                }
                // after data read we convert them to float
                rebin[i].pitch_dim = pitch_size/sizeof(float);
                iolog(1, "rebin factor: %d original row size %d final row size: %d\n", rebin[i].rebin,
                                                                                    nrow,
                                                                                    rebin[i].pitch_dim);
                iolog(1,"alloc_device_global: allocated %lld bytes for sub-integration converted data", rebin[i].msize);
            } else {            // cudaMalloc failure
                errlog("Error in allocating global device memory for input data!  (%s)", cudaGetErrorString(err));
                return EXIT_FAILURE;
            }
        }
    }
    mem_size = fold.ncand * fold.nsubints * fold.nbins * sizeof(float);
    if ((err = cudaMalloc((void **)&D_outfprof, mem_size )) == cudaSuccess) {
        if ((err = cudaMemset((void*)D_outfprof, 0, mem_size)) != cudaSuccess) {
            errlog("Error in setting to 0 global device memory for out profiles! (%s)",
                                                          cudaGetErrorString(err));
            return EXIT_FAILURE;
        }
    } else {            // cudaMalloc failure
        errlog("Error in allocating global device memory for out profiles!  (%s)", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    iolog(1, "alloc_device_global: allocated %lld bytes to store out frequency summed data", mem_size);

    //mem_size = fold.ncand * fold.nbins * fold.nsubints *sizeof(float);
    mem_size = fold.ncand * fold.nbins * sizeof(float);
    if ((err = cudaMalloc((void **)&D_outprof, mem_size )) == cudaSuccess) {
        if ((err = cudaMemset((void*)D_outprof, 0, mem_size)) != cudaSuccess) {
            errlog("Error in setting to 0 global device memory for out profiles! (%s)",
                                                          cudaGetErrorString(err));
            return EXIT_FAILURE;
        }
    } else {            // cudaMalloc failure
        errlog("Error in allocating global device memory for out profiles!  (%s)", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    iolog(1, "alloc_device_global: allocated %lld bytes to store out profiles data", mem_size);

    // allocate global device memory to store profiles of candidates
    //
    // OSS: we use max number of bins (fold.nbins) because the bin value is
    // different for each candidate as it's calculated using the period and
    // the sampling time.  We then allocate the max memory even if it's not
    // used.
    size_t profile_len = fold.nbins * fold.nsubints * fold.nsubbands * sizeof(float);
    mem_size = (uint64_t)profile_len * fold.ncand;

    if ((err = cudaMalloc((void **)&D_folded, mem_size )) == cudaSuccess) {
        if ((err = cudaMemset((void*)D_folded, 0, mem_size)) != cudaSuccess) {
            errlog("Error in cudaMemset global device memory for folded profiles! (%s)",
                                                               cudaGetErrorString(err));
            return EXIT_FAILURE;
        }
    } else {            // cudaMalloc failure
        errlog("Error in allocating global device memory for folded data!  (%s)", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    iolog(2," alloc_device_global: fold.nsamp=%lld fold.nsubints=%d mem_size=%lld", fold.nsamp,
                                                                      fold.nsubints, mem_size);

    iolog(1, "alloc_device_global: allocated %lld bytes to store sub-integration folded data", mem_size);
    if ((err = cudaMalloc((void **)&d_weight, mem_size )) == cudaSuccess) {
        if ((err = cudaMemset((void*)d_weight, 0, mem_size)) != cudaSuccess) {
            iolog(1, "alloc_device_global: allocated %lld bytes to store sub-integration weight data", mem_size);
            return EXIT_FAILURE;
        }
    } else  {
        errlog("Error in allocating global device memory to store sub-integration weight data (%d)", err);
        return EXIT_FAILURE;
    }
    mem_size = fold.nsubbands * fold.nsubints * fold.ncand * sizeof(double);
    if ((err = cudaMalloc((void **)&d_phase_shift, mem_size)) == cudaSuccess) {
        if ((err = cudaMemset((void*)d_phase_shift, 0, mem_size)) != cudaSuccess) {
            iolog(1, "alloc_device_global: allocated %lld bytes to store optimization phase shifts", mem_size);
            return EXIT_FAILURE;
        }
    } else {            // cudaMalloc failure
        errlog("Error in allocating global device memory for optimization phase shifts! (%s)",
                                                                     cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    iolog(1, "alloc_device_global: allocated device memory for weights!");
    return EXIT_SUCCESS;
}

/*
 * int alloc_host_memory(binning_t *rebin)
 */
/**
 *
 * Allocate host global memory to store the profiles in frequency and
 * time of each candidate. Only for test analysis!!
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise
 *
 */
__host__ int alloc_host_memory(binning_t *rebin)
{

    uint64_t mem_size = 0;
    cudaError_t err = cudaSuccess;      // cuda error code

    if (EnableCheck == MYTRUE) {
        int nbinning = -1;

        nbinning = is_binvalue_valid(fold.prebin, rebin);
        if (nbinning < 0) {
            nbinning = first_valid_binidx(rebin); // get the first valid binning value
        }
        mem_size = rebin[nbinning].msize;
        // allocate host memory to copy input converted data (only for test  purpose)
        if ((err = cudaHostAlloc(&Host_mtx, mem_size, cudaHostAllocDefault)) == cudaSuccess) {
            if (memset(Host_mtx, 0, mem_size) == NULL) {
                errlog("Error in memset global host memory to check output converted data! (%s)",
                                                              cudaGetErrorString(err));
                return EXIT_FAILURE;
            }
            iolog(1," alloc_host_memory: allocated %lld bytes to check input data", mem_size);
        }
    }
    if (EnableWrite == MYTRUE) {
        // allocate host memory to copy profile (only for test analysis)
        mem_size = fold.ncand * fold.nsubints * fold.nbins * sizeof(float);
        if ((err = cudaHostAlloc(&H_outfprof, mem_size, cudaHostAllocDefault)) == cudaSuccess) {
            if (memset(H_outfprof, 0, mem_size) == NULL) {
                errlog("Error in memset global host memory to check output freq. profiles data! (%s)",
                                                              cudaGetErrorString(err));
                return EXIT_FAILURE;
            }
            mem_size = fold.ncand * fold.nbins * sizeof(float);
            if ((err = cudaHostAlloc(&H_outprof, mem_size, cudaHostAllocDefault)) == cudaSuccess) {
                if (memset(H_outprof, 0, mem_size) == NULL) {
                    errlog("Error in memset global host memory to check output profiles data! (%s)",
                                                                  cudaGetErrorString(err));
                    return EXIT_FAILURE;
                }
                size_t profile_len = fold.nbins * fold.nsubints * fold.nsubbands * sizeof(float);
                mem_size = (uint64_t)profile_len * fold.ncand;
                if ((err = cudaHostAlloc(&H_folded, mem_size, cudaHostAllocDefault)) == cudaSuccess) {
                    if (memset(H_folded, 0, mem_size) == NULL) {
                        errlog("Error in memset global host memory to check folded profiles"
                                                  " data! (%s)", cudaGetErrorString (err));
                        return EXIT_FAILURE;
                    }
                    return EXIT_SUCCESS;
                }
            }
        }
        errlog("alloc_host_memory: error in allocating host memory.(%s)", cudaGetErrorString (err));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

/**
 * int allocate_mem4best(int nelem)
 *
 * \param nelem         the total space dimension for each iter in the optimization process
 *
 * Allocate the device and host memory to store the optimized results.
 *
 * \param nelem         the number of trials in optimization phase
 */
int allocate_mem4best(int nelem)
{
    int memsize = nelem * fold.ncand *sizeof(float);
    cudaError_t err = cudaSuccess;

    // OSS: the number of optimization trials may change from one call to
    // the other and so the memory allocated may change in size. For this
    // reason we free and then re-allocate the memory needed for the
    // optimization procedure.
    if (D_bestprof && D_best) {
        iolog(1, "dbestprof and D_best already allocated!! Release memory and re-allocatei it\n");
        cudaFree(D_bestprof);
        cudaFree(D_best);
        D_bestprof = 0;
        D_best = 0;
    }
    // allocate a device array to store the profiles for each candidate,
    // for each trial
    memsize = fold.nbins * nelem * fold.ncand *sizeof(float);
    if ((err = cudaMalloc((void **)&D_bestprof, memsize)) == cudaSuccess) {
        if ((err = cudaMemset((void*)D_bestprof, 0, memsize)) != cudaSuccess) {
            errlog("Error in setting to 0 global device memory for best out data! (%s)", cudaGetErrorString(err));
            return EXIT_FAILURE;
        }
        //for each trial we store 2 values: the S/N and the width
        memsize = nelem * 2 * fold.ncand *sizeof(float);
        iolog(1,"nelem: %d\n", nelem);
        if ((err = cudaMalloc((void **)&D_best, memsize)) == cudaSuccess) {
            if ((err = cudaMemset((void*)D_best, 0, memsize)) != cudaSuccess) {
                errlog("Error in setting to 0 global device memory for best out data! (%s)", cudaGetErrorString(err));
                return EXIT_FAILURE;
            }
            if ((err = cudaHostAlloc(&Host_best, memsize, cudaHostAllocDefault)) == cudaSuccess) {
                if (memset(Host_best, 0, memsize) == NULL) {
                    errlog("Error in memset global host memory to check output sn values! (%s)",
                                                                  cudaGetErrorString(err));
                    return EXIT_FAILURE;
                }
                return EXIT_SUCCESS;
            }
        }
    }
    errlog(" allocate_mem4best: error in allocating global device memory");
    return EXIT_FAILURE;
}

/**
 * int alloc_device_constant(double *delta_freq, candidate_t *candidate, double *time_sum, double *freq_sum)
 *
 * Allocate the device constant memory to store the values used to
 * calculate the dispersion.
 *
 * \param delta_freq    tabulated K_DM * 1/(v0^2 - v^2) values for all * freq. channels
 * \param candidate     array with candidate_t structures
 * \param time_sum      array with the sub-integration mean time
 * \param freq_sum      array with the sub-band mean frequency
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise
 */
__host__ int alloc_device_constant(double *delta_freq, candidate_t *candidate, double *time_sum,
                                   double * freq_sum)
{
    int err_cnt = 0;
    cudaError_t err = cudaSuccess;      /* cuda error code */

    int nchan_per_band = (int)(fold.nchan/fold.nsubbands);
    int ndev = 0;
    err_cnt = 0;

    //allocate memory to store candidates info to copy in GPU costant memory
    double *nu = (double*) malloc(fold.ncand * sizeof(double));
    double *nudot = (double*) malloc(fold.ncand * sizeof(double));
    float  *dm = (float*) malloc(fold.ncand * sizeof(float));
    int *phase_bin = (int*) malloc(fold.ncand * sizeof(int));

    //check allocation memory
    if ((nu == NULL) || (nudot == NULL) || (dm == NULL) || (phase_bin == NULL)) {
        errlog("alloc_device_constant: error in allocataing memory");
        return EXIT_FAILURE;
    }

    // fill the arrays with the pulsars data
    for (int ncand = 0; ncand < fold.ncand; ncand++) {
        nu[ncand] = candidate[ncand].nu;
        nudot[ncand] = candidate[ncand].nudot;
        dm[ncand] = candidate[ncand].dm;
        phase_bin[ncand] = candidate[ncand].phase_bin;
    }
    cudaGetDevice(&ndev);
    iolog(1, "constant_memory: nchan: %d ncand: %d device: %d\n", fold.nchan, fold.ncand, ndev);

    if ((err = cudaMemcpyToSymbol(_delta_freq, delta_freq, fold.nchan * sizeof(double), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    // candidate freqs
    if ((err = cudaMemcpyToSymbol(_nu, nu, fold.ncand * sizeof(double), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    free(nu);
    // candidate dot freqs
    if ((err = cudaMemcpyToSymbol(_nudot, nudot, fold.ncand * sizeof(double), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    free(nudot);
    // candidates DMs
    if ((err = cudaMemcpyToSymbol(_dm, dm, fold.ncand * sizeof(float), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    free(dm);
    // candidates phases
    if ((err = cudaMemcpyToSymbol(_nbins, phase_bin, fold.ncand * sizeof(int), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    free(phase_bin);
    // subint time mean value
    if ((err = cudaMemcpyToSymbol(_time_sum, time_sum, fold.nsubints * sizeof(double), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    // subband freq mean value
    if ((err = cudaMemcpyToSymbol(_freq_sum, freq_sum, fold.nsubbands * sizeof(double), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    // number of freq. channels per band
    if ((err = cudaMemcpyToSymbol(_nchan_per_band, &nchan_per_band, sizeof(int), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    // number of sub-integrations
    if ((err = cudaMemcpyToSymbol(_nsubints, &fold.nsubints, sizeof(int), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    // sampling time
    // OSS: the prebin value is taken into account inside the kernel
    // function.
    if ((err = cudaMemcpyToSymbol(_tsamp, &fold.tsamp,  sizeof(double), 0, cudaMemcpyHostToDevice)) != cudaSuccess) {
        err_cnt++;
        goto end;
    }
    return EXIT_SUCCESS;
end:
    errlog("(%d) Failure in cudaMemcpyToSymbol (%s)", err_cnt, cudaGetErrorString(err));
    if (nu)
        free(nu);
    if (nudot)
        free(nudot);
    if (dm)
        free(dm);
    if (phase_bin)
        free(phase_bin);
   return EXIT_FAILURE;
}

/*
 * int allocate_memory(int ndev, binning_t *rebin, double *delta_freq, * candidate_t *candidate,
 *                     double *time_sum, double *freq_sum)
 */
/**
 *
 * Allocate chuncks of host memory to be mmaped.
 *
 * \param ndev    the GPU device number (0, 1 ...)
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE on errors.
 */
extern "C" int allocate_memory(int ndev, binning_t *rebin, double*
        delta_freq, candidate_t* candidate, double * time_sum, double *freq_sum)
{
    double start = 0.;
    cudaError_t err = cudaSuccess;

    iolog(1, "allocating memory on device :GPU-%d", ndev);
    start = get_current_time();
    // set the device to use
    if ((err = cudaSetDevice(ndev)) != cudaSuccess) {
        errlog("allocate_memory: cudaSetDevice error (%s)", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    if (alloc_device_constant(delta_freq, candidate, time_sum, freq_sum) == EXIT_SUCCESS) {
        if (alloc_device_global(rebin) == EXIT_SUCCESS) {
            if (map_host_memory(rebin) == EXIT_SUCCESS) {
                if (alloc_host_memory(rebin) == EXIT_SUCCESS) {
                    iolog(1, "Time to allocate memory: %f (ms)\n", get_current_time() - start);
                    return EXIT_SUCCESS;
                }
            }
        }
    }
    release_memory(ndev, rebin);
    return EXIT_FAILURE;
}

/*
 * void release_memory(int ndev, binnin_t *rebin)
 *
 * Release the memory allocated both on device and host.
 *
 */
extern "C" void release_memory(int ndev, binning_t * rebin)
{
    int i;
    cudaError_t err = cudaSuccess;      /* cuda error code */

    // set the device to use
    cudaSetDevice(ndev);
    iolog(1, "release allocated memory for GPU-%d\n", ndev);
    for (i = 0; i < MAX_SLOT; i++) {
        if (H_in[i]) {
            if ((err = cudaFreeHost(H_in[i])) != cudaSuccess) {
                errlog("release_memory: cudaFreeHost error (%s)", cudaGetErrorString(err));
            }
            H_in[i] = 0;
        }
        if (Ska_in[i]) {
            free(Ska_in[i]);
            Ska_in[i] = 0;
        }
    }
    // release global device memory
    if (D_folded) {
        cudaFree(D_folded);
    }
    if (d_weight) {
        cudaFree(d_weight);
    }
    if (D_outprof) {
        cudaFree(D_outprof);
    }
    if (D_outfprof) {
        cudaFree(D_outfprof);
    }
    if (D_bestprof) {
        cudaFree(D_bestprof);
    }
    if (D_best) {
        cudaFree(D_best);
    }
    if (d_phase_shift) {
        cudaFree(d_phase_shift);
    }
    //release host memory allocated for final results
    if (Host_best) {
        cudaFreeHost(Host_best);
        Host_best = 0;
    }
    //release host memory allcated only when check enabled
    // (test purpose)
    if (EnableCheck == MYTRUE) {
        if (Host_mtx) {
            cudaFreeHost(Host_mtx);
            Host_mtx = 0;
        }
    }
    if (EnableWrite == MYTRUE) {
        if (H_outfprof) {
            cudaFreeHost(H_outfprof);
        }
        if (H_outprof) {
            cudaFreeHost(H_outprof);
        }
        if (H_folded) {
            cudaFreeHost(H_folded);
        }
    }
    if (Opt_period) {
        free (Opt_period);
    }
    if (Opt_dm) {
        free(Opt_dm);
    }
    if (Opt_pdot) {
        free(Opt_pdot);
    }
    return;
}

/*
 * void release_streams(int ndev, int nstreams)
 *
 * Destroy and clean up the asynchronous streams allocated to process each
 * candidate in GPU.
 *
 */
extern "C" void release_streams(int ndev, int nstreams)
{
    cudaStream_t *streams = Gpus[ndev].streams;

    iolog(1, "release allocated streams for GPU-%d\n", ndev);
    // set the device to use
    cudaSetDevice(ndev);
    if (streams) {
        for (int i = 0; i < nstreams; i++) {
            if (streams[i] != NULL) {
                if (cudaStreamDestroy(streams[i]) != cudaSuccess) {
                    errlog("Failure in cudaStreamDestroy (%d)", i);
                    break;
                }
            }
        }
        free(streams);
    }
    return;
}

/*
 * int reset_device(int ndev)
 *
 * Destroy and clean up all resources associated with the device in the
 * current process.
 *
 * \return EXIT_SUCCESS on success, otherwise EXIT_FAILURE
 */
extern "C" int reset_device(int ndev)
{
    cudaError_t err = cudaSuccess;
    iolog(1, "reset GPU-%d\n", ndev);
    cudaSetDevice(ndev);
    if ((err = cudaDeviceReset()) != cudaSuccess) {
        errlog("reset_Device: Failed to release resources (%s)\n", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

/*
 * int init_streams(int ndev, int nstreams)
 *
 * Create asynchrnous streams to execute GPU kernels.
 *
 * \param ndev          the device
 * \param nstreams      the number of asynchronous streams.

 * \return EXIT_SUCCESS on success, otherwise EXIT_FAIURE.
 *
 */
extern "C" int init_streams(int ndev, int nstreams)
{
    cudaError_t err = cudaSuccess;
    cudaStream_t *streams = 0;

    iolog(2, "init_streams: device: %d nstreams: %d", ndev, nstreams);
    // set the device to use
    if ((err = cudaSetDevice(ndev)) != cudaSuccess) {
        errlog("init_streams: Failure in cudaSetDevice: (%s) %d", cudaGetErrorString(err), err);
        return EXIT_FAILURE;
    }
    // allocate as many streams as the number of candidates
    streams = (cudaStream_t *) malloc(nstreams * sizeof(cudaStream_t));
    Gpus[ndev].streams = streams;
    if (streams == NULL) {
        errlog("init_steams: error in allocating host memory for CUDA streams!");
        return EXIT_FAILURE;
    }
    for (int ncand = 0; ncand < nstreams; ncand ++) {
        err = cudaStreamCreate(&(streams[ncand]));
        if (err != cudaSuccess) {
            release_streams(ndev, nstreams);
            errlog("Failed in creating stream %d", ncand);
            return EXIT_FAILURE;
        }
    }
    iolog(1, "Created  %d streams", nstreams);
    return EXIT_SUCCESS;
}

/*
 * void release_rebindev_resources(int ndev, int nstreams)
 */
extern "C" void release_rebindev_resources(int ndev, binning_t* rebin)
{
    int i = 0 ;
    // set the device to use
    cudaSetDevice(ndev);
    for (i = 0; i < MAX_NREBIN; i++) {
        if (is_binentry_valid(i, rebin) == MYTRUE) {
            if (rebin[i].d_out) {
                cudaFree(rebin[i].d_out);
            }
            if (rebin[i].stream) {
                cudaStreamDestroy(rebin[i].stream);
            }
            if (rebin[i].event) {
                cudaEventDestroy(rebin[i].event);
            }
        }
    }
    return;
}

/*
 * void init_rebindev_resources(int ndev,  binning_t *rebin)
 *
 * Create a GPU stream and event for each rebin entry.
 *
 * \param ndev          the gpu device
 * \param rebin         the array of rebin structures
 *
 * \return EXIT_SUCCESS on success, otherwise EXIT_FAIURE.
 *
 * Create a GPU stream for each rebin entry.
 */
extern "C" int init_rebindev_resources(int ndev, binning_t *rebin)
{
    int i = 0;
    if (cudaSetDevice(ndev) == cudaSuccess) {
        for (i = 0; i < MAX_NREBIN; i++) {
            if ((cudaEventCreateWithFlags(&rebin[i].event, cudaEventDisableTiming) != cudaSuccess) ||
                (cudaStreamCreate(&rebin[i].stream) != cudaSuccess)) {
                release_rebindev_resources(ndev, rebin);
                return EXIT_FAILURE;
            }
        }
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}


/*
 * int core_per_sm(int major, int minor)
 *
 * Return the number of core per streaming multiprocessor.
 *
 */
int core_per_sm(int major, int minor)
{

    // Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
    typedef struct
    {
        int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
        int Cores;
    } sSMtoCores;

    sSMtoCores nGpuArchCoresPerSM[] =
    {
        { 0x10,  8 }, // Tesla Generation (SM 1.0) G80 class
        { 0x11,  8 }, // Tesla Generation (SM 1.1) G8x class
        { 0x12,  8 }, // Tesla Generation (SM 1.2) G9x class
        { 0x13,  8 }, // Tesla Generation (SM 1.3) GT200 class
        { 0x20, 32 }, // Fermi Generation (SM 2.0) GF100 class
        { 0x21, 48 }, // Fermi Generation (SM 2.1) GF10x class
        { 0x30, 192}, // Kepler Generation (SM 3.0) GK10x class
        { 0x35, 192}, // Kepler Generation (SM 3.5) GK11x class
        {   -1, -1 }
    };

    int index = 0;

    while (nGpuArchCoresPerSM[index].SM != -1)
    {
        if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor)) {
            return nGpuArchCoresPerSM[index].Cores;
        }
        index++;
    }

    // If we don't find the values, we default use the previous one to run properly
    iolog(1, "MapSMtoCores for SM %d.%d is undefined.  Default to use %d Cores/SM\n", major, minor,
                                                                      nGpuArchCoresPerSM[7].Cores);
    return nGpuArchCoresPerSM[7].Cores;
}

/*
 * int gpu_init(void)
 */
/**
 *
 * Get and store in a structure the device properties of the GPU(s)
 * detected.
 *
 * \return the number of gpu supporting CUDA on success, otherwise 0.
 *
 */
extern "C" int gpu_init(void)
{
    int i;
    int ndevice = 0;    // number of GPU device present
    int ngpu = 0;
    cudaError_t error;
    struct cudaDeviceProp deviceProp;

    // get the number of GPU with compute capability >= 1.0
    error = cudaGetDeviceCount(&ndevice);
    if (error != cudaSuccess) {
        errlog("cudaGetDeviceCount failure: %d\n", error);
        return -1;
    }
    iolog(1, "Found %d GPU(s)\n", ndevice);
    for (i = 0; i < ndevice; i++) {
        deviceProp.major = 0;
        deviceProp.minor = 0;
        error = cudaGetDeviceProperties(&deviceProp, i);
        if (error != cudaSuccess) {
            errlog("cudaGetDeviceProperties failure: %d\n", error);
            return -1;
        }
        if (deviceProp.computeMode == cudaComputeModeProhibited) {
            errlog("Error: device is running in <Compute Mode Prohibited>, no threads "
                            "can use ::cudaSetDevice().\n");
            return -1;
        }
        if (deviceProp.major < 1) {
            errlog("gpu_init(): GPU device does not support CUDA.\n");
            return -1;
        }
        int sm_per_multiproc = core_per_sm(deviceProp.major, deviceProp.minor);

        // found NVIDIA gpu with compute capability >= 1.0
        iolog(1, "GPU: %s\n", deviceProp.name);
        iolog(1, "compute capability: %d.%d\n", deviceProp.major, deviceProp.minor);
        iolog(1, "global Memory: %u (MB)\n", (unsigned long)(deviceProp.totalGlobalMem/(1024.*1024.)));
        iolog(1, "compute mode: %d", deviceProp.computeMode);
        if ((deviceProp.concurrentKernels != 0)) {
            iolog(1, "> GPU supports concurrent kernel execution");
        }
        Gpus[i].max_thread_per_block = deviceProp.maxThreadsPerBlock;
        Gpus[i].max_thread_dim[0] = deviceProp.maxThreadsDim[0];
        Gpus[i].max_thread_dim[1] = deviceProp.maxThreadsDim[1];
        Gpus[i].max_thread_dim[2] = deviceProp.maxThreadsDim[2];
        Gpus[i].warp_size = deviceProp.warpSize;
        Gpus[i].sharedMemPerBlock = deviceProp.sharedMemPerBlock;
        iolog(1, "%d cores/SM\n", sm_per_multiproc);
        ngpu ++;
    }
    return ngpu;
}

