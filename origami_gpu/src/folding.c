/* $Id: folding.c 1209 2020-02-18 10:07:25Z baffa $ */

#include <signal.h>
#include <folding.h>
#include <math.h>
#include "../config.h"

//#define TRANSPOSE_CHECK 1     //to enable check on transpose matrix

// local variables
// local global variables start with '_' and capital letter
extern char* optarg;
int *_Nbins                     = 0;            // local array to store the max bins for each candidate
double _StartTime               = 0.;           // start time to get CPU total exec. time
char *_Base_filename            = 0;            // data file filename without extension
char _Cand_filename[240];                       // file with list of pulsar candidates
double *_Freq_sum               = 0;            // local array with the central freq. of each subband
double *_Time_sum               = 0;            // local array with the central time of each subinteg.
binning_t _Rebin[MAX_NREBIN];                   // array of rebin structures
candidate_t *_Candidate         = 0;            // ordered structure with candidates
Shared_t _Shared;                               // Shared_t structure with lock to access data
void *_Sem_end                   = NULL;        // semaphore to signal end of file reading

//global variables
//
int EnableCheck                 = MYFALSE;      // flag to enable/disable output data check
int EnableWrite                 = MYFALSE;      // flag to enable/disable output data check
int EnableSplit                 = MYFALSE;      // flag to enable/disable folding phase split
int FixedRebin                  = MYFALSE;      // flag to enable/disable variable rebinning
int Header                      = MYFALSE;      // (flag) Header is present?
int HeaderOffset                = 0;            // Offset due to header presence (default 242)
int Ngpu                        = 1;            // number of devices (default=1)
int Nthread                     = 1;            // number of threads for device (default=1)
int Rebin                       = 1;            // fixed rebing if MYTRUE == FixedRebin
int RebinStrategy               = 1;            // selecto of pre-computed rebinning strategy
int First_rebin_idx             = 0;            // first rebin index
int Debug                       = 0;            // verbosity output
int Keepgoing                   = 1;            // flag to handle loop break
int Logging                     = MYFALSE;      // flag to enable/disable logging on file
int Optimization                = O_2D;         // optimization space dimension: default 2D
int Tobs_forced                 = MYFALSE;      // flag to enable forcing of tobs
float Gpu_time                  = 0.;           // total GPU exec time
float Gpu_fold                  = 0.;           // total GPU folding time
struct gpu_info Gpus[MAX_GPUDEVICES];
struct sigaction termhandler, inthandler;
args_option_t fold;                             // folding parameters
args_option_t head_fold;                        // from data header folding parameters
FILE *LogFptr = 0;                              // pointer to log file
char _Inputfile[220];                           // custom input data file name
//host and device memory
unsigned char *H_in[MAX_SLOT];                  // array with transposed input data
unsigned char *Ska_in[MAX_SLOT];                // array with input data in natural order
unsigned char *D_in[MAX_SLOT];                  // device array with transposed input data

// static local variables
static int EndRead              = 1;            // flag to signal end of read

static struct option long_options[] =
{
    {"log",     no_argument, &Logging, 1},
    {"ngpu",    required_argument, NULL,'g'},
    {"tsamp",   required_argument, NULL,'t'},
    {"tobs",    required_argument, NULL,'T'},
    {"nsamp",   required_argument, NULL, 'n'},
    {"nchan",   required_argument, NULL, 'c'},
    {"ncand",   required_argument, NULL, 'C'},
    {"fch1",    required_argument, NULL,'f'},
    {"foff",    required_argument, NULL,'b'},
    {"period",  required_argument, NULL, 'p'},
    {"pdot",    required_argument, NULL, 'P'},
    {"dm",      required_argument, NULL, 'd'},
    {"nbins",   required_argument, NULL, 'B'},
    {"nsubint", required_argument, NULL, 'i'},
    {"nsubband", required_argument, NULL,'s'},
    {"debug",   required_argument, NULL, 'v'},
    {"nosplit", no_argument, &EnableSplit, 'N'},
    {"optimize", required_argument, NULL, 'o'},
    {"enable",  no_argument, &EnableCheck, 1},
    {"write",   no_argument, &EnableWrite, 1},
    {"help",   no_argument, NULL, 'h'},
    {NULL, 0, NULL, 0}
};

/*
 * void print_help(char *program_name)
 *
 * Print the program options.
 *
 */
void print_help(const char * program_name)
{
    printf("Usage:  %s options [ inputfile ... ]\n", program_name);
    printf( "  -h  --help              Display this usage information.\n"
            "  -v  --verbose  flag     Print verbose messages                   (Default 0)\n"
            "  -g  --ngpu     num      the number of gpu in use                 (Default 1)\n"
            "  -t  --tsamp    time     the sampling time (usec)                 (Default 50)\n"
            "  -T  --tobs     time     the observ. time (sec)          [1- 600] (Default 60)\n"
            "  -F  --input    file     the input data file                      (Default ska.dat)\n"
            "  -n  --nsamp    num      the number of samples                    (Default 12e5)\n"
            "  -c  --nchan    num      the number freq. channels      [64-4096] (Default 4096) \n"
            "  -C  --ncand    num      the number of pulsar candidates [1- 256] (Default 128) \n"
            "  -f  --fch1     val      the first channel freq (MHz)             (Default 1550)\n"
            "  -b  --foff     val      channel bandwith (MHz)                   (Default 0.075)\n"
            "  -p  --period   val      pulsar period (ms)                       (Default 1.1236)\n"
            "  -P  --pdot     val      pulsar pdot (ms/s)                       (Default 0.)\n"
            "  -d  --dm       val      dispersion measure                       (Default 10)\n"
            "  -B  --nbins    val      max folding bins                [16-256] (Default 128)\n"
            "  -r  --rebin    val      pre-folding rebin (expon. of 2) [1 - 16] (Default 1)\n"
            "  -R  --Rebin    val      pre-folding rebin strategy      [1 -  2] (Default 2)\n"
            "  -i  --nsubint  val      folding sub integ               [64-256] (Default 64)\n"
            "  -s  --nsubband val      folding subbands                [1 -128] (Default 64) \n"
            "  -o  --optimize          select optimization dimension   [2,3,5]  (Default 2) 5 = 2D & 3D \n"
            "  -N  --nosplit           toggle  folding phase shift              (Default no split) \n"
            "  -e  --enable   val      enable data check for rebin val [1 - 16] (Default  1) \n"
            "  -l  --log               logging on file                          (Default off) \n"
            "  -w  --write             enable output profile writing            (Default off) \n"
          );
}

/*
 * void initvar()
 *
 * Initialize the global variables with defautl values.
 *
 */
void initvar(void)
{
    Gpu_time = 0;               // total GPU exec time
    Gpu_fold = 0;               // total GPU folding time
    Debug = 0;                  // flag to enable/disable verbose printing
    Nthread = 1;                // numer of CPU threads
    Ngpu = 1;                   // number of GPU devices
    Keepgoing = 1;              // flag to catch SIGTERM
    EndRead   = 1;              // flag for end of read
    fold.infile = INPUTFILE;    // pointer to input data file name
    Tobs_forced = MYFALSE;      // default is to get tobs from filedata size
    Logging = MYFALSE;
    RebinStrategy= 2;           // default strategy
    Optimization = O_2D;        // optimization strategy: default 2D (3=3D, 5=2D and 3D)
    EnableCheck = MYFALSE;      // to enable check of input data
    EnableWrite = MYFALSE;      // to enable device->host write for test
    fold.nchan = O_FREQUENCIES; // number of freq. channels
    fold.tsamp = O_TSAMPLING;   // Sampling time in sec
    fold.tobs  = O_TIMEOBS;     // Observing time in sec
    fold.nsamp = 0;             // number of samples
    fold.ncand = O_CANDIDATES;  // number of candidates
    fold.period = 1.1234e-3,    // pulsar period in sec.
    fold.dm     = 10.0;         // dedispersion measure
    fold.pdot   = 0.0;
    fold.prebin = O_PREBIN;     // pre-folding binning
    fold.nbins  =   O_PHASES;   // number of phase for binning
    fold.nsubints = O_SUBINT;   // number of sub integrations
    fold.nsubbands = O_BANDS;   // number of frequencies subbands
    fold.freq_ch1 = 1550e6;     // first channel freq. in Hz
    fold.freq_off = -0.075e6;   // first channel freq. in Hz
    fold.nsamp = (uint64_t)(fold.tobs/fold.tsamp+0.5) * fold.nchan;
    head_fold = fold;           // to initialize also head_fold with sensible values. 
    //printf("Data file in head_fold     = %s\n",  head_fold.infile);
}

/*
 * void printvar(void)
 *
 * Print the variables used for folding.
 *
 */
void printvar(void)
{
    printf("Data file                  = %s\n",  fold.infile);
    printf("Candidates file            = %s\n",  _Cand_filename);
    printf("Frequency of channel 1     = %f Mhz\n",  fold.freq_ch1/1.e6);
    printf("Channel bandwidth          = %f Mhz\n",  fold.freq_off/1.e6);
    printf("Number of channel          = %d\n",  fold.nchan );
    printf("Sampling time  (s)         = %f\n",  fold.tsamp );
    printf("Number of samples          = %ld\n",  fold.time_nsamp);
    printf("Observation length (s)     = %f\n",  fold.time_nsamp * fold.tsamp);
    printf("Obs time for folding (s)   = %f\n",  fold.tobs);
    printf("Number of folded samples   = %ld\n", fold.nsamp/fold.nchan);
    printf("Number of candidates       = %d\n",  fold.ncand );
    printf("Number of phase bins       = %d\n",  fold.nbins  );
    printf("Number of sub-ints         = %d\n",  fold.nsubints );
    printf("Number of sub-bands        = %d\n",  fold.nsubbands );
    printf("Prebinning on time samples = %d\n",  fold.prebin  );
    printf("Optimization strategy      = %dD\n", Optimization);
    if (EnableSplit == MYTRUE) {
        printf("Phase bin split enabled\n");
    } else {
        printf("Phase bin split disabled");
    }
    printf("\n\n");
}

/*
 * int handle_options(int argc, char *argv[])
 *
 * Handle user options.
 *
 * \param argc          the number of options
 * \param argv          the list of options
 *
 * \return 0 on success, otherwise < 0
 */
int handle_options(int argc, char *argv[])
{
    int opt = 0;
    int long_index =0;
    int val;            // to store integer option value
    double fval;        // to store float option value
    uint64_t val_64;    // to store int64 option value
    const char* program_name;
    Tobs_forced = MYFALSE;      // default is to get tobs from filedata size

    program_name = argv[0];
    while ((opt = getopt_long(argc, argv, "g:t:T:F:n:c:C:f:b:p:P:d:B:i:s:v:r:R:o:e:Nwlh",
                    long_options, &long_index )) != -1) {
        switch (opt) {
            // If flag is not a null pointer, that means this option should
            // just set a flag in the program. In this case, getopt_long
            // returns 0
            case 0:
                if (long_options[long_index].flag != 0)
                    break;
                if (optarg) {
                    printf (" with arg %s\n", optarg);
                }
                break;
            case 'g' :
                val = atoi(optarg);
                if ((val < 1) || (val > 32)) {
                    val = 1;
                }
                iolog(1, "handle_options: Forcing number of GPU ");
                Ngpu = val;
                break;
            case 't' :                  /* sampling time */

                fval = atof(optarg);
                if (fval < 0.) {
                    fval = 50; // sound default value.
                }
                /* convert from usec to sec */
                fold.tsamp = fval * 1e-6;
                iolog(1, "handle_options: Forcing sampling time");
                break;
            case 'T' :                  /* obs time */
                fval = atof(optarg);
                if (fval < 0.) {
                    fval = O_TIMEOBS;
                }
                fold.tobs = fval;
                Tobs_forced = MYTRUE ;      // default is to get tobs from filedata size
                iolog(1, "handle_options: Forcing Tobs");
                break;
            case 'n' :                  /* number of samples */
                val_64 = (uint64_t)atoll(optarg);
                if ((val_64 < 0) || (val_64 > (uint64_t)32*1024*1024*1024)) {
                    val_64 = 1024*10124/4;
                }
                fold.nsamp = val_64;
                iolog(1, "handle_options: Forcing number of samples ");
                break;
            case 'c' :                  /* number of freq. channels */
                val_64 = (uint64_t)atoi(optarg);

                if ((val_64 < MINFREQUENCIES) || (val_64 > MAXFREQUENCIES)) {
                    val_64 = O_FREQUENCIES;
                }
                fold.nchan = val_64;
                iolog(1, "handle_options: Forcing number of freq. channels ");
                break;
            case 'C' :                  /* number of candidates */
                val = atoi(optarg);
                if ((val < 0) || (val > MAXCANDIDATES)) {
                    val = O_CANDIDATES;
                }
                fold.ncand = val;
                iolog(1, "handle_options: Forcing number of candidates ");
                break;
            case 'f' :                  /* central freq. (Mhz) */
                fval = atof(optarg);
                if (fval < 0.) {
                    printf("Invalid freq value: %f\n", fval);
                    return -1;
                }
                /* from Mhz-> to Hz */
                fold.freq_ch1 = fval * 1e6;
                iolog(1, "handle_options: Forcing central frequency");
                break;
            case 'b' :                  /* channel freq. bandwidth (Mhz) */
                fval = atof(optarg);
                //15/04/2015
                //fval can be negative, because the first freq. is
                //generally the higher one
                // Here we should put the limit for folding (20KHz-75KHz)
                // but for the moment we don't do it because we want to
                // test the program against true data.

                //if (fval < 0.) {
                //    printf("Invalid freq_off value: %f\n", fval);
                //    return -1;
                //}
                /* from Mhz-> to Hz */
                fold.freq_off = fval * 1e6;
                iolog(1, "handle_options: Forcing frequency Offset");
                break;
            case 'p' :                  /* pulsar period (ms)   */
                fval = atof(optarg);
                if (fval < 0.) {
                    printf("Invalid period: %f\n", fval);
                    return -1;
                }
                /* from msec. to sec. */
                fold.period = fval * 1e-3;
                iolog(1, "handle_options: Forcing folding period: %f", fold.period);
                break;
            case 'P' :                  /* pulsar pdot (ms/s) */
                fval = atof(optarg);
                if (fval < 0.) {
                    printf("Invalid pdot value: %f\n", fval);
                    return -1;
                }
                /* from msec./sec to sec./sec */
                fold.pdot = fval * 1e-3;
                iolog(1, "handle_options: Forcing pdot");
                break;
            case 'F' :                  /* the input data file (Default ska.dat)"*/
                if (((strlen(optarg) < 4) || (strlen(optarg) > (180)))
                    || (0 > access(optarg, R_OK))) {
                    printf("Invalid input file: %s\n", optarg);
                    if (strlen(optarg) > (180)) {
                        printf("Input filename too long\n");
                    }
                    return -1;
                }
                /* we use optarg value */
                strncpy(_Inputfile, optarg, sizeof(_Inputfile));
                fold.infile = & (_Inputfile[0]);
                iolog(1, "handle_options: Forcing Input file: %s\n", fold.infile);
                break;
            case 'd' :                  /* pulsar dm */
                fval = atof(optarg);
                if (fval < 0.) {
                    printf("Invalid dispersion measure value: %f\n", fval);
                    return -1;
                }
                fold.dm   = fval;
                iolog(1, "handle_options: Forcing folding dm: %f", fold.dm);
                break;
            case 'r' :                  /* fixed rebinning */
                val = atoi(optarg);
                if ((val < 1) || (val >256))  {
                    // we do not check for a power of 2!!
                    printf("Invalid fixed rebinning: %d\n", val);
                    return -1;
                }
                Rebin      = val;
                FixedRebin = MYTRUE;
                iolog(1, "handle_options: Forcing fixed rebinning: %d", Rebin);
                break;
            case 'R' :                  /* fixed rebinning */
                RebinStrategy = 1;
                val = atoi(optarg);
                if ((val < 1) || (val > 2))  {
                    printf("Invalid rebinning strategy: %d\n", val);
                    return -1;
                }
                RebinStrategy = val;
                iolog(1, "handle_options: Forcing rebinning strategy: %d", RebinStrategy);
                break;
            case 'B' :                  /* number of pahses */
                val_64 = (uint64_t)atoi(optarg);
                if ((val_64 < 16) || (val_64 > MAXPHASES)) {
                    val_64 = O_PHASES;  // use default value
                }
                fold.nbins = val_64;
                iolog(1, "handle_options: Forcing number of phases");

                break;
            case 'i' :                  /* number of sub-integrations */
                val_64 = (uint64_t)atoi(optarg);
                if ((val_64 < 0) || (val_64 > MAXSUBINT)) {
                    val_64 = O_SUBINT;  // use default value
                }
                fold.nsubints = val_64;
                iolog(1, "handle_options: Forcing number of sub-integrations");
                break;
            case 's' :                  /* number of sub-bands */
                val_64 = (uint64_t)atoi(optarg);
                if ((val_64 < MINBANDS) || (val_64 > MAXBANDS)) {
                    val_64 = O_BANDS;
                }
                fold.nsubbands = val_64;
                iolog(1, "handle_options: Forcing number of sub-bands ");
                break;
            case 'v' :                  /* verbosity level */
                Debug = atoi(optarg);
                break;
            case 'o':                   // select optimization space dimension
                // Note: O_2D3D both optimization done in the same origami run
                val = atoi(optarg);
                if ((val == O_2D) || (val == O_3D) || (val == O_2D3D)) {
                    Optimization = val;
                    iolog(1, "Optimization in %dD\n", val);
                    break;
                }
                iolog(1, "invalid %d value for optimization. Using default (%d)\n",
                         val, Optimization);
                break;
            case 'e':
                val = atoi(optarg);
                if ((val < 0) || (val > MAXPREBIN) ||
                        ((val != 1) && (val != 2) && (val != 4) && (val != 8) && (val != 16)) ) {
                    val = O_PREBIN;
                }
                fold.prebin = val;
                EnableCheck = MYTRUE;
                iolog(1, "handle_options: enable data check for rebin ");
                break;
            case 'l':
                Logging = MYTRUE;
                iolog(1, "handle_options: Enable logging on file ");
                break;
            case 'w':
                EnableWrite = MYTRUE;
                iolog(1, "handle_options: Enable  output profile writing ");
                break;
            case 'N':   // toggle Nosplit flag
                if (EnableSplit == MYTRUE) {
                    EnableSplit = MYFALSE;
                    iolog(1, "handle_options: Disable folding phase split ");
                } else {
                    EnableSplit = MYTRUE;
                    iolog(1, "handle_options: Enable folding phase split ");
                }
                break;
            case 'h':
            default:
                print_help(program_name);
                return -1;
        }
    }
    return 0;
}

/*
 * void computeTranspose_prebin_noCL(unsigned char *idata, float *odata, const  int size_x,
 *                                                       const  int size_y, int pitch_dim)
 */
/**
 *
 * Executes the transpose of the input matrix in CPU without OpenCL
 * parallelization.
 *
 * \param idata         input data in natural order
 * \param odata         input data in trasposed order
 * \param size_x        x dimension (numb. of channels)
 * \param size_y        y dimesnion (numb. of samples)
 *
 */
void cpuTranspose_prebin_noCL(unsigned char *idata, float *odata, const  int size_x, const  int size_y, int pitch_dim)
{
    int y, x, k;
    double time0 = get_current_time();

    iolog(1, "cpuTranspose_prebin_noCL pitch_dim: %d fold.prebin: %d\n", pitch_dim, fold.prebin);
    for (y = 0; y < size_y; y += fold.prebin) {
        for (x = 0; x < size_x; ++x) {
            float ftemp = 0.;
            for (k = 0; k < fold.prebin; k++) {
                ftemp += idata[((y + k) * size_x) + x];
            }
            odata[((x * pitch_dim) + y/fold.prebin)] = ftemp/fold.prebin;
        }
    }
    iolog(1, "CPU time to transpose/prebin matrix: %lf (msec)\n", get_current_time() - time0);
}

/*
 * void computeTranspose_noCL(unsigned char *idata, unsigned char *odata, const  int size_x, const  int size_y)
 */
/**
 *
 * Executes the transpose of input matrix in CPU without OpenCL parallelization.
 * Used to check the correctness of the transpose operation done in GPU (when check enabled).
 *
 * \param idata         input data in natural order
 * \param odata         input data in trasposed order
 * \param size_x        x dimension (numb. of channels)
 * \param size_y        y dimesnion (numb. of samples)
 *
 */
void cpuTranspose_noCL(unsigned char *idata, unsigned char *odata, const  int size_x, const  int size_y, int pitch_dim)
{
    int y, x;
    double time0 = get_current_time();

    for (y = 0; y < size_y; ++y) {
        for (x = 0; x < size_x; ++x) {
            odata[(x * pitch_dim) + y] = idata[(y * size_x) + x];
        }
    }
    iolog(1, "CPU time to transpose matrix: %lf (msec)\n", get_current_time() - time0);
}


/*
 * void gpu_thread(void *userdata)
 *
 * Transfer one sub-integration at time to GPU memory and execute  folding
 * and optimization.
 *
 * \param userdata: pointer to data passed by user (of type struct thr_info_t)
 */
void gpu_thread(void *userdata)
{
    int gpu = 0;
    struct thr_info_t *mythr = (struct thr_info_t *)userdata;
    const int thr_id = mythr->id;       // the thread ID
    int in = 0;                         // Warning: what happen if more than one gpu_thread run ?? */
    int isubint;                        // sub-integration number (1,..,fold.nsubints)
    int pitch_dim = mythr->pitch_dim;   //pitch dim of the rebinned data
    double t0 = get_current_time();

    gpu = mythr->gpu->id;
    isubint = 0;
    iolog(2, "[%lf] gpu%d thread-%d: started !\n", get_current_time(), gpu, thr_id);
    iolog(1, "gpu%d thread-%d running on CPU core %d\n", gpu, thr_id, sched_getcpu());
    while(1) {
        if (Keepgoing == 0) {
            // got a SIGTERM signal
            break;
        }
        if ((EndRead == 0) && (isubint == fold.nsubints)) {
            // reader_thread has finished and gpu_thread has
            // processed all subint! Exit....
            break;
        }
        // wait for data from reader_thread
        if (cXaSemaphore_wait(_Shared.full, 1) == 0) {
            long t = (long)((get_current_time() - t0) * 100);
            if ((t > 0) && ((t % 5000) == 0)) {
                iolog(3, "t: %ld\n", t);
                iolog(3, "[%lf] gpu%d thread-%d: waiting full....\n",  get_current_time(), gpu, thr_id);
            }
            continue;
        }
        // acquired semaphore on data slot
        iolog(2, "[%lf] gpu%d thread-%d: acquired full semaphore\n", get_current_time(), gpu, thr_id);
        cXaSemaphore_acquire(_Shared.mutex, 1);
        iolog(2, "[%lf] gpu%d thread-%d: acquired slot %d\n", get_current_time(), gpu, thr_id, in);
        iolog(1, "[%lf] Calling GPU kernels on sub-integration %d data slot %d", get_current_time(), isubint, in);
        // fold data on GPU
        if (folding(gpu, isubint, D_in[in], _Candidate, _Rebin) == EXIT_FAILURE) {
            printf("Kernel execution error!!\n");
            iolog(1, "[%lf] Exiting from gpu%d thread-%d...\n", get_current_time(), gpu, thr_id);
            Keepgoing = 0;
            cXaSemaphore_release(_Sem_end, 1);
            cXaSemaphore_release(_Shared.mutex, 1);
            cXaSemaphore_release(_Shared.empty, 1);
            pthread_exit(NULL);
            return;
        }
        // if test check enabled we control if corner turner operation has
        // been correctly performed in GPU.
        if (EnableCheck == MYTRUE) {
            int l, err = 0;
            float p = 0.;
            float diff = 0.;
            err = 0;
            uint64_t nsample_subslot = fold.nsamp/(fold.nchan*fold.nsubints);
            iolog(1, "Check!!!");
#ifndef TRANSPOSE_CPU
            // Perform the corner turner operation in CPU and check the result against those got from
            // GPU and copied in Host_mtx array (allocated in host memory)
            uint64_t mem_devsize = fold.nchan * pitch_dim * sizeof(float);
            void *gold = 0;
            if (fold.prebin == 1) {
                gold = (unsigned char*) calloc(mem_devsize, sizeof(char));
                cpuTranspose_noCL(H_in[in], (unsigned char*)gold, fold.nchan, nsample_subslot, pitch_dim);
            } else {    // prebinning is enabled. Allocate memory to store float values.
                iolog(1, "going to allocate gold memory!!");
                gold = (float*) calloc(mem_devsize, sizeof(float));
                if (gold == NULL) {     // it may fail because of the limitated host memory size!
                    errlog("Failure in allocating memory for check!!");
                } else {
                    cpuTranspose_prebin_noCL(H_in[in], (float*)gold, fold.nchan, nsample_subslot, pitch_dim);
                }
            }
#endif
            // control input data correctness
            iolog(1, "pitch_dim: %d \n", pitch_dim);
            for (l = 0; l < pitch_dim * fold.nchan; l ++) {
#ifdef  TRANSPOSE_CPU
                p = (float)H_in[in][l];
#else
                if (gold) {
                    if (fold.prebin == 1) {
                        unsigned char *v= (unsigned char*)gold;
                        p = (float)v[l];
                    } else {
                        float *v= (float *)gold;
                        p = (float)v[l];
                    }
                }
#endif
                int nsamp_per_subslot = fold.nsamp/(fold.nchan*fold.nsubints * fold.prebin);
                int row = l /pitch_dim;
                int max_col = nsamp_per_subslot + row * pitch_dim;
                //Host_mtx contains the copy in host memory of the matrix transposed in GPU
                //here we check if the matrix calculated in GPU is equal to
                //the one calculated in CPU
                if (l < max_col) {
                    diff = p - Host_mtx[l] - 128.;
                } else {
                    diff = p - Host_mtx[l];
                }
                if (abs(diff) > 1e-5) {
                    iolog(1, "diff[%d]: %f p: %f Host_mtx[%d]: %f \n", l, diff, p, l, Host_mtx[l]);
                    err ++;
                    errlog("Result verification failed at element %d!\n", l);
                    break;
                }
            }
            if (err == 0) {     // input data copy correct!
                iolog(1, "TEST PASSED!!!\n");
            } else {            // something wrong in copy/transpose
                iolog(1, "TEST FAILED!!!\n");
            }
#ifndef TRANSPOSE_CPU
            if (gold) {
                free(gold);
            }
#endif
        }
        // update the data array index position
        in = (++_Shared.front) % MAX_SLOT;
        // release the semaphores
        cXaSemaphore_release(_Shared.mutex, 1);
        cXaSemaphore_release(_Shared.empty, 1);
        iolog(2, "[%lf] gpu_thread-%d: released semaphore\n", get_current_time(), thr_id);
        isubint ++;
    }
    // we compute ALWAYS the 'nominal value' profiles
    iolog(2, "call kernel to build profiles!");
    profiles(gpu);
    if (EnableWrite == MYTRUE) {
        for (isubint = 0; isubint < fold.nsubints; isubint ++) {
            write_profile(isubint, _Candidate, 0, 0);
        }
        write_profile(0, _Candidate, 1, 0);
    }

    // The file with optimization results has the same name as the  data file with 'txt' extension and preceded
    // by the string "res2D" or "res3D".
    char res_filename[256];             // output filename

    // run the optimization kernels
    // OSS Optimization = O_2D3D means we run both optimization during the same origami execution
    if ((Optimization == O_2D) || (Optimization == O_2D3D)) {
        iolog(1, "First optimization step");
        // the second input parameter of the function is the print verbosity: for 2D optimization we force print
        // on screen only for the last iteration (verbose = 0 means print on screen). Only if the verbosity
        // option is enabled (-v1 or -v2 etc.) the other 2D iterations are printed on screen
        int verbose = 0;
        if (Debug > 0) {
            verbose = 1;
        }
        perturbed_profiles(gpu, verbose, O_2D, _Candidate, _Rebin, _Time_sum, _Freq_sum, 10, 1, 1, 1, 10, 1, '\0');
        iolog(1, "Second optimization step");
        perturbed_profiles(gpu, verbose, O_2D, _Candidate, _Rebin, _Time_sum, _Freq_sum, 1, 1, 10, 1, 10, 3, '\0');
        sprintf(res_filename,"res2D_%s.txt", _Base_filename);
        perturbed_profiles(gpu, 1, O_2D, _Candidate, _Rebin, _Time_sum, _Freq_sum, 10, 3, 10, 3, 1, 1, res_filename);
    }
    if ((Optimization == O_3D) || (Optimization == O_2D3D)) {
        if  (Optimization == O_2D3D) {
            int ncand;
            //if both optimization enabled re-initialize the opt parameters to the starting values.
            for (ncand = 0; ncand < fold.ncand; ncand++) {
                Opt_period[ncand] = _Candidate[ncand].period;
                Opt_pdot[ncand] = _Candidate[ncand].pdot;
                Opt_dm[ncand] = _Candidate[ncand].dm;
            }
        }
        sprintf(res_filename,"res3D_%s.txt", _Base_filename);
        perturbed_profiles(gpu, 1, O_3D, _Candidate, _Rebin, _Time_sum, _Freq_sum, 10, 1, 10, 1, 10, 1, res_filename);
    }
    //write optimized reduced profiles on disk
    if (EnableWrite == MYTRUE) {
        for (isubint = 0; isubint < fold.nsubints; isubint ++) {
            write_profile(isubint, _Candidate, 0, 1);
        }
        write_profile(0, _Candidate, 1, 1);
    }
    iolog(1, "[%lf] Exiting from gpu-thread-%d...\n", get_current_time(), thr_id);
    return;
}

/*
 * void nvml_thread(void *userdata)
 *
 * Monitor the GPu temperature and fan speed.
 *
 * \param user_data     structure with input data (as needed)
 */
void nvml_thread(void *userdata)
{
    struct thr_info_t *mythr = (struct thr_info_t *)userdata;
    int gpu = mythr->gpu->id;

    iolog(1,"[%lf] nvml_thread-%d: started\n", get_current_time(), mythr->id);
    //pthread_cleanup_push(cleanup_thread,(void*)userdata);
    while(1) {
        usleep(2000000);
        gpu_temp(gpu);
        gpu_fanspeed(gpu);
        iolog(1, "[%lf] nvml_thread-%d GPU-%d temp: %u (C) fan: %u\n", get_current_time(), mythr->id, gpu, mythr->gpu->nvml.temp,
                                                                                    mythr->gpu->nvml.fan_speed);
    }
    iolog(1, "Exiting from nvml_thread!");
}

/*
 * void reader_thread(void *userdata)
 */
/**
 * This thread reads the data from the disk and store them
 * in a memory buffer (type FIFO).
 */
void reader_thread(void *userdata)
{
    int infile = -1;   /* data file handle */
    int in = 0;
    uint64_t count = 0;
    uint64_t total_bytes = 0;
    double start_read, end_read;
    struct thr_info_t *mythr = (struct thr_info_t*)userdata;
    size_t chunk_size = mythr->chunk_size;
    int pitch_dim = mythr->pitch_dim;   //pitch dim value for original matrix

    iolog(1, "[%lf] reader thread id %d started!!\n", get_current_time(), mythr->id);
    iolog(1, "[%lf] reader thread chunk_size %d pitch_dim:%d \n", get_current_time(), chunk_size,
                                                                            pitch_dim);

    if (infile < 0) {
        if ((infile = open(fold.infile, O_RDONLY)) < 0) {
            perror("Error in open of input data file");
            errlog("Error in opening %s file", fold.infile);
            goto out;
        } else { // we skip the header
            if (0 > lseek(infile, (off_t) HeaderOffset, SEEK_SET)) {
                perror("Error in header seek of input data file");
                errlog("Error in header seek of file %s %d", fold.infile, (int) HeaderOffset);
                goto out;
            }
        }
    }

    int empty = 0;
    total_bytes = fold.nsamp * sizeof(char);    //number of bytes to read
    uint64_t left = total_bytes;
    while(left > 0) {
        if (Keepgoing == 0) {
            // got a SIGTERM signal
            break;
        }
        iolog(3, "reader thread: _Shared.rear: %d\n", _Shared.rear);
        iolog(3, "reader thread: _Shared.fron: %d\n", _Shared.front);
        /* decrease available resources number */
        empty = cXaSemaphore_available(_Shared.empty);
        if (empty == 0) {
            iolog(3, "[%lf] reader thread: Attention! fifo is full!!"
                     " _Shared.front: %d _Shared.rear: %d\n", get_current_time(),
                     _Shared.front, _Shared.rear);
            if ((MYTRUE == Logging) && (NULL != LogFptr)) {
                fprintf(LogFptr, "[%lf] FIFO full in writing!!\n", get_current_time());
            }
        }
        cXaSemaphore_acquire(_Shared.empty, 1);
        start_read = get_current_time();
        iolog(1, "[%lf] reader thread: reading slot %d (read at %5.1f%)", start_read, in,
                                         ((float)total_bytes - left) * 100./total_bytes);
        // if matrix transposition is done in CPU, we check for OpenCL presence
        //
        // OSS: next lines represents a test done at the beginning of this project to improve the execution time
        // performance of the folding alghoritm moving the matrix transpose in CPU (with and without OpenCL).
        // Since matrix corner turner does not take too much GPU time, next lines could be removed for
        // better code readibility.
        //
        errno = 0;
        //transpose done in GPU mem_size no more useful
        uint64_t mem_size = pitch_dim * fold.nchan/fold.nsubints;
        memset(H_in[in], 0, mem_size);
        if ((count = read(infile, H_in[in], chunk_size)) < 0) {
            errlog("Error in reading file (errno: %d)\n", errno);
            close(infile);
            infile = -1;
            cXaSemaphore_release(_Shared.empty, 1);
            break;
        }
        left -= count;
        fprintf(stderr, "Data processing: %5.1f%c time: %4.1lf\r", ((float)total_bytes - left) * 100./total_bytes,'%',
                                                                          (get_current_time() - _StartTime)/1000.);
        iolog(2, "[%lf] reader thread: writed slot %d (%p)\n", get_current_time(), in, H_in[in]);
        //update data array index
        in = (++_Shared.rear) % MAX_SLOT;
        // increment available resources number
        cXaSemaphore_release(_Shared.full, 1);
        end_read = get_current_time();
        iolog(2, "[%lf] reading from file takes %lf msec\n", get_current_time(), end_read - start_read);
    }
out:
    iolog(3, "reader thread: empty %d\n", cXaSemaphore_available(_Shared.empty));
    iolog(3, "reader thread: full  %d\n", cXaSemaphore_available(_Shared.full));
    /* unblock the semaphore to signal ending... */
    cXaSemaphore_release(_Sem_end, 1);
    iolog(1, "[%lf] Exiting from reader thread...\n", get_current_time());
    if (infile) {
        close(infile);
    }
    EndRead = 0;
    pthread_exit(NULL);
    return;
}

/*
 * void kill_work(void)
 */
void kill_work(void)
{
    /* unblock the semaphore to signal ending... */
    cXaSemaphore_release(_Sem_end, 1);
    Keepgoing = 0;
    iolog(1, "Keepgoing : %d\n", Keepgoing);
}

static void sighandler(int sig)
{
    /* Restore signal handlers so we can still quit if kill_work fails */
    sigaction(SIGTERM, &termhandler, NULL);
    sigaction(SIGINT, &inthandler, NULL);
    kill_work();
}

/*
 * int main(int argc, char **argv)
 */
int main(int argc, char **argv)
{
    int i;
    int ngpu                    = 0;    // number of NVIDIA gpus installed
    uint64_t nsamp              = 0;    // number of data samples
    size_t chunk_size;                  // input data chunk size (in bytes) to transfer to GPU
    int nvml_thr_id             = 0;    // GPU monitoring thread id
    int reader_thr_id           = 0;    // reader thread id
    int nstreams                = 0;    // number of GPU streams
    void **gpu_thr              = NULL; // pointer to gpu threads
    void **nvml_thr             = NULL; // pointer to nvml thread
    void **reader_thr           = NULL; // pointer to reader thread
    struct stat file_stat;              // file structure to get dimension
    struct thr_info_t *thr      = 0;
    struct sigaction handler;
    time_t now;
    int gpu_threads             = 1;    // threads running GPU kernels (1 for GPU)
    int nvml_threads            = 1;    // threads to get GPU telemetry (1 for GPU)
    int total_threads           = 1;    // total number of running threads
    struct thr_info_t *thr_info = 0;            // structure to store thread info

    printf("\n");
    now = time(NULL);
    //initialize the global variables
    initvar();
    if (handle_options(argc, argv) < 0) {
        return EXIT_FAILURE;
    }
    handler.sa_handler = &sighandler;
    handler.sa_flags = 0;
    sigemptyset(&handler.sa_mask);
    sigaction(SIGTERM, &handler, &termhandler);
    sigaction(SIGINT, &handler, &inthandler);

    // Just a failsafe check
    if (fold.nchan < fold.nsubbands) {
        fold.nsubbands = fold.nchan;
    }
    iolog(1, "[#%u] gpu: %d cpu threads: %d nchan: %d \n",  now, Ngpu, Nthread, fold.nchan);
    iolog(1, "[#%u] nbins: %d nsubints: %d nsubbands: %d\n",  now, fold.nbins, fold.nsubints, fold.nsubbands);

    // open the application logfile
    if (MYTRUE == Logging) {
        LogFptr = fopen("folding.log", "a");
        if (LogFptr == NULL) {
            errlog("Can't open log file\n");
        }
    } else {
        LogFptr = NULL;
    }
    // detect header presence
    {
        int infile = -1;   /* data file handle */
        Header = MYFALSE;
        if (infile < 0) {
            if ((infile = open(fold.infile, O_RDONLY)) < 0) {
                return EXIT_FAILURE;
            }
        }

        if (0 <= (HeaderOffset = read_header(infile)) ) {
            iolog(0, "Header present, Filterbank detected (%d)", HeaderOffset);
            Header = MYTRUE;
        } else {
            Header = MYFALSE; // just a safety
            HeaderOffset = 0; // we need a zero offset
        }
        close(infile);
    }
    //here we should read the header if file is not header-less
    //
    //On 202002 we implement a Datafile Header decode. We decode only
    //frequency data and sample time.
    //NOTE:
    //These values supersede both default and command line values.
    // If header present we get frequency data and sample time
    if (Header == MYTRUE) {
        fold.freq_ch1 = head_fold.freq_ch1;
        fold.freq_off = head_fold.freq_off;
        fold.tsamp    = head_fold.tsamp   ;
        fold.nchan    = head_fold.nchan   ;
        iolog(0, "We use frequency data and sample time from data header");
    } 

    // get info about file dimension
    if (stat(fold.infile, &file_stat) < 0) {
        if (errno == ENOENT) {
            errlog("%s doesn't exist", fold.infile);
            return EXIT_FAILURE;
        }
        errlog("Failure in stat!(error = %d)", errno);
    }
    //iolog(1, "file size: %d %d -> %lld (bytes)\n", sizeof(file_stat.st_size),
    //        sizeof(uint64_t), (uint64_t)file_stat.st_size);
    if (MYTRUE == Header) {
        file_stat.st_size = file_stat.st_size - (off_t)HeaderOffset;
    }
    //
    iolog(1, "file size: %lld (bytes)\n", (uint64_t)file_stat.st_size);
    //total number of time samples in the file
    fold.time_nsamp = (float)(file_stat.st_size-HeaderOffset)/fold.nchan;


    // check input file dimension against user options
    if (MYTRUE == Tobs_forced) {    // command line arguments get precedence
        nsamp = (uint64_t)(fold.tobs/fold.tsamp + 0.5) * fold.nchan;
        iolog(1, "main: setting nsamp from tobs nsamp=%lld", nsamp);
    } else {
        nsamp = (uint64_t)fold.nsamp;
    }
    iolog(2, "nsamp: point0 %lld", nsamp);
    /* OSS: input data are 8-bits! */
    if (nsamp > (uint64_t)file_stat.st_size / NBYTES) {
        nsamp = (uint64_t)file_stat.st_size / NBYTES;
        iolog(1, "main: nsamp forced by short filesize nsamp = %lld", nsamp);
    }
    // we want the data to be a multiple also of the MAXPREBIN value
    if ((nsamp % (fold.nsubints * fold.nchan * MAXPREBIN)) != 0) {
        nsamp = (uint64_t)((nsamp/(((float)fold.nsubints) * fold.nchan * MAXPREBIN))) * (fold.nsubints * fold.nchan * MAXPREBIN);
    }
    iolog(2, "nsamp: point1 %lld", nsamp);
    if (nsamp <= fold.nsubints * fold.nchan * MAXPREBIN) {
        errlog("Too few samples!\n");
        return EXIT_FAILURE;
    }
    fold.nsamp = nsamp;
    /* recalc total obs time */
    fold.tobs = fold.tsamp * nsamp/fold.nchan;
    iolog(1, "nsamp: %lld tsamp: %lf nsubints: %d NBYTES: %d tobs: %lf\n", nsamp, fold.tsamp, fold.nsubints,
                                                                                         NBYTES, fold.tobs);

    // build the name of the file with pulsar candidates. The canadidate
    // name is he same as the data file but with "txt" extension
    FILE *cand_ptr = NULL;
    char *tmp_file = 0;         // temproary pointer to find special chars
    char *data_folder   = 0;    // data folder name with data and candidates file

    // get the base filename used to build the name of the files with list of candidates and the
    // result of the optimization procedure.
    if ((tmp_file = (char *) malloc(strlen(fold.infile) + 1)) == NULL) {
        errlog("Error in allocating memory to store filename");
        return EXIT_FAILURE;
    }
    strcpy(tmp_file, fold.infile);
    if ((_Base_filename = (char *) malloc(strlen(tmp_file) + 1)) == NULL) {
        errlog("Error in allocating memory to store base filename");
        return EXIT_FAILURE;
    }
    //search for last '/' if the name include the complete path
    char *lastch = strrchr(tmp_file, '/');
    //printf("%s found at %d\n", tmp_file, *lastch);
    if (lastch != NULL) {
        printf("%s found at %d\n", tmp_file, *lastch);
        int  n = lastch + 1 - &tmp_file[0];
        strcpy(_Base_filename, &tmp_file[n]);
        *lastch = '\0';
        if ((data_folder = (char *) malloc(strlen(tmp_file) + 1)) == NULL) {
            errlog("Error in allocating memory to store data directory name");
            return EXIT_FAILURE;
        }
        strcpy(data_folder, tmp_file);
    } else { // local folder
        strcpy(_Base_filename, &tmp_file[0]);
    }
    free(tmp_file);
    // search for the last '.' char to get rid of the extension
    lastch = strrchr(_Base_filename, '.');
    if (lastch != NULL) {
        *lastch = '\0';
    }
    if (data_folder != NULL) {
        sprintf(_Cand_filename, "%s/%s.txt", data_folder, _Base_filename);
        //printf(" >%s< %p\n", data_folder, data_folder);
        //free(data_folder);
    } else {
        sprintf(_Cand_filename, "%s.txt",  _Base_filename);
    }
    // try to open the file with the list of candidates
    int ncand = 0;
    char line[80];
    if ((cand_ptr  =  fopen(_Cand_filename, "r")) == NULL) {
        // try to open the default file "candidates.txt"
        if ((cand_ptr  =  fopen("candidates.txt", "r")) == NULL) {
            iolog(0, "No candidates file found.");
            return EXIT_FAILURE;
        } else {
            iolog(0, "No %s file found: we use candidates.txt file ", _Cand_filename);
            strcpy(_Cand_filename, "candidates.txt");
        }
    }
    printvar();         // print main information
    fflush(NULL);           // to sync output

    // count how many candidates are into the file and compare with the one
    // set by user (if any).
    if (cand_ptr) {
        while (fgets(line, sizeof(line), cand_ptr)) {
            if ((line[0] == '%') || (line[0] == '#') || (line[0] == '\n') || (line[0] == '@')) {
                iolog(2,"line[0]: %c ncand: %d\n",line[0] ,ncand);
                continue;
            }
            ncand ++;
        }
        // cut to MAX_NCAND
        if (ncand > MAXCANDIDATES) {
            ncand = MAXCANDIDATES;
        }
        // check the read number against the requested one
        if (ncand > fold.ncand) {
            iolog(0, "Main- Found %d candidates in input file. Requested only %d",
                    ncand, fold.ncand);
            if (fold.ncand > 0) {
                ncand = fold.ncand ;
            } else {
                errlog(" Error fold.ncand = %d ",  fold.ncand);
                return EXIT_FAILURE;
            }
        }
        iolog(1, "ncand: %d fold.ncand: %d", ncand, fold.ncand);
    }
    fold.ncand = ncand;

    if (fold.ncand < 1) {
        errlog("No valid candidate found!!");
        //goto end;
        return EXIT_FAILURE;
    }

    //allocate memory to store frequencies and candidates info
    _Freq_sum= (double *)calloc(fold.nsubbands, sizeof(double));
    _Time_sum= (double *)calloc(fold.nsubints, sizeof(double));
    double *delta_freq  = (double*) calloc(fold.nchan, sizeof(double));
    //_Nbins   = (int*) calloc(fold.ncand, sizeof(int));

    // allocate memory to store the optimized values for period, dm and pdot for each pulsar candidate
    Opt_period = (double*) calloc(fold.ncand, sizeof(double));
    Opt_dm     = (float*)  calloc(fold.ncand, sizeof(float));
    Opt_pdot   = (double*) calloc(fold.ncand, sizeof(double));

    // check memory allocation
    if ((delta_freq == NULL) || (_Freq_sum == NULL) || (_Time_sum == NULL) ||
        (Opt_period == NULL) || (Opt_dm == NULL) || (Opt_pdot == NULL)) {
        if (delta_freq) {
            free(delta_freq);
        }
        if (_Time_sum) {
            free(_Time_sum);
        }
        if (_Freq_sum) {
            free(_Freq_sum);
        }
        if (Opt_period) {
            free(Opt_period);
        }
        if (Opt_dm) {
            free(Opt_dm);
        }
        if (Opt_pdot) {
            free(Opt_pdot);
        }
        errlog("Error in allocating memory!");
        if (cand_ptr) {
            fclose(cand_ptr);
        }
        return EXIT_FAILURE;
    }

    // initialize the arrays
    int ich = 0;
    float freq_chan = 0., freq_ch1_2 = 0., freq_chan_2 = 0.;

    freq_ch1_2 = fold.freq_ch1 * fold.freq_ch1;
    for (ich = 0; ich < fold.nchan; ich ++) {
        freq_chan = fold.freq_ch1 + fold.freq_off * ich;
        freq_chan_2 = freq_chan * freq_chan;
        delta_freq[ich] = K_DM * (1/freq_ch1_2 - 1/freq_chan_2);
    }
    uint64_t nchan_per_subband = fold.nchan/fold.nsubbands;
    for (ich = 0; ich < fold.nsubbands; ich ++) {
        _Freq_sum[ich] = (fold.freq_ch1 + ich * nchan_per_subband * fold.freq_off +
                         fold.freq_off * (nchan_per_subband - 1) * 0.5);
    }
    uint64_t local_measures = nsamp/(fold.nchan * fold.nsubints);
    iolog(1, "local_measures: %lld fold.tobs: %lf fold.tsamp: %lf\n", local_measures, fold.tobs, fold.tsamp);
    for (ich = 0; ich < fold.nsubints; ich ++) {
        /* from Mitch comment
         *  _Time_sum - I know you've fixed this, but I'll add it here for
         * completeness. _Time_sum is an array storing that centre times of each subint. As
         * calculated on line 1126/1127, the calculation is off by half of the sampling
         * time multiplied by one less than the number of samples per subint (samples per
         * subint == local_measures in your code), i.e. tsamp * (local_measures - 1) * 0.5.
         * Removing the final multiplication by 0.5 seems to correct things, i.e.
         * _Time_sum[ich] = (-0.5 * Tobs + tsamp * ich * local_measures + tsamp *
         * (local_measures - 1));
         */
        _Time_sum[ich] = (-0.5 * fold.tobs + fold.tsamp * ich  * local_measures +
                                       fold.tsamp * (local_measures - 1) );
    }
    // if candidates file is present, we load data from it
    if (cand_ptr == NULL) {
        errlog("No candidates file found!!");
        return EXIT_FAILURE;
    }
    // get candidates period. pdot and dm from file
    // one line per candidate (#, % comment!!)

    _Candidate = (candidate_t*) calloc(fold.ncand, sizeof(candidate_t));
    if (_Candidate == NULL) {
        errlog("Failed to allocate memory to store candidates values");
        return EXIT_FAILURE;
    }
    // go to the start of the input file
    fseek(cand_ptr, 0, SEEK_SET);
    // read values
    ncand = 0;
    while (fgets(line, sizeof(line), cand_ptr)) {
        if ((line[0] == '%') || (line[0] == '#') || (line[0] == '\n')) {
            continue;
        }
        if (line[0] == '@') {
           continue;
        }
        sscanf(line, "%lf %lf %f", &_Candidate[ncand].period, &_Candidate[ncand].pdot, &_Candidate[ncand].dm);
        // calculate the nudot value and store it inside pdot array
        _Candidate[ncand].nudot = -_Candidate[ncand].pdot/(_Candidate[ncand].period*_Candidate[ncand].period);
        iolog(1,"nudot: %15.12g\n", _Candidate[ncand].nudot);
        _Candidate[ncand].nu = 1./_Candidate[ncand].period;
        _Candidate[ncand].index = ncand;
        _Candidate[ncand].phase_bin = 0;
        //nbins[ncand] = (int) (period[ncand] * 2. / (fold.tsamp*fold.prebin) + 0.5);
        ncand ++;
        if (ncand == fold.ncand) {
            break;
        }
    }
    fclose(cand_ptr);
    iolog(1,"local_measures: read ncand = %d candidates from file candidates.txt", ncand);

    // sort candidates on ascending period
    qsort(_Candidate, fold.ncand, sizeof(*_Candidate), compare);

    //check the max number of phase bins for each candidate
    for (i = 0; i < fold.ncand; i ++) {
        iolog(1,"sorted candidate %d index: %d period=%g ", i, _Candidate[i].index,_Candidate[i].period);
    }

    for (ncand = 0; ncand < fold.ncand; ncand++) {
        Opt_period[ncand] = _Candidate[ncand].period;
        Opt_pdot[ncand] = _Candidate[ncand].pdot;
        Opt_dm[ncand] = _Candidate[ncand].dm;
    }

    // create list of candidates with different re-binning factor
    if (rebin_candidate(&_Rebin[0], _Candidate) == MYFALSE )  {
        errlog("No valid rebinninig operation!");
        return EXIT_FAILURE;
    }

    //check if it exists just a valid entry
    if (first_valid_binidx(_Rebin) < 0) {
        errlog("No valid rebinninig value found!!");
        return EXIT_FAILURE;
    }

    //allocate GPU resources for each valid prebin entry
    for (i = 0; i < MAX_NREBIN; i ++) {
        if (is_binentry_valid(i, _Rebin) == MYTRUE) {
            //for each candidate initialize the nbins array with the rebinned number of phases
            for (ncand = _Rebin[i].first; ncand <= _Rebin[i].last; ncand ++) {
                int phase_bin = 2 * ((int) (_Candidate[ncand].period / (fold.tsamp * _Rebin[i].rebin)));
                // only if phase split is not enabled, we set a max limit
                // on the number of natural phases
                if (phase_bin > fold.nbins - 1) {
                    if (EnableSplit == MYFALSE) {
                        phase_bin = fold.nbins - 1;
                    } else {
                        phase_bin = fold.nbins - 2;
                    }
                    //_Candidate[ncand].phase_bin = fold.nbins - 1;
                }
                _Candidate[ncand].phase_bin = phase_bin;
                iolog(1, "basevalue ncand: %d period[%d]: %f rebin[%d]: %d nbins: %d\n", ncand,
                                     ncand, _Candidate[ncand].period, i, _Rebin[i].rebin, _Candidate[ncand].phase_bin);
            }
        }
    }

    fflush(NULL);           // to sync output
    //get initial time
    _StartTime = get_current_time();

    // initialization of _Shared semaphores
    _Shared.front = 0;
    _Shared.rear = 0;
    _Shared.mutex = cXaSemaphore_new(1);
    _Shared.empty = cXaSemaphore_new(MAX_SLOT);
    _Shared.full = cXaSemaphore_new(0);
    _Sem_end = cXaSemaphore_new(0);

    ngpu = gpu_init();
    if (ngpu < 0) {
        printf("No CUDA capable gpu found!!\n");
        goto end;
    }
    // check returned value ...
    // use only the number of gpus specified by the user
    if (ngpu > Ngpu) {
        ngpu = Ngpu;
    }

    // initialize NVML library
    if (init_nvml() != 0) {
        printf("Error initializing nvm library\n");
        goto end;
    }

    // set the number of threads
    gpu_threads = ngpu;         // one thread for each gpu
    nvml_threads = ngpu;        // one thread for each gpu
    // the total threads include the reader one!
    total_threads = gpu_threads + nvml_threads + 1;

    // allocate memory to store threads info
    thr_info = (struct thr_info_t*)calloc(total_threads, sizeof(struct thr_info_t));
    gpu_thr = (void**)calloc(gpu_threads, sizeof(void *));
    nvml_thr = (void**)calloc(nvml_threads, sizeof(void *));
    if (!thr_info || !gpu_thr || !nvml_thr) {
        errlog("Error in memory allocation!");
        return EXIT_FAILURE;
    }

    // start gpu threads
    nstreams = fold.ncand;      // CUDA number of streams
    for (i = 0; i < ngpu; i ++) {
        // allocate host and device buffers
        if (allocate_memory(i, _Rebin, delta_freq, _Candidate, _Time_sum, _Freq_sum) == EXIT_FAILURE) {
            goto end;
        }
        // Create and initialize the GPU streams to run kernels working on
        // data processed with different rebin values.
        if (init_rebindev_resources(i, _Rebin) == EXIT_FAILURE) {
            goto end;
        }
        // Create and initialize the GPU streams to run one kernel for each kernels  candidate.
        // OSS: It seems there is no limit on the max number of GPU streams that can be
        // allocated. This does not mean all streams will execute concurrently.
        if (init_streams(i, nstreams) == EXIT_FAILURE) {
            goto end;
        }
        thr = &thr_info[i];
        thr->id = i;
        Gpus[i].id = i;
        thr->gpu = &Gpus[i];
        //get the index entry of the structure with the bin value equal to the
        //one defined by the user for data checking. If no value is specified
        //for data checking, it is used the default value (1).
        int idx = is_binvalue_valid(fold.prebin, _Rebin);
        if (idx == -1) {
            idx = first_valid_binidx(_Rebin);
            iolog(1, "fold.prebin :%d not valid. Going to use %d", fold.prebin, idx);
            fold.prebin = _Rebin[idx].rebin;
        }
        iolog(1, "fold.prebin :%d Going to use %d", fold.prebin, _Rebin[idx].rebin);
        thr->pitch_dim = _Rebin[idx].pitch_dim;
        gpu_thr[i] = cXaThread_new(thr, gpu_thread);
        if (gpu_thr[i]) {
            thr->pth = (pthread_t )cXaThread_ID(gpu_thr[i]);
        }
    }
    // start nvml threads to monitor GPU temperature and fan
    for (i = 0; i < ngpu; i ++) {
        nvml_thr_id = gpu_threads + i;
        thr = &thr_info[nvml_thr_id];
        thr->id = nvml_thr_id;
        thr->gpu = &Gpus[i];
        nvml_thr[i] = cXaThread_new(thr, nvml_thread);
        thr->pth = (pthread_t)cXaThread_ID(nvml_thr[i]);
    }
    // Dimension in bytes of a chunk of input data to copied from disk
    // to host memory and then to GPU.
    // Oss: one chunck of data = total samples of one sub-integration
    chunk_size = nsamp * NBYTES/fold.nsubints;

    //start reader thread
    reader_thr_id = gpu_threads + nvml_threads;
    thr = &thr_info[reader_thr_id];
    thr->id = reader_thr_id;
    thr->chunk_size = chunk_size;
    reader_thr = cXaThread_new(thr, reader_thread);
    thr->pth = cXaThread_ID(reader_thr);
    thr->pitch_dim = _Rebin[0].pitch_dim;
    /* block the semaphore signaling the end of reading operation */
    cXaSemaphore_acquire(_Sem_end, 1);
    /* reader thread finished, going to join all threads */
    cXaThread_join(reader_thr);
    //....
    //

    iolog(1, "Waiting threads end...\n");
    for (i = 0; i < gpu_threads; i++) {
        thr = &thr_info[i];
        cXaThread_join(gpu_thr[i]);
    }
    for (i = 0; i < nvml_threads; i++) {
        nvml_thr_id = gpu_threads + i;
        thr = &thr_info[nvml_thr_id];
        pthread_cancel(thr->pth);
        // cancel nvml threads
        cXaThread_join(nvml_thr[i]);
    }
    // get the end execution time
    double end_time = get_current_time();
    fflush(NULL);           // to sync output
    printf("\nTotal exec time      : %8.2lf  (sec)\n", (end_time - _StartTime)/1000.);
    printf("Total GPU exec time  : %8.2lf  (sec)  (folding=%8.2lf, opt=%8.2lf)\n\n",
            Gpu_time/1000., Gpu_fold/1000.,  (Gpu_time-Gpu_fold)/1000.);

 end:
    // release the resources
    if (_Shared.mutex) {
        cXaSemaphore_release(_Shared.mutex, 1);
    }
    if (_Shared.full) {
        cXaSemaphore_release(_Shared.full, 1);
    }
    if (_Shared.empty) {
        cXaSemaphore_release(_Shared.empty, 1);
    }
    if (thr_info)
        free(thr_info);
    if (nvml_thr) {
        free(nvml_thr);
    }
    if (gpu_thr) {
        free(gpu_thr);
    }
    // cleanup()
    shutdown_nvml();
    if (LogFptr) {
        fclose(LogFptr);
    }
    for (i = 0; i < ngpu; i ++) {
        release_memory(i, _Rebin);
        release_streams(i, nstreams);
        release_rebindev_resources(i, _Rebin);
        reset_device(i);
    }
    if (_Candidate) {
        free(_Candidate);
    }
    if (delta_freq) {
        free(delta_freq);
    }
    if (_Freq_sum) {
        free(_Freq_sum);
    }
    if (_Time_sum) {
        free(_Time_sum);
    }
    cXaSemaphore_delete(_Shared.mutex);
    cXaSemaphore_delete(_Shared.full);
    cXaSemaphore_delete(_Shared.empty);
    // has to return 0 to use nvidia profile program (nvvp)!!
    return 0;
}

