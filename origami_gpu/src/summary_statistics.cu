#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/transform_reduce.h>
#include <thrust/functional.h>
#include <thrust/extrema.h>
#include <cmath>
#include <limits>

// This example computes several statistical properties of a data
// series in a single reduction.  The algorithm is described in detail here:
// http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Parallel_algorithm
//
// Thanks to Joseph Rhoads for contributing this example


// structure used to accumulate the moments and other
// statistical properties encountered so far.
template <typename T>
struct summary_stats_data
{
    T n;
    T mean;
    T M2;

    // initialize to the identity element
    void initialize()
    {
      n = mean = M2 = 0;
    }

    T variance()   { return M2 / (n - 1); }
    T variance_n() { return M2 / n; }
};

// stats_unary_op is a functor that takes in a value x and
// returns a variace_data whose mean value is initialized to x.
template <typename T>
struct summary_stats_unary_op
{
    __host__ __device__
    summary_stats_data<T> operator()(const unsigned char& x) const
    {
         summary_stats_data<T> result;
         result.n    = 1;
         result.mean = (float)x - 128.;
         result.M2   = 0;

         return result;
    }
};

template <typename T>
struct summary_stats_unary_float_op
{
    __host__ __device__
    summary_stats_data<T> operator()(const T& x) const
    {
         summary_stats_data<T> result;
         result.n    = 1;
         result.mean = (float)x ;
         result.M2   = 0;

         return result;
    }
};
// summary_stats_binary_op is a functor that accepts two summary_stats_data
// structs and returns a new summary_stats_data which are an
// approximation to the summary_stats for
// all values that have been agregated so far
template <typename T>
struct summary_stats_binary_op
    : public thrust::binary_function<const summary_stats_data<T>&,
                                     const summary_stats_data<T>&,
                                           summary_stats_data<T> >
{
    __host__ __device__
    summary_stats_data<T> operator()(const summary_stats_data<T>& x, const summary_stats_data <T>& y) const
    {
        summary_stats_data<T> result;

        // precompute some common subexpressions
        T n  = x.n + y.n;
        //T n2 = n  * n;

        T delta  = y.mean - x.mean;
        //printf("y.mean: %f x.mean: %f x.n: %d\n", y.mean, x.mean, x.n);
        T delta2 = delta  * delta;

        //Basic number of samples n
        result.n   = n;
        result.mean = x.mean + delta * y.n / n;

        result.M2  = x.M2 + y.M2;
        result.M2 += delta2 * x.n * y.n / n;
        return result;
    }
};

template <typename Iterator>
void print_range(const std::string& name, Iterator first, Iterator last)
{
    typedef typename std::iterator_traits<Iterator>::value_type T;

    std::cout << name << ": ";
    thrust::copy(first, last, std::ostream_iterator<T>(std::cout, " "));
    std::cout << "\n";
}


void statistics(unsigned char *raw_in, int N, float *mean, float *rms)
{
    typedef float T;

    // initialize host array

    // transfer to device
    thrust::device_ptr<unsigned char> dev_in = thrust::device_pointer_cast(raw_in);

    // setup arguments
    summary_stats_unary_op<T>  unary_op;
    summary_stats_binary_op<T> binary_op;
    summary_stats_data<T>      init;

    init.initialize();

    // compute summary statistics
    summary_stats_data<T> result = thrust::transform_reduce(dev_in, dev_in + N, unary_op, init, binary_op);


    //std::cout <<"******Summary Statistics Example*****"<<std::endl;
    //print_range("The data", dev_in.begin(), dev_out.end());

    //std::cout <<"Count              : "<< result.n << std::endl;
    //std::cout <<"Mean               : "<< result.mean << std::endl;
    //std::cout <<"Variance           : "<< result.variance() << std::endl;
    //std::cout <<"Standard Deviation : "<< std::sqrt(result.variance_n()) << std::endl;

    *mean = result.mean;
    *rms = sqrt(result.variance_n());
    return;
}

void statistics_float(float *raw_in, int N, float *mean, float *rms)
{
    typedef float T;

    // initialize host array

    // transfer to device
    thrust::device_ptr<T> dev_in = thrust::device_pointer_cast(raw_in);

    // setup arguments
    summary_stats_unary_float_op<T>  unary_op;
    summary_stats_binary_op<T> binary_op;
    summary_stats_data<T>      init;

    init.initialize();

    // compute summary statistics
    summary_stats_data<T> result = thrust::transform_reduce(dev_in, dev_in + N, unary_op, init, binary_op);


    //std::cout <<"******Summary Statistics Example*****"<<std::endl;
    //print_range("The data", dev_in.begin(), dev_out.end());

    //std::cout <<"Count              : "<< result.n << std::endl;
    //std::cout <<"Mean               : "<< result.mean << std::endl;
    //std::cout <<"Variance           : "<< result.variance() << std::endl;
    //std::cout <<"Standard Deviation : "<< std::sqrt(result.variance_n()) << std::endl;

    *mean = result.mean;
    *rms = sqrt(result.variance_n());
    return;
}
void find_max(float *raw_in, int start, int N, float *max_val, int *index)
{
    typedef float T;
    thrust::device_ptr<float> d_in = thrust::device_pointer_cast(raw_in);

    //thrust::device_vector<float>::iterator iter = thrust::max_element(d_in, d_in + N);
    thrust::device_ptr<float> max_pos;
    max_pos = thrust::max_element(d_in + start, d_in + start + N);

    *index =  max_pos - (d_in + start);

    *max_val = *max_pos;
    //std::cout << "The maximum value is " << *max_val << " at position " << *index << std::endl;
}
