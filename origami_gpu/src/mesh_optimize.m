% Mesh plot of input file.
% $Id: mesh_optimize.m 1028 2017-07-18 15:47:43Z baffa $
% now if we do not specify xrow, yrow gets them from firts line of
% 'filename'
% on 11/2014 we try to have 3D

function max_z =  mesh_optimize(filename, xrow, yrow)

% check the number of arguments passes to the function
 if ((nargin() < 1) || (nargin() > 3))
      printf ("usage: max_rms = mesh_optimize  ( file, [xrow,yrow])\n");
      return;
 endif
 if (nargin() < 2)
      xrow = -19;
      yrow = -19;
 endif
%
 if ((xrow < 5) || (xrow > 30))
      xrow = 19;
 endif
 if ((yrow < 5) || (yrow > 30))
      yrow = 19;
 endif
 subplot(1,1,1)

%
a = load(filename);
% if called as  mesh_optimize(file) we get xrow,yrow from first row of file
whos
q = a(1,:)
xrow = q(3);
yrow = q(5);
zrow = q(4);

if (1 == zrow)  % this is the no accel search case
    a = a(2:size(a)(1),:);
    x = a(:,2);
    y = a(:,4);
    z = a(:,5);
    %
    zz = reshape(z,yrow,xrow);
    whos
    yy = y(1:yrow);
    xx = x(1:yrow:(xrow*yrow));
    deltax = 2*(max(xx)-min(xx))/(max(xx)+min(xx))
    xx = (xx - mean(xx))/mean(xx);
    yy = (yy - mean(yy))/mean(yy);
    mesh(xx,yy,zz);
    grid on
    xlabel('Fractional change in period');
    ylabel('Fractional change in DM');
    zlabel('S/N');
    title('Optimization search results')

else  % Three dimensional case. we plot only graph around maxima

    a = a(2:size(a)(1),:);
    x = a(:,2);
    z = a(:,3);
    y = a(:,4);
    s = a(:,5);
    %
    zzg = reshape(s,yrow,xrow,zrow);
    % just to limit the graph output number
    qzrow = 3;
    subplot(2,qzrow,1)

    % find the z plane containing maximum
    m_z = -1.e10;   %% max s/n on a z plane 
    m_zc= 0;        %% z coord of m_zc
    for (i = 1:zrow)
        zq      = max(zzg(:,:,i));
        zmax(i) = max(zq(:));
        if (zmax(i) > m_z) 
            m_z = zmax(i)
            m_zc= i
        endif
    endfor
    yy = y(1:yrow);
    zz = z(1:yrow:(xrow*yrow));
    xx = x(1:(yrow*zrow):(xrow*yrow*zrow));
    deltax = 2*(max(xx)-min(xx))/(max(xx)+min(xx))
    xx = (xx - mean(xx))/mean(xx);
    yy = (yy - mean(yy))/mean(yy);
    for (zi = 1:qzrow)
        subplot(2,qzrow,zi)
        ss = zzg(:,:,zi-2+m_zc);
        mesh(xx,yy,ss);
        grid on
        xlabel('Fractional change in period ');
        ylabel('Fractional change in DM');
        zlabel('S/N');
        title(sprintf('Optimization search results. Accel.Plane=%d',zi-2+m_zc));
    endfor

    % find the y plane containing maximum
    m_y = -1.e10;   %% max s/n on a y plane 
    m_yc= 0;        %% y coord of m_yc
    for (i = 1:zrow)
        yq      = max(zzg(:,:,i));
        ymax(i) = max(yq(:));
        if (ymax(i) > m_y) 
            m_y = ymax(i);
            m_yc= i
        endif
    endfor
    yy = y(1:yrow);
    zz = z(1:yrow:(xrow*yrow));
    xx = x(1:(yrow*zrow):(xrow*yrow*zrow));
    deltax = 2*(max(xx)-min(xx))/(max(xx)+min(xx))
    xx = (xx - mean(xx))/mean(xx);
    yy = (yy - mean(yy))/mean(yy);
    zz = (zz - mean(zz))/(mean(zz)+1.e-7);
    for (yi = 1:qzrow)
        subplot(2,qzrow,yi+qzrow)
        %ss = ones(xrow,zrow);
        qss = zzg(:,yi-2+m_yc,:);
        ss = reshape(qss,xrow,zrow);
        mesh(xx,zz,ss);
        grid on
        xlabel('Fractional change in period ');
        ylabel('Fractional change in accel');
        zlabel('S/N');
        title(sprintf('Optimization search results. DM.Plane=%d',yi-2+m_yc));
    endfor

endif
max_z = (max(z));
