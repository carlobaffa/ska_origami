/*
 * $Id: device_folding.cu 1205 2019-08-21 10:58:31Z baffa $
 *
 * kernel to read unsigned chars from host memory and convert into float in
 * main memory.
 * We follow the mental framework of Mike Keith's reference implementation
 *
 */

/*
 * initial assumptions:
 * Data are read in into main CPU memory. They are divided into 64
 * subintegrations (blocks). At least one full block is present at the same
 * time on main memory. Data are in unsigned char format (8 bit)
 * Block dimension is (4096 frequency channels) X (up to 187500 sequential
 * measures).  This lead to up to 3GB. Too much, we will split either in
 * frequency or in time.
 *
 * Each thread reads and process a single frequency num_frequency =
 * blockIdx.x and inside GPU frequency number is threadIdx.x data are read
 * and then converted into streams of float (4bytes) in GPU main memory.
 *
 *
*/

// includes, system

#include <folding.h>
#include <float.h>
#include "kernel.cu"
#include "host_folding.cu"
#include "summary_statistics.cu"

//
// void input_converter(unsigned char *g_in, float *g_out, int local_measures, int nchan)
//
// g_in                 the input data read from file
// g_out                the converted output data as float
// local_measure        the number of sample per channel and for sub-integration
// nchan                the global number of channel
//
__global__ void input_converter(unsigned char *g_in, float *g_out, int pitch_dim,
                                int local_measures, int nchan)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
    int j = threadIdx.y + blockIdx.y * blockDim.y;

    if ((i < local_measures) && (j < nchan)) {
        g_out[i + pitch_dim * j] = (float)g_in[i + pitch_dim *j] - 128;
    }
}

//
// void folding_worker(float *g_in, float *folded, float *weight, int local_measures,
//                                          int candidate, int warp_count,
//                                          double tobs,
//                                          int max_phases, int isubint,
//                                          int threadblock_memory)
//
// g_in                 input converted data
// folded               array to store folded data
// weight               array to store weight data
// pitch_dim            the data array row length (padded with 0)
// local_measure        number of sample per channel and per subint ( <=  pitch_dim)
// candidate            the candidate number
// warp_count           the number of warp programmed on each SM
// tobs                 the observation time (to set as constant in future ??)
// max_phases           the maximum number of phase bins for the current candidate list
// isubint              the current subintegration number
// threadblock_memory   number of shared memory elements.
//
//
// Arrangement of the data into folded and weight arrays for the first
// candidate. The same organization is replicated for each other candidate
// [0, 127]: phase bins range
// ---------------------------------------------------------------------------------------
// |0|1|...|127 |0|1|...|127|...|0|1|...|127|0|1|...|127|0|1|...|127|...|0|1|...|127|....
// ---------------------------------------------------------------------------------------
// |<-band 0  ->|<-band 1 ->|...|<-band N ->|<-band 0 ->|<-band 1 ->|...|<-band N ->|
// <------------- subint 0  --------------->|<----------- subint 1  ----------------> ... <-- subint N  ->
// <------------                     candidate 0                                                 -------->
//
//
__global__ void folding_worker(float *g_in, float *folded, float *weight,
                               int pitch_dim, int local_measures, int candidate,
                               int rebin, int shared_phases, int warp_count,
                               double tobs, int max_phases, int isubint, int threadblock_memory)
{
    /* threadIdx.x:      the current sample inside thread block
     * threadIdx.y       the current channel inside thread block
     * blockDim.x        the stride in samples
     * blockIdx.x:       subband index
     * blockDim.y        the stride in channels
     * gridDim.x:        number of subbands
     */
    int subband_idx  = blockIdx.x;      // the sub-band index (0, 1, ...64)
    int chan_idx = threadIdx.y;         // index of channel in current subband
                                        // (0, 1, .., n_chan_band)

    int time_idx = 0;                   // index to subint measures
    // start of phase array for current operations
    int start_phase = max_phases * subband_idx + isubint * max_phases * gridDim.x +
                      candidate * _nsubints * max_phases *gridDim.x;
    int channel = 0;    // index of local channel
    // To be computed into CPU and put in a global tabellar form  (DONE on 24/12/2013)
    int nbins   = _nbins[candidate];    // total number of phases of the candidate
    // physical values.
    // The extra delay in for this channel based on the DM
    // equation (see Lorimer & Kramer book)
    // time of local sample
    double time_s = 0.; // <--- WARNING!!!!!!!!!!!!
    // float has only 7 digit mantissa. for a full 600 sec datafile this is not enough!
    double phase     = 0.; // phase of single measure

    int bin1 = 0;       // profile index of the first phase bin
    int bin2 = 0;       // profile index of the second phase bin
    int start_data = subband_idx * _nchan_per_band * pitch_dim;
    int start_idx = 0;          //index of the first time sample of each subband
    float input_temp = 0;       //the input sample value
  //  float *ptr = 0;             //pointer to the data array
    float frac_floor = 0.;
    float frac = 0;
    float bin2_frac = 0.;       //the second phase bin weight
    float val2 = 0.;            //the second phase bin intensity value
    extern   __shared__ float hist_profile[];   //dinamically allocated shared memory
    int idx = threadIdx.x + blockDim.x * threadIdx.y;   //thread global id
    // (idx >> 5) gives the warp number
    // 1 warp = 32 threads
    //
    // Att: if for the future GPUs 1 warp != 32, next lines has to be changed!!!
    // The warp_size is obtained by the cudaDeviceProp structure.
    float *warp_profile = hist_profile + (idx >> 5) * shared_phases;
    float *hist_weight = hist_profile + warp_count * shared_phases;
    float *warp_weight=  hist_weight + (idx >> 5) * shared_phases;

    // zero the shared memory
    for (int i = idx; i < threadblock_memory ; i += blockDim.x * blockDim.y) {
        hist_profile[i] = 0.;
        hist_weight[i] = 0.;
    }
    __syncthreads();

    // loop on time measures  (local_measures at time, nsamp/fold.nsubints)
    int nchan =  _nchan_per_band * subband_idx; //number of the first channel of the subband
    int nsamp = local_measures * isubint;       //number of the first time sample of the
                                                //sub-integration
    double _tt = _tsamp * rebin;
    float nudot_contrib = 0.;
    double t0 = -tobs * 0.5;
    for (time_idx = threadIdx.x ; time_idx < local_measures; time_idx += blockDim.x) {
        start_idx = start_data + time_idx;                  // sample position into the data array
        //ptr = g_in + start_idx;
        time_s = t0 + _tt * (nsamp + time_idx);   // sample position in time
        nudot_contrib = time_s * time_s * _nudot[candidate] * 0.5;//phase shift due to acceleration
        for (chan_idx = threadIdx.y ; chan_idx < _nchan_per_band; chan_idx += blockDim.y) {
            channel = chan_idx + nchan;                     // index of local channel
            input_temp = g_in[start_idx + chan_idx * pitch_dim];
            // we compute phase
            phase = (time_s + _dm[candidate] * _delta_freq[channel]) *_nu[candidate] + nudot_contrib;
            //frac = (float) ((phase - floor(phase)) * nbins);
            frac = (float) ((phase - floor(phase)) * (nbins-1));
            bin2_frac = modff(frac, &frac_floor);

            //bin1 = ((int)frac_floor) % nbins;
            //bin1 = ((int)frac_floor);

            // ATT:
            // bin1 can be less or equal to nbins (nbins is the max number
            // of phase bins for the specific candidate), so the mod
            // operation is not necessary.
            // To avoid mod operation also in the calculation of bin2
            // (to limit the number of internal registers used) it may
            // happen that the data are stored also in the element  (nbins + 1) of
            // the profile array.
            // In the computation of the final profile we will add the
            // value of element nbins to that stored in  element 0,
            // and the one of the element (nbins + 1) in element 1.
            //
            //bin2 = ((int)ceilf(frac)) % nbins;
            //bin2 = ((int)ceilf(frac));
            //
            // if at the end we sum up to the last  phase
            // (nbins) to the first one (0)
            // OSS: here frac > 0 so the integer part is equal to floor(frac)
            bin1 = (int)frac_floor;
            val2 =  bin2_frac * input_temp;
            bin2 = (bin1 + 1);
            // input Index is computed as follows:
            // time_idx is the index of the measures (along time)
            //          of the current subint of the current channel
            // chan_idx * local_measures is the offset of start of
            //          measure of current channel from the beginning of
            //          channel block
            // The following changes only in the outer loop:
            // n_chan_band * blockIdx.x * local_measures is the offset
            //          the start of measure of current channel block
            // input_temp = g_in[time_idx + chan_idx * local_measures +
            //             n_chan_band * blockIdx.x * local_measures];

            // output
            // folded is structured as a (multidim) array:

            // MAXPHASES  number of phases
            // weight is exactly the same.
            // start_phase = candidate * blockDim.x + subband_idx * gridDim.x

            //read and update data use atomic functions because there can be
            //collisions between threads accessing the same address
            atomicAdd((float *)&warp_profile[bin1], input_temp - val2);
            atomicAdd((float *)&warp_profile[bin2], val2);
            atomicAdd((float *)&warp_weight[bin1], (float)(1. - bin2_frac));
            atomicAdd((float *)&warp_weight[bin2], (float)bin2_frac);

        }
    }
    __syncthreads();
    float sum  = 0;
    float sum1 = 0;
    // ATT: since we do not use the mod operator to limit the number of
    // phases up to the maximum value (nbins),  we have to take into
    // account also the profile elements at index = nbins and index =
    // (nbins + 1).
    // The next procedure takes care to sum up these elements
    // respectively to element 0 and 1.
    // !! However this means that we can't elaborate data belonging to
    // !! candidates with a number of phase bins > 126, assuming that our max
    // !! number of phase bins is 128.
    //
    if (idx < nbins) {
        for (int bin = idx; bin <= nbins + 1; bin += nbins) {
            for (int iwarp = 0; iwarp < warp_count; iwarp ++) {
                sum +=  hist_profile[bin + iwarp * shared_phases];
                sum1 += hist_weight[bin + iwarp * shared_phases];
            }
        }
        folded[idx + start_phase] = sum;
        weight[idx + start_phase] = sum1;
    }
}

//
// void folding_worker_nosplit(float *g_in, float *folded, float *weight, int local_measures,
//                                          int candidate, int warp_count,
//                                          double tobs,
//                                          int max_phases, int isubint,
//                                          int threadblock_memory)
//
// g_in                 input converted data
// folded               array to store folded data
// weight               array to store weight data
// pitch_dim            the data array row length (padded with 0)
// local_measure        number of sample per channel and per subint ( <=  pitch_dim)
// candidate            the candidate number
// warp_count           the number of warp programmed on each SM
// tobs                 the observation time (to set as constant in future ??)
// max_phases           the maximum number of phase bins for the current candidate list
// isubint              the current subintegration number
// threadblock_memory   number of shared memory elements.
//
//
// Arrangement of the data into folded and weight arrays for the first
// candidate. The same organization is replicated for each other candidate
// [0, 127]: phase bins range
// ---------------------------------------------------------------------------------------
// |0|1|...|127 |0|1|...|127|...|0|1|...|127|0|1|...|127|0|1|...|127|...|0|1|...|127|....
// ---------------------------------------------------------------------------------------
// |<-band 0  ->|<-band 1 ->|...|<-band N ->|<-band 0 ->|<-band 1 ->|...|<-band N ->|
// <------------- subint 0  --------------->|<----------- subint 1  ----------------> ... <-- subint N  ->
// <------------                     candidate 0                                                 -------->
//
//
__global__ void folding_worker_nosplit(float *g_in, float *folded, float *weight,
                               int pitch_dim, int local_measures, int candidate,
                               int rebin, int shared_phases, int warp_count,
                               double tobs, int max_phases, int isubint, int threadblock_memory)
{

    /* threadIdx.x:      the current sample inside thread block
     * threadIdx.y       the current channel inside thread block
     * blockDim.x        the stride in samples
     * blockIdx.x:       subband index
     * blockDim.y        the stride in channels
     * gridDim.x:        number of subbands
     */
    int subband_idx  = blockIdx.x ;     // the sub-band index (0, 1, ...64)
    int chan_idx = threadIdx.y;         // index of channel in current subband
                                        // (0, 1, .., n_chan_band)

    int time_idx = 0;                   // index to subint measures
    // start of phase array for current operations
    int start_phase = max_phases * subband_idx + isubint * max_phases * gridDim.x +
                      candidate * _nsubints * max_phases *gridDim.x;
    int channel = 0;    // index of local channel
    // To be computed into CPU and put in a global tabellar form  (DONE on 24/12/2013)
    int nbins   = _nbins[candidate];    // total number of phases of the candidate
    // physical values.
    // The extra delay in for this channel based on the DM
    // equation (see Lorimer & Kramer book)
    // time of local sample
    double time_s = 0.; // <--- WARNING!!!!!!!!!!!!
    // float has only 7 digit mantissa. for a full 600 sec datafile this is not enough!
    double phase     = 0.; // phase of single measure

    int bin1 = 0;       // profile index of the first phase bin
    int start_data = subband_idx * _nchan_per_band * pitch_dim;
    int start_idx = 0;          //index of the first time sample of each subband
    float input_temp = 0;       //the input sample value
    float *ptr = 0;             //pointer to the data array
    extern   __shared__ float hist_profile[];   //dinamically allocated shared memory
    int idx = threadIdx.x + blockDim.x * threadIdx.y;   //thread global id
    // (idx >> 5) gives the warp number
    // 1 warp = 32 threads
    //
    // Att: if for the future GPUs 1 warp != 32, next lines has to be changed!!!
    // The warp_size is obtained by the cudaDeviceProp structure.
    float *warp_profile = hist_profile + (idx >> 5) * shared_phases;
    float *hist_weight = hist_profile + warp_count * shared_phases;
    float *warp_weight=  hist_weight + (idx >> 5) * shared_phases;

    // zero the shared memory
    for (int i = idx; i < threadblock_memory ; i += blockDim.x * blockDim.y) {
        hist_profile[i] = 0.;
        hist_weight[i] = 0.;
    }
    __syncthreads();

    // loop on time measures  (local_measures at time, nsamp/fold.nsubints)
    int nchan =  _nchan_per_band * subband_idx; //number of the first channel of the subband
    int nsamp = local_measures * isubint;       //number of the first time sample of the
                                                //sub-integration
    float nudot_contrib = 0.;
    double t0 = -tobs * 0.5;
    for (time_idx = threadIdx.x ; time_idx < local_measures; time_idx += blockDim.x) {
        start_idx = start_data + time_idx;                  // sample position into the data array
        time_s = t0 + _tsamp * rebin *(nsamp + time_idx);   // sample position in time
        ptr = g_in + start_idx;
        nudot_contrib = time_s * time_s * _nudot[candidate] * 0.5;//phase shift due to acceleration
        for (chan_idx = threadIdx.y ; chan_idx < _nchan_per_band; chan_idx += blockDim.y) {
            channel = chan_idx + nchan;                     // index of local channel
            input_temp = ptr[chan_idx * pitch_dim];
            // we compute phase
            phase = (time_s + _dm[candidate] * _delta_freq[channel]) *_nu[candidate] +
                                            nudot_contrib;
            bin1 = (int)((phase - floor(phase)) * nbins);

            // ATT:
            // bin1 can be less or equal to nbins (nbins is the max number
            // of phase bins for the specific candidate), so the mod
            // operation is not necessary.
            // To avoid mod operation  (to limit the number of internal registers used) it may
            // happen that the data are stored also in the element nbins  of
            // the profile array.
            // In the computation of the final profile we will add the
            // value of element nbins to that stored in  element 0,
            //
            //
            // if at the end we sum up to the last  phase
            // (nbins) to the first one (0)
            // OSS: here frac > 0 so the integer part is equal to floor(frac)
            // input Index is computed as follows:
            // time_idx is the index of the measures (along time)
            //          of the current subint of the current channel
            // chan_idx * local_measures is the offset of start of
            //          measure of current channel from the beginning of
            //          channel block
            // The following changes only in the outer loop:
            // n_chan_band * blockIdx.x * local_measures is the offset
            //          the start of measure of current channel block
            // input_temp = g_in[time_idx + chan_idx * local_measures +
            //             n_chan_band * blockIdx.x * local_measures];

            // output
            // folded is structured as a (multidim) array:

            // MAXPHASES  number of phases
            // weight is exactly the same.
            // start_phase = candidate * blockDim.x + subband_idx * gridDim.x

            //read and update data use atomic functions because there can be
            //collisions between threads accessing the same address
            atomicAdd((float *)&warp_weight[bin1], 1.);
            atomicAdd((float *)&warp_profile[bin1], input_temp);
        }
    }
    __syncthreads();
    float sum = 0;
    float sum1 = 0;
    //
    // ATT: since we do not use the mod operator to limit the number of
    // phases up to the maximum value (nbins),  we have to take into
    // account also the profile element at index = nbins.
    // The next procedure takes care to sum up this element
    // respectively to element 0.
    // !! However this means that we can't elaborate data belonging to
    // !! candidates with a number of phase bins > 127, assuming that our max
    // !! number of phase bins is 128.
    //
    if (idx < nbins) {
        for (int bin = idx; bin < nbins + 1; bin += nbins) {
            for (int iwarp = 0; iwarp < warp_count; iwarp ++) {
                sum +=  hist_profile[bin + iwarp * shared_phases];
                sum1 += hist_weight[bin + iwarp * shared_phases];
            }
        }
        folded[idx + start_phase] = sum;
        weight[idx + start_phase] = sum1;
    }
}

//
// __global__ void normalize_kernel(float *folded, float *weight, int ncand,
//                                  float mean, int nsubband)
//
// folded       global array with folded data
// weight       global array with weights
// ncand        the candidate id
// mean         the input data mean
// nsubband     the number of freq. bands
//
__global__ void normalize_kernel(float *folded, float *weight, int ncand,
                                 float mean, int nsubbands)
{
   int n;       //loop counter
   int idx0 = threadIdx.x + blockIdx.x*blockDim.x * nsubbands + ncand *
                            gridDim.x * blockDim.x * nsubbands;
   int sub_idx = 0;     //subband index (0, 1, .., 63)
   int idx = 0;
   float profile = 0.;
   float counts = 0.;

   /*
    * threadIdx.x       the bin number
    * blockDim.x        max number of bins
    * blockIdx.x        sub-int number (0, 1, ..., nsubints - 1)
    * gridDim.x         number of sub-integrations (nsubints)
    */
   //mean = -0.353104; // Debug: mean value of test_data as commputed by Mike

   // idx0: the index of the bin of the subband 0 of the current subint
   // loop on the band
   for (n = 0; n < nsubbands; n ++) {
        sub_idx = n * blockDim.x;       // the subband index
        idx = idx0 + sub_idx;           // the global bin index for the current candidate
        counts = weight[idx];
        profile = 0.;
        if (counts > FLT_EPSILON) {
            // here we subctract the mean value of data
            profile = folded[idx];
            profile = profile/counts - mean;
        }
        folded[idx] = profile;
    }
}

//
// __global__ void profile_kernel(float *in, float *out, int ncand, int nloop)
//
// Builds intermediate and final profile
// This kernel is called two times: in the first call is created the
// profile in freq (an array with Ncand*subints*max_phases elements
// In the second call is build the final profile (an array with
// max_phases * Ncand elements)
//
// in           the input array
//              first call : d_folded
//              second call: d_outprof
// out          the output array
//              first call : d_outprof
//              second call: d_outprof
// ncand        the candidate id
// nloop        the number of repetition:
//              first call  = nsubbands
//              second call = nsubints
__global__ void profile_kernel(float *in, float *out, int ncand, int nloop)
{
   int idx0 = threadIdx.x + blockIdx.x * blockDim.x * nloop +
                            ncand * blockDim.x * gridDim.x * nloop;
   int sub_idx = 0;     // shift depending on loop counter
   int idx;             // index inside data array
   float profile = 0.;
   float ftscrunch = 0;

    //
    // threadIdx.x       bin
    // blockDim.x        max number of bins
    // blockIdx.x        first call : sub-int number (0, 1, ..., nsubints - 1)
    //                   second call: 0
    // gridDim.x         first call : number of sub-integrations (nsubints)
    //                   second call: 1
    //
    //
    // loop on the subband/subints
    for (int n = 0; n < nloop; n ++) {
        sub_idx = n* blockDim.x;
        idx = idx0 + sub_idx;
        profile = in[idx];
        ftscrunch += profile;
    }
    out[threadIdx.x + blockIdx.x * blockDim.x + ncand * blockDim.x*gridDim.x] =
                                                               ftscrunch/nloop;
}

//
//__global__ void best_profile_kernel(float *in, float *out, int ncand,
//                                    int nloop, int index, int ntrial)
//
// Build the signal profile for all the trials.
//
// in           the profile in freq.
// out          an array with the signal profile for each 3-tuple of trial values
// ncand        the candidate number
// index        the trial index identifying the 3-tuple
// ntrial       the max number of trials
__global__ void best_profile_kernel(float *in, float *out, int ncand,
                                    int nloop, int index, int ntrial)
{
   int idx0 = threadIdx.x + ncand * blockDim.x * nloop; // starting index
   int sub_idx = 0;     // shift depending on loop counter
   int idx;             // index inside data array
   float profile = 0.;
   float ftscrunch = 0;
    //
    // threadIdx.x       bin
    // blockDim.x        max number of bins
    //
    // loop on the subband/subints
    for (int n = 0; n < nloop; n ++) {
        sub_idx = n* blockDim.x;
        idx = idx0 + sub_idx;
        profile = in[idx];
        ftscrunch += profile;
    }
    out[threadIdx.x + index * blockDim.x + ncand * blockDim.x * ntrial] = ftscrunch/nloop;
}

//
//
//__global__ void perturbate_kernel(float *g_in, float *g_out,
//                                 double delta_nu, double delta_dm,
//                                 double delta_nudot, int candidate,
//                                 int max_phases, int index)
// The threads along x process the bin phase, while the threads along y
// process a subband profile. The kernel takes into account the possibility
// that the max number of threads along x (blockDim.x) is less than nbins
// (the number of bin phases for each candidate).
//
// g_in  the input data to be corrected
// g_out the output data pertubed
// ncand the candidate number
// delta_nu     perturbation in freq.
// delta_dm     perturbation in dm     (perturbed = original*delta_dm)
// delta_pdot   perturbation in pdot   (perturbed = original*delta_pdot)
// candidate    the candidate number
// max_phases   maximum number of phases
// index        the global index used to address the set of three parameters used
//              in the optimization phase. This value is only used for
//              debug purpose. To remove later!!
//
//
__global__ void perturbate_kernel(float *g_in, float *g_out,
                                 double *phase_shift, int candidate,
                                 int max_phases, int index)
{
    //
    // threadIdx.x      bin (threadIdx < nbins)
    // blockIdx.x       the sub-band number
    // blockIdx.y       the sub-integ number
    // gridDim.y        the number of sub-integ
    int subband_idx  = threadIdx.y + blockIdx.x * blockDim.y;  // the sub-band index (0, 1, ...64)

    // nsubbands = gridDim.x * blockDim.y;
    // start phase: the shift to the next profile of max_phases bins.
    int start_phase = max_phases * subband_idx + blockIdx.y * max_phases * gridDim.x * blockDim.y +
                          candidate * max_phases *gridDim.x *gridDim.y * blockDim.y;
    int nbins = _nbins[candidate];
    double ph_shift = phase_shift[subband_idx + blockIdx.y * gridDim.x  * blockDim.y + candidate * gridDim.x *gridDim.y * blockDim.y];
    // indata store the folded profile (max_phases bins) for blockDim.y subbands
    extern __shared__ float indata[];

    int bin_off = (int)(ph_shift * nbins + 0.5);   // the offset in bins of the perturbed profile
    for (int idx = threadIdx.x; idx < max_phases;  idx += blockDim.x) {
    //null the output array
        g_out[idx + start_phase] = 0.;
    }
    __syncthreads();
    for (int idx = threadIdx.x; idx < nbins; idx += blockDim.x) {
        int ibin = (idx + bin_off) % nbins;
        // max_phases * threadIdx.y shift to the profile of the following subband
        indata[idx + max_phases * threadIdx.y] = g_in[ibin + start_phase];
        g_out[idx + start_phase] = indata[idx + max_phases * threadIdx.y];
    }
}

// This kernel (old version) is launched with max_phases (=128) threads along the x
// direction and no thread along y.
// Subbands and subintegrations are handled as blocks in x and y.
// No thread is launched in the y direction.
// Since in general for millisec pulsar the number of phase bins is << 128,
// the lauched threads are not fully utilized.
// The new version of the current kernel is launched with a smaller number
// of threads along x, then some threads along y can be used to process a number of subbands
// (or subints). In this way threads along x axis are nearly all used to
// process the phase bins.
__global__ void perturbate_kernel_old(float *g_in, float *g_out, float*phase_shift, int candidate,
                                      int max_phases, int index)
{
    //
    // threadIdx.x      bin
    // blockDimx.x      max number of bins
    // blockIdx.x       the sub-band number
    // gridDim.x        the number of subbands
    // blockIdx.y       the sub-integ number
    // gridDim.y        the number of sub-integ
    //
    int subband_idx  = blockIdx.x;  // the sub-band index (0, 1, ...64)
    int isub = blockIdx.y;          // sub integration index

    int start_phase = max_phases * subband_idx + isub * max_phases * gridDim.x +
                          candidate * max_phases *gridDim.x *gridDim.y;
    int bin_off = 0;
    int ibin = 0;
    int nbins = _nbins[candidate];
    float ph_shift = phase_shift[subband_idx + isub * gridDim.x + candidate
        * gridDim.x * gridDim.y];
    extern __shared__ float indata[];

    //null the output array
    g_out[threadIdx.x + start_phase] = 0;
    indata[threadIdx.x] = 0;
    __syncthreads();
    bin_off = (int)(ph_shift * nbins + 0.5) % nbins;
    if (threadIdx.x < nbins) {
        ibin = (threadIdx.x + bin_off) % nbins;
        indata[threadIdx.x] = g_in[start_phase + ibin];
        g_out[threadIdx.x + start_phase] = indata[threadIdx.x];
    }
}

//
//
//__global__ void compute_sn(float* profile, float* best, int max_phases, int cand,
//                           int nelem, float sigma)
//
//
// profile              the profile array
// best                 the array to store best result during pertubation
// max_phases           the max number of phases
// cand                 the candidate index
// nelem                the total number of pertubation applied
// sigma                the profile standard deviation less a nbins correction (see comment below)
//
//
// From the "Handbbok of Pulsar astronomy" (pag 167): the most commonly-used measure
// of profile significance is the signal to noise ratio, S/N usually
// defined as:
//
// S/N = 1/(_sigma* (sqrt(Weff))) * sum (p_i - p_mean)  for i = 0, ... nbins
//
// Weff         the equivalent width (in bins) of a top-hat pulse with the
//              same area and peak height as the observad profile
// p_i          the amplitude of the i-th bin of the profile
// p_mean       off pulse mean calculated for on the raw time series
// _sigma = sigma * (sqrt(nbins))
// sigma        is the standard deviation calculated for the raw time series
//              ( sqr( (<X^2> - <X>^2) / (num(X)-1) Where X are input
//              values both in time and in frequency )
// nsamples     the total number of samples in the time series
//
// The calculation of S/N is repetead for each (dm, nu, nudot) combination
// and for each combination is taken the max value.
// The number of combinations (trials) depends on the value of range and
// step choosen for each of the 3 dimensions.
//
// The kernel runs a thread for each trial.
// If the total number of trials is > 1024 (the max number of threads
// programmable for each SM), the threads (= trial) has to be divided in
// block.

// OSS: the last block may contain a number of threads < 1024. So it
// is necessary to compare the global thread id number (= trial index)
// with the max number of trials passed to the kernel as argument.

__global__ void compute_sn(float* profile, float* best, int max_phases, int ncand, int nelem, float sigma)
{
    //
    // threadIdx.x      the trial number inside a thread block
    // blockDimx.x      the number of trials/per block
    // blockIdx.x       the block id:
    // gridDim.x        the number trials block
    //
    int width;          // impulse width in bin
    int nelem_per_block = blockDim.x;   // the number of pertubations per thread block
    float curval = 0;                   // the profile value inside the box
    float sn = 0;                       // the signal-to-noise value
    float b_width = 0;                  // the best pulse width in physical unit
    float b_sn = - FLT_MAX;             // the best signal-to-noise value
    float maxval = 0.;                  // the maximum integrated value of profile
    float _sigma;                       // standard deviation of the profile
    int tid = threadIdx.x;              // local thread id
    int gid = threadIdx.x + blockIdx.x * blockDim.x;    // global thread id = trial index
    int start = gid * max_phases + ncand * nelem * max_phases;
    extern float __shared__ result[];

    int nbins = _nbins[ncand];  // the real number of phases for the current candidate
    _sigma = sigma * sqrt((float)nbins);
    //_sigma = sigma;
    // Warning!!! Last arg should be sigma*sqrt((float)  (nbins[ncand] * first_prebin))
    // but  in calling routine we do not have nbins[], so we make computation here

    result[tid] = 0;
    if (gid < nelem) {
        for (width = 1; width < (nbins >> 1); width ++) {
            curval = 0;
            for (int ibin = 0; ibin < width; ibin++){       // initial guess
                curval += profile[start + ibin % nbins];
            }

            maxval = curval;
            // To get maximum value we shift the 'active' window on the phase
            // array.  To save computation on each step we start from the value
            // of preceding try and then we get the shift of window by dropping
            // its first value and adding the first bin after its end. The
            // assumed topology is toroidal: last phase bin is contigous to the first
            for (int ibin = 0; ibin < nbins; ibin++){
                int addbin = ibin + width;
                curval -= profile[ start + ibin  % nbins];
                curval += profile[ start + addbin  % nbins];
                if (maxval < curval) {
                    maxval = curval;
                }
            }
            sn = maxval /sqrt((float)width)/_sigma;
            if (sn > b_sn) {
                b_sn = sn;
                b_width =  width;
            }
        }
        // we understood how Matlab code computes S/N
        // on 7/2019 we revert to Mike formula

        // store the best sn and width values in shared memory
        result[tid] = b_sn;
        result[tid + nelem_per_block] = b_width/(_nu[ncand] * nbins);
        //result[tid + nelem_per_block] = b_width * _tsamp;
        __syncthreads();
        // copy to global memory all the nelem values for sn and width
        best[gid + ncand * nelem * 2] = result[tid];
        best[gid + nelem + ncand * nelem * 2] = result[tid + nelem_per_block];
    }
    return ;
}

//
// __global__ void transposeNoBankConflicts(float *odata, unsigned char *idata,
//                                          int width, int height, int nsamp)
//
// Executes the transpose of input data and convert it from unsigned to float.
//
// odata         the output data matrix
// idata         the input  data matrix
// width         the matrix width (= pitch dimension to get aligned  memory data)
// height        the matrix heigth(= number of freq. channels)
// nsamp         the number of samples per sub-integration and per channel
//
__global__ void transposeNoBankConflicts(float *odata, unsigned char *idata,
                                         int width, int height, int nsamp)
{
    extern __shared__ char tile[];      //shared memory size allocated dinamically
    int tile_dimx = blockDim.x;         // number of threads in x direction
    int tile_dimy = blockDim.y;         // number of threads in y direction

    // null the shared memory
     tile[threadIdx.x + blockDim.x * threadIdx.y] = 0;
     __syncthreads();

    int xIndex = blockIdx.x * tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * tile_dimy + threadIdx.y;
    int index_in = 0;
    if ((xIndex < width) && (yIndex < nsamp)) {
        index_in = xIndex + (yIndex)*width;
        // convert from unsigned to signed
        tile[threadIdx.y + blockDim.y * threadIdx.x] = idata[index_in] - 128;
    }
    __syncthreads();

    xIndex = blockIdx.y * tile_dimy + threadIdx.x;
    yIndex = blockIdx.x * tile_dimx + threadIdx.y;
    int index_out = xIndex + (yIndex)*height;
    // convert to float
    odata[index_out] = (float)tile[threadIdx.x + blockDim.x * threadIdx.y];
}

//
// __global__ void transposePreBin(float *odata, unsigned char *idata,
//                                          int width, int height,
//                                          int nsamp, int prebin)
//
// Executes the transpose of input data and convert it from unsigned to
// float. If the prebinning value > 1, it executes also the data binning.
//
// odata         the output data matrix
// idata         the input  data matrix
// in_height     the input matrix number of rows (heigth) equal to the
//               number of freq. channels
// out_width     the outout matrix number of cols (width) equal to the
//               pitch dimension to get aligned  memory data
// nsamp         the number of samples per sub-integration and per channel
// prebin        the number of samples averaged as prebinning (1-8 power of 2)
//
__global__ void transposePreBin(float *odata, unsigned char *idata, int in_height,
                                int out_width, int nsamp, int prebin)
{
    extern __shared__ char tile[];      //shared memory size allocated dinamically
    int tile_dimx = blockDim.x;         // number of threads in x direction (time)
    int tile_dimy = blockDim.y;         // number of threads in y direction (frequency)
    //int prebin_index = threadIdx.x % prebin;  // index inside prebinning group
    //OSS: prebin is a power of 2 so modulo operation can be replaced with
    //the next one
    int prebin_index = threadIdx.x  & (prebin - 1);  // index inside prebinning group
    float ftemp = 0.;                   // temporary float well

    // null the shared memory
     tile[threadIdx.x + blockDim.x * threadIdx.y] = 0;
     __syncthreads();

    int xIndex = blockIdx.x * tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * tile_dimy + threadIdx.y;
    int index_in = 0;
    if ((xIndex < in_height) && (yIndex < nsamp)) {
        index_in = xIndex + (yIndex)*in_height;
        // convert from unsigned to signed
        tile[threadIdx.y + blockDim.y * threadIdx.x] = idata[index_in] - 128;
    }
    __syncthreads();

    // we only proceed if first thread of prebinning group

    if (prebin_index == 0) {
        xIndex = blockIdx.y * tile_dimy + threadIdx.x;
        yIndex = blockIdx.x * tile_dimx + threadIdx.y;
        //int index_out = xIndex/prebin + (yIndex)*height/prebin;
        int index_out = xIndex/prebin + (yIndex)*out_width;
        // convert to float and sum up
        for (int i = 0 ; i < prebin ; i++) {
            ftemp += (float)tile[threadIdx.x + i + blockDim.x * (threadIdx.y)];
        }
        odata[index_out] = ftemp/prebin;
    }
    return;
}

//__global__ void binInputKernel(float *odata, float *idata, int height, int in_width,
//                               int out_width, int prebin)
//
//
// odata         the output data matrix
// idata         the input data matrix
// in_height     the input matrix number of rows (height = num of freq. channels)
// in_width      the input matrix number of columns (width) equal to the input pitch_dim
// out_width     the output matrix number of columns (width) equal to the output pitch_dim
// prebin        the rebinning factor (2, 4, 8 or 16)
//
//
// This kernel executes the binning of the input matrix.
// When this kernel is executed, the input matrix has been already transposed. In
// this case:
// x direction -> time samples
// y direction -> freq. channels
// The input matrix and output matrix have the same height (4096 channels)
// and different widths. The two widths are not in the 2:1 proportion
// because the dimensionis in the row direction of the in and out matrix are aligned to get
// coalesced i/o operations.

__global__ void binInputKernel(float *odata, float *idata, int in_height, int in_width,
                               int out_width, int prebin)

{
    extern __shared__ float Tile[];      //shared memory size allocated dinamically
    int Tile_dimx = blockDim.x;         // number of threads in x direction
    int Tile_dimy = blockDim.y;         // number of threads in y direction
    //int prebin_index = threadIdx.x % prebin;  // index inside prebinning group
    //OSS: prebin is a power of 2 so modulo operation can be replaced with
    //the next one
    int prebin_index = threadIdx.x & (prebin - 1);  // index inside prebinning group
    float ftemp = 0.;                   // temporary float well

    // null the shared memory
     Tile[threadIdx.x + blockDim.x * threadIdx.y] = 0.;
     __syncthreads();

    int xIndex = blockIdx.x * Tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * Tile_dimy + threadIdx.y;
    int index_in = 0;
    if ((xIndex < in_width) && (yIndex < in_height)) {
        index_in = xIndex + (yIndex) * in_width;
        // convert from unsigned to signed
        Tile[threadIdx.x+ blockDim.x * threadIdx.y] = idata[index_in] ;
    }
    __syncthreads();

    // we only proceed if first thread of prebinning group

    int index_out = 0;
    if (prebin_index == 0) {
        index_out = xIndex/prebin + (yIndex)*out_width;
        // convert to float and sum up
        for (int i = 0 ; i < prebin ; i++) {
            ftemp += Tile[threadIdx.x + i + blockDim.x * threadIdx.y];
        }
        odata[index_out] = ftemp/prebin;
    }
    return;
}

//
// __global__ void transposePreBin_shfl(float *odata, unsigned char *idata, int in_height,
//                                      int out_width, int nsamp, int prebin)
//
// odata         the output data matrix
// idata         the input  data matrix
// in_height     the input  matrix number of rows (i.e. matrix height) equal to the
//               number of freq. channels
// out_width     the output matrix number of cols (i.e. matrix width) equal to the
//               pitch dimension to get aligned  memory data
// nsamp         the number of samples per sub-integration and per channel
//               (<= than input height = rebin[0].pitch_dim)
// prebin        the prebin factor
//
// Executes the transpose of input data and convert it from unsigned to float.
// The rebinning of the input matrix is done using the CUDA _shfl_up
// function.
//
// 2018/05/29 We got  warning: function "__shfl_up(float, unsigned int, int)"
// here was declared deprecated ("__shfl_up() is deprecated in favor of
// __shfl_up_sync() and may be removed in a future release)
// A test do not shows important slowing.
//
__global__ void transposePreBin_shfl(float *odata, unsigned char *idata, int in_height,
                                                 int out_width, int nsamp, int prebin)
{
    extern __shared__ char tile[];      //shared memory size allocated dinamically
    int tile_dimx = blockDim.x;         // number of threads in x direction
    int tile_dimy = blockDim.y;         // number of threads in y direction
    int xIndex = blockIdx.x * tile_dimx + threadIdx.x;
    int yIndex = blockIdx.y * tile_dimy + threadIdx.y;
    int id = (gridDim.x * blockDim.x) * yIndex + xIndex; //the global thread id
    int lane_id = id % 32;
    float value = 0.;

    // null the shared memory
     tile[threadIdx.x + blockDim.x * threadIdx.y] = 0;
    __syncthreads();
    int index_in = xIndex + (yIndex)*in_height;
    //load data into shared memory
    if( (xIndex < in_height) && (yIndex < nsamp)) {
        tile[threadIdx.y + blockDim.y * threadIdx.x] = idata[index_in] - 128;
    }
    __syncthreads();
    //rebin data using shfl_up
    value =  (float)tile[threadIdx.x + blockDim.x * (threadIdx.y)];
    for (int i= 1; i<= prebin/2; i*= 2) {
#if __CUDACC_VER_MAJOR__ >= 9
        float n = __shfl_up_sync(0xFFFFFFFF,value, i, 32);
#else
        float n = __shfl_up(value, i, 32);
#endif
        if (lane_id >= i) {
            value += n;
        }
    }
    //store the rebinned values into the output array
    //if (((lane_id + 1) % prebin) == 0) {
    if ((((lane_id + 1) & (prebin -1))) == 0) {
        xIndex = blockIdx.y * tile_dimy + threadIdx.x;
        yIndex = blockIdx.x * tile_dimx + threadIdx.y;
        int index_out = xIndex/prebin + (yIndex)*out_width;
        odata[index_out] = value/prebin;
    }
    return;
}


//__global__ void binInputKernel_shfl(float *odata, float *idata, int in_height,
//                                    int in_width, int out_width, int prebin)
//
// odata         the output data matrix
// idata         the input data matrix
// in_width      the input matrix number of columns (width) equal to the input pitch_ dim
// out_width     the output matrix number of columns (width) equal to the output pitch_dim
// prebin        the rebinning factor (2, 4, 8 or 16)
//
//
// This kernel executes the binning of the input matrix.
// When this kernel is executed, the input matrix has been already transposed. In
// this case:
// x direction -> time samples
// y direction -> freq. channels
// The input matrix and output matrix have the same height (4096 channels)
// and different widths. The two widths are not in the 2:1 proportion
// because the row dimension of the in and out matrix is aligned to get
// coalesced i/o operations.
//
// The rebinning of the input matrix is done using the CUDA _shfl_up
// function.
// 2018/05/29 We got  warning: function "__shfl_up(float, unsigned int, int)"
// here was declared deprecated ("__shfl_up() is deprecated in favor of
// __shfl_up_sync() and may be removed in a future release)
// A test do not shows important slowing.
//
__global__ void binInputKernel_shfl(float *odata, float *idata, int in_width,
                                    int out_width, int prebin)

{
    float value = 0.;                   // input value
    int xIndex = (blockIdx.x * blockDim.x) + threadIdx.x;
    int yIndex = (blockIdx.y * blockDim.y) + threadIdx.y;
    int id = (gridDim.x * blockDim.x) * yIndex + xIndex; //the global thread id
    int lane_id = id % 32;

    //index of the element inside the indata array
    int index_in = xIndex + (yIndex) * in_width;
    value = idata[index_in] ;

    // sum prebin values
    for (int i = 1; i <= prebin >> 1; i*= 2) {
#if __CUDACC_VER_MAJOR__ >= 9
        float n = __shfl_up_sync(0xFFFFFFFF,value, i, 32);
#else
        float n = __shfl_up(value, i, 32);
#endif
        if (lane_id >= i) {
            value += n;
        }
    }
    //index of the element inside the out data array
    int index_out = xIndex/prebin + yIndex * out_width;
    //if (((lane_id + 1) % prebin) == 0) {
    if ((((lane_id + 1) & (prebin - 1))) == 0) {
        odata[index_out] = value/prebin;
    }
    return;
}
