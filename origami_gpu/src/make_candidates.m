% produce a candidates.txt file with all possibly triples
% buit with three input matrix: period, dm, pdot
% $Id: make_candidates.m 1159 2019-01-10 09:52:46Z baffa $
% 

function max_z =  make_candidates(factor, caso, start)

% check the number of arguments passes to the function
if ((nargin() < 1) || (nargin() > 3))
      printf ("usage:  make_candidates ( standard_values_multiplicative_factor, distribution_type, stating_point(s) )\n");
      printf (" distribution_type  =-1 => candidates uniformly spread on standard period space 128 + 1 \n");
      printf (" distribution_type  = 0 => candidates uniformly spread on standard period space 128 + 1 \n");
      printf (" distribution_type  = 1 => candidates uniformly spread on all optimization space 5*5*5 => 125 + 3 \n");
      printf (" distribution_type  = 2 => candidates uniformly spread on DM and period space\t 11*11 => 121 + 7 \n");
      printf (" distribution_type  = 3 => candidates uniformly spread on period space\t\t 128*1 => 128 + 1 \n");
      printf (" distribution_type  = 4 => candidates uniformly spread on dm space\t\t 128*1 => 128 + 1 \n");
      printf (" distribution_type  = 5 => candidates uniformly spread on pdot space\t\t 128*1 => 128 + 1 \n");
      printf (" - default starting points \n");
      printf (" period_real  = 0.0011234  \n");
      printf (" pdot_real    = 0.000000;  \n");
      printf (" dm_real      = 10.0;      \n");
          %
      return;
endif
if (nargin() == 1 ) % default value for caso
    caso = 1;
endif

% Factor handling
% factor is scalar 
if (1 == max(size(factor)) )
    max_z = factor;
    if ((0 >= factor) || (20 < factor)) 
        printf ("usage:  make_candidates ( standard_values_multiplicative_factor, distribution_type, stating_point(s) )\n");
        return;
    endif
    % we build all three factors
    factor_per = factor;
    factor_pdot= factor;
    factor_dm  = factor;
else
    % factor is an array 
    if ( 3 != max(size(factor)))
        printf ("Factor must either be a scalar or a 3 elements vector!\n");
        return;
    endif
    factor_per = factor(1);
    factor_pdot= factor(2);
    factor_dm  = factor(3);
    printf ("factor_period = %g  factor_pdot = %g factor_dm = %g \n", factor(1), factor(2), factor(3));
endif 

% ranges and steps    
period_range = 0.00101e-3*factor_per; % period range and step for optimization phase
period_step  = 0.0001e-3*factor_per;
pdot_range   = 0.101e-6*factor_pdot;  % pdot range and step for optimization phase
pdot_step    = 0.1e-7*factor_pdot;
dm_range     = 1.*factor_dm;          % dm range and step for optimization phase
dm_step      = 0.1*factor_dm;

% Start point handling

% default starting points
period_real  = 0.0011234 ; % period  real
pdot_real    = 0.000000;   % pdot  real
dm_real      = 10.0;       % dm  real

% start needs to be a vector
if (3 == nargin() )
    % start is an array 
    if ( 3 != max(size(start)))
        printf ("Start must either be a or a 3 elements vector!\n");
        return;
    endif
    period_real = start(1);
    pdot_real   = start(2);
    dm_real     = start(3);
    printf ("real_period = %g  real_pdot = %g real_dm = %g \n", start(1), start(2), start(3));
endif 

%
%
% We generate different candidates distribution depending of input case
% variable
%
if (caso == 0 ) 
    % Standard case candidates uniformly spread on standard period space
    % 
      printf (" distribution_type  = 0 => candidates uniformly spread on standard period space 128 + 1 \n");
    steps        = 128;
    period_start = 4.01000000e-04;
    period_stop  = 2.33372000e-02*factor_per;
    period_step  = (period_stop-period_start)/(steps-1);
    period = [ period_start:period_step:period_stop ];

    pdot_start   = pdot_real ;
    pdot_stop    = pdot_real ;
    pdot         = [ pdot_start ];

    dm_start     = dm_real ;
    dm_stop      = dm_real  ;
    dm           = [ dm_real ];

    %Candidate matrix
    candidates = make_scan(period, pdot, dm);
    candidates = [ [period_real pdot_real dm_real ] ; candidates ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];

%
%
elseif (caso == -1 ) 
    % Standard case candidates uniformly spread on long period space
    % to test rebin
    % 
    steps        = 128;
    period_start = 4.01000000e-04;
    period_stop  = 2.00000000e-00*factor_per;
    period_step  = (period_stop-period_start)/(steps-1);
    period = [ period_start:period_step:period_stop ];

    pdot_start   = pdot_real ;
    pdot_stop    = pdot_real ;
    pdot         = [ pdot_start ];

    dm_start     = dm_real ;
    dm_stop      = dm_real  ;
    dm           = [ dm_real ];

    %Candidate matrix
    candidates =  make_scan(period, pdot, dm);
    candidates = [ [period_real pdot_real dm_real ] ; candidates ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];

%
elseif (caso == 1 ) 
    % First case candidates uniformly spread on all optimization space
    % 125 + 3 => 5*5*5
      printf (" distribution_type  = 1 => candidates uniformly spread on all optimization space 5*5*5 => 125 + 3 \n");
    steps        = 5;
    period_start = period_real - period_range*0.45;
    period_stop  = period_real + period_range*0.45;
    period_step  = (period_stop-period_start)/(steps-1);
    period = [ period_start:period_step:period_stop ];

    pdot_start   = pdot_real - pdot_range*0.45;
    pdot_stop    = pdot_real + pdot_range*0.45;
    pdot_step    = (pdot_stop-pdot_start)/(steps-1);
    pdot         = [ pdot_start:pdot_step:pdot_stop ];

    dm_start     = dm_real - dm_range*0.45;
    dm_stop      = dm_real + dm_range*0.45;
    dm_step      = (dm_stop-dm_start)/(steps-1);
    dm           = [ dm_start:dm_step:dm_stop ];

    %Candidate matrix
    candidates =  make_scan(period, pdot, dm);
    candidates = [ [period_real pdot_real dm_real ] ; candidates ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];
    % we add a small increment in period to have a predictable order in
    % sort (we have many points with the same period) 
    % 20181005 Does not work.
    %siz   = size(candidates);
    %indez = 1:siz(1);
    %indez = indez * period_step / siz(1) / 100;
    %candidates(:,1) += indez' ;

elseif (caso == 2 )
    % Second case. NO pdot search 
    % 11*11 + 7 default
      printf (" distribution_type  = 2 => candidates uniformly spread on DM and period space\t 11*11 => 121 + 7 \n");
    steps        = 11;
    period_start = period_real - period_range*0.45;
    period_stop  = period_real + period_range*0.45;
    period_step  = (period_stop-period_start)/(steps-1);
    period = [ period_start:period_step:period_stop ];

    pdot_start   = pdot_real ;
    pdot_stop    = pdot_real ;
    pdot         = [ pdot_start ];

    dm_start     = dm_real - dm_range*0.45;
    dm_stop      = dm_real + dm_range*0.45;
    dm_step      = (dm_stop-dm_start)/(steps-1);
    dm           = [ dm_start:dm_step:dm_stop ];

    %Candidate matrix
    candidates =  make_scan(period, pdot, dm);
    candidates = [ [period_real pdot_real dm_real ] ; candidates ];
    for i = 1:7
        candidates = [ candidates;  [period_real pdot_real dm_real ] ];
    endfor

elseif (caso == 3 )
    % Third case. Candidate unifomly spread on period alone
    % 11*11 + 7 default
      printf (" distribution_type  = 3 => candidates uniformly spread on period space\t\t 128*1 => 128 + 1 \n");
    steps        = 127;
    period_start = period_real - period_range*0.45;
    period_stop  = period_real + period_range*0.45;
    period_step  = (period_stop-period_start)/(steps-1);
    period = [ period_start:period_step:period_stop ];

    pdot_start   = pdot_real ;
    pdot_stop    = pdot_real ;
    pdot         = [ pdot_start ];

    dm_start     = dm_real ;
    dm_stop      = dm_real  ;
    dm           = [ dm_real ];

    %Candidate matrix
    candidates =  make_scan(period, pdot, dm);
    candidates = [ [period_real pdot_real dm_real ] ; candidates ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];


elseif (caso == 4 )
    % fourth case. Candidate unifomly spread on dm alone
    % 11*11 + 7 default
      printf (" distribution_type  = 4 => candidates uniformly spread on dm space\t\t 128*1 => 128 + 1 \n");
    steps        = 127;
    period_start = period_real;
    period_stop  = period_real;
    period = [ period_real ];

    pdot_start   = pdot_real ;
    pdot_stop    = pdot_real ;
    pdot         = [ pdot_start ];

    dm_start     = dm_real - dm_range*0.45;
    dm_stop      = dm_real + dm_range*0.45;
    dm_step      = (dm_stop-dm_start)/(steps-1);
    dm           = [ dm_start:dm_step:dm_stop ];

    %Candidate matrix
    candidates =  make_scan(period, pdot, dm);
    candidates = [ [period_real pdot_real dm_real ] ; candidates ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];


elseif (caso == 5 )
    % fifth case. Candidate unifomly spread on pdot alone
    % 11*11 + 7 default
      printf (" distribution_type  = 5 => candidates uniformly spread on pdot space\t\t 128*1 => 128 + 1 \n");
    steps        = 127;
    period_start = period_real;
    period_stop  = period_real;
    period = [ period_real ];

    pdot_start   = pdot_real - pdot_range*0.45;
    pdot_stop    = pdot_real + pdot_range*0.45;
    pdot_step    = (pdot_stop-pdot_start)/(steps-1);
    pdot         = [ pdot_start:pdot_step:pdot_stop ];

    dm_start     = dm_real ;
    dm_stop      = dm_real  ;
    dm           = [ dm_real ];

    %Candidate matrix
    candidates =  make_scan(period, pdot, dm);
    candidates = [ [period_real pdot_real dm_real ] ; candidates ];
    candidates = [ candidates;  [period_real pdot_real dm_real ] ];

endif 

save ("-ascii", "candidates.txt", "candidates")
endfunction

