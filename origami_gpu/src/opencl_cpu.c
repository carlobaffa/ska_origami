#include <CL/opencl.h>
#include <folding.h>

//static _clState *clStates = NULL;       /* pinter array to single gpu thread  OpenCL info structures */
//static int cpu_devices[MAX_GPUDEVICES];
//static  cl_platform_id platform = NULL;

#define KL_CONVERT 0
#define KL_MEAN    1

typedef struct {
    size_t max_work_size;
    size_t work_size;
    int nCU;
    cl_context context;
    cl_kernel kernel[4];
    cl_command_queue commandQueue;
    cl_program program;
    //cl_mem in_buf[MAX_SLOT];
    //cl_mem out_buf[MAX_SLOT];
    cl_mem in_buf;
    cl_mem out_buf;
} _clState;

_clState *clStates = 0;

/*
 * CLErrString --
 *
 *      Utility function that converts an OpenCL status into a human
 *      readable string.
 *
 * Results:
 *      const char * pointer to a static string.
 */

static const char *
CLErrString(cl_int status) {
   static struct { cl_int code; const char *msg; } error_table[] = {
      { CL_SUCCESS, "success" },
      { CL_DEVICE_NOT_FOUND, "device not found", },
      { CL_DEVICE_NOT_AVAILABLE, "device not available", },
      { CL_COMPILER_NOT_AVAILABLE, "compiler not available", },
      { CL_MEM_OBJECT_ALLOCATION_FAILURE, "mem object allocation failure", },
      { CL_OUT_OF_RESOURCES, "out of resources", },
      { CL_OUT_OF_HOST_MEMORY, "out of host memory", },
      { CL_PROFILING_INFO_NOT_AVAILABLE, "profiling not available", },
      { CL_MEM_COPY_OVERLAP, "memcopy overlaps", },
      { CL_IMAGE_FORMAT_MISMATCH, "image format mismatch", },
      { CL_IMAGE_FORMAT_NOT_SUPPORTED, "image format not supported", },
      { CL_BUILD_PROGRAM_FAILURE, "build program failed", },
      { CL_MAP_FAILURE, "map failed", },
      { CL_INVALID_VALUE, "invalid value", },
      { CL_INVALID_DEVICE_TYPE, "invalid device type", },
      { 0, NULL },
   };
   static char unknown[25];
   int ii;

   for (ii = 0; error_table[ii].msg != NULL; ii++) {
      if (error_table[ii].code == status) {
         return error_table[ii].msg;
      }
   }

   snprintf(unknown, sizeof unknown, "unknown error %d", status);
   return unknown;
}
static void PrintDevices(cl_device_id device)
{
#define LONG_PROPS \
  defn(VENDOR_ID), \
  defn(MAX_COMPUTE_UNITS), \
  defn(MAX_WORK_ITEM_DIMENSIONS), \
  defn(MAX_WORK_GROUP_SIZE), \
  defn(PREFERRED_VECTOR_WIDTH_CHAR), \
  defn(PREFERRED_VECTOR_WIDTH_SHORT), \
  defn(PREFERRED_VECTOR_WIDTH_INT), \
  defn(PREFERRED_VECTOR_WIDTH_LONG), \
  defn(PREFERRED_VECTOR_WIDTH_FLOAT), \
  defn(PREFERRED_VECTOR_WIDTH_DOUBLE), \
  defn(MAX_CLOCK_FREQUENCY), \
  defn(ADDRESS_BITS), \
  defn(MAX_MEM_ALLOC_SIZE), \
  defn(IMAGE_SUPPORT), \
  defn(MAX_READ_IMAGE_ARGS), \
  defn(MAX_WRITE_IMAGE_ARGS), \
  defn(IMAGE2D_MAX_WIDTH), \
  defn(IMAGE2D_MAX_HEIGHT), \
  defn(IMAGE3D_MAX_WIDTH), \
  defn(IMAGE3D_MAX_HEIGHT), \
  defn(IMAGE3D_MAX_DEPTH), \
  defn(MAX_SAMPLERS), \
  defn(MAX_PARAMETER_SIZE), \
  defn(MEM_BASE_ADDR_ALIGN), \
  defn(MIN_DATA_TYPE_ALIGN_SIZE), \
  defn(GLOBAL_MEM_CACHELINE_SIZE), \
  defn(GLOBAL_MEM_CACHE_SIZE), \
  defn(GLOBAL_MEM_SIZE), \
  defn(MAX_CONSTANT_BUFFER_SIZE), \
  defn(MAX_CONSTANT_ARGS), \
  defn(LOCAL_MEM_SIZE), \
  defn(ERROR_CORRECTION_SUPPORT), \
  defn(PROFILING_TIMER_RESOLUTION), \
  defn(ENDIAN_LITTLE), \
  defn(AVAILABLE), \
  defn(COMPILER_AVAILABLE),

#define STR_PROPS \
  defn(NAME), \
  defn(VENDOR), \
  defn(PROFILE), \
  defn(VERSION), \
  defn(EXTENSIONS),

#define HEX_PROPS \
   defn(SINGLE_FP_CONFIG), \
   defn(QUEUE_PROPERTIES),


/* XXX For completeness, it'd be nice to dump this one, too. */
#define WEIRD_PROPS \
   CL_DEVICE_MAX_WORK_ITEM_SIZES,

   static struct { cl_device_info param; const char *name; } longProps[] = {
#define defn(X) { CL_DEVICE_##X, #X }
      LONG_PROPS
#undef defn
      { 0, NULL },
   };
   static struct { cl_device_info param; const char *name; } hexProps[] = {
#define defn(X) { CL_DEVICE_##X, #X }
      HEX_PROPS
#undef defn
      { 0, NULL },
   };
   static struct { cl_device_info param; const char *name; } strProps[] = {
#define defn(X) { CL_DEVICE_##X, #X }
      STR_PROPS
#undef defn
      { CL_DRIVER_VERSION, "DRIVER_VERSION" },
      { 0, NULL },
   };
   size_t size;
   cl_int status;
   int ii;
   long long val; /* Avoids unpleasant surprises for some params */

#ifdef NO
   char buf[65536];
   for (ii = 0; strProps[ii].name != NULL; ii++) {
      status = clGetDeviceInfo(device, strProps[ii].param, sizeof buf, buf, &size);
      if (status != CL_SUCCESS) {
         printf("\tdevice[%p]: Unable to get %s: %s!\n", device, strProps[ii].name, CLErrString(status));
         continue;
      }
      if (size > sizeof buf) {
         printf("\tdevice[%p]: Large %s (%d bytes)!  Truncating to %d!\n", device, strProps[ii].name, size,
                 sizeof(buf));
      }
      printf("\tdevice[%p]: %s: %s\n", device, strProps[ii].name, buf);
   }
#endif
   printf("\n");

   status = clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(val), &val, NULL);
   if (status == CL_SUCCESS) {
      printf("\tdevice[%p]: Type: ", device);
      if (val & CL_DEVICE_TYPE_DEFAULT) {
         val &= ~CL_DEVICE_TYPE_DEFAULT;
         printf("Default ");
      }
      if (val & CL_DEVICE_TYPE_CPU) {
         val &= ~CL_DEVICE_TYPE_CPU;
         printf("CPU ");
      }
      if (val & CL_DEVICE_TYPE_GPU) {
         val &= ~CL_DEVICE_TYPE_GPU;
         printf("GPU ");
      }
      if (val & CL_DEVICE_TYPE_ACCELERATOR) {
         val &= ~CL_DEVICE_TYPE_ACCELERATOR;
         printf("Accelerator ");
      }
      if (val != 0) {
         printf("Unknown (0x%llx) ", val);
      }
      printf("\n");
   } else {
      printf("\tdevice[%p]: Unable to get TYPE: %s!\n",
              device, CLErrString(status));
   }

   status = clGetDeviceInfo(device, CL_DEVICE_EXECUTION_CAPABILITIES, sizeof(val), &val, NULL);
   if (status == CL_SUCCESS) {
      printf("\tdevice[%p]: EXECUTION_CAPABILITIES: ", device);
      if (val & CL_EXEC_KERNEL) {
         val &= ~CL_EXEC_KERNEL;
         printf("Kernel ");
      }
      if (val & CL_EXEC_NATIVE_KERNEL) {
         val &= ~CL_EXEC_NATIVE_KERNEL;
         printf("Native ");
      }
      if (val) {
         printf("Unknown (0x%llx) ", val);
      }
      printf("\n");
   } else {
      printf("\tdevice[%p]: Unable to get EXECUTION_CAPABILITIES: %s!\n", device, CLErrString(status));
   }

   status = clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
                            sizeof(val), &val, NULL);
   if (status == CL_SUCCESS) {
      static const char *cacheTypes[] = { "None", "Read-Only", "Read-Write" };
      static int numTypes = sizeof(cacheTypes) / sizeof(cacheTypes[0]);

      printf("\tdevice[%p]: GLOBAL_MEM_CACHE_TYPE: %s (%lld)\n", device, val < numTypes ? cacheTypes[val] : "???", val);
   } else {
      printf("\tdevice[%p]: Unable to get GLOBAL_MEM_CACHE_TYPE: %s!\n",
              device, CLErrString(status));
   }
   status = clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(val), &val, NULL);
   if (status == CL_SUCCESS) {
      static const char *lmemTypes[] = { "???", "Local", "Global" };
      static int numTypes = sizeof(lmemTypes) / sizeof(lmemTypes[0]);

      printf("\tdevice[%p]: CL_DEVICE_LOCAL_MEM_TYPE: %s (%lld)\n", device, val < numTypes ? lmemTypes[val] : "???", val);
   } else {
      printf("\tdevice[%p]: Unable to get CL_DEVICE_LOCAL_MEM_TYPE: %s!\n", device, CLErrString(status));
   }

   for (ii = 0; hexProps[ii].name != NULL; ii++) {
      status = clGetDeviceInfo(device, hexProps[ii].param, sizeof(val), &val, &size);
      if (status != CL_SUCCESS) {
         printf("\tdevice[%p]: Unable to get %s: %s!\n",
                 device, hexProps[ii].name, CLErrString(status));
         continue;
      }
      if (size > sizeof(val)) {
         printf("\tdevice[%p]: Large %s (%d bytes)!  Truncating to %d!\n", device, hexProps[ii].name,
                                                                                  size, sizeof(val));
      }
      printf("\tdevice[%p]: %s: 0x%llx\n",
             device, hexProps[ii].name, val);
   }
   printf("\n");

   for (ii = 0; longProps[ii].name != NULL; ii++) {
      status = clGetDeviceInfo(device, longProps[ii].param, sizeof(val), &val, &size);
      if (status != CL_SUCCESS) {
         printf("\tdevice[%p]: Unable to get %s: %s!\n", device, longProps[ii].name, CLErrString(status));
         continue;
      }
      if (size > sizeof val) {
         printf("\tdevice[%p]: Large %s (%d bytes)!  Truncating to %d!\n", device, longProps[ii].name,
                                                                                   size, sizeof(val));
      }
      printf("\tdevice[%p]: %s: %lld\n", device, longProps[ii].name, val);
   }
}

int init_CL(unsigned int ncpu, int ndev, cl_device_id *devices, _clState *cls)
{
    char pbuff[128];
    cl_int status;

    iolog(2, "_init_CL: &devices[%d]: %p\n", ncpu, &devices[ncpu]);
    status = clGetDeviceInfo(devices[ncpu], CL_DEVICE_NAME, sizeof(pbuff), pbuff, NULL);
    if (status != CL_SUCCESS) {
        errlog("Error: Getting Device Info\n");
        return EXIT_FAILURE;
    }
    iolog(1, "Selected ncpu num. %i: %s\n", ncpu, pbuff);
    //strncpy(name, pbuff, nameSize);
    cl_uint v_id;
    status = clGetDeviceInfo(devices[ncpu], CL_DEVICE_VENDOR_ID, sizeof(cl_uint), &v_id, NULL);
    iolog(1, "Vendor ID: %u\n", v_id);
    status|= clGetDeviceInfo(devices[ncpu], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint),
                                     &cls->nCU,  NULL);
    status|= clGetDeviceInfo(devices[ncpu], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t),
                                      &cls->max_work_size, NULL);

    if (status != CL_SUCCESS) {
        errlog("Error: Getting Device Info (err = %d)\n", status);
        return EXIT_FAILURE;
    }

    cls->context = clCreateContext(NULL, ndev, devices, NULL, NULL, &status);
    if (status != CL_SUCCESS) {
        errlog("Error: Creating Context. (err = %d)\n", status);
        return EXIT_FAILURE;
    }
    iolog(1, "context: %p\n", cls->context);

    cls->commandQueue = clCreateCommandQueue(cls->context, devices[ncpu],
                                                 CL_QUEUE_PROFILING_ENABLE,
                                                                  &status);
    iolog(1, "commandQueue: %p\n", cls->commandQueue);
    if (status != CL_SUCCESS) {
        errlog("Failed to create a command queue! (err = %d)\n", status);
        return EXIT_FAILURE;
    }

    /* going to open the kernel source file */
    FILE *fp = fopen("kernel.cl", "rb");
    if (fp == NULL ) {
        errlog("Can't open kernel file\n");
        return EXIT_FAILURE;
    }
    fseek(fp, 0, SEEK_END );
    const size_t size = ftell(fp);
    const char *kernel_source = (const char *) malloc(size);
    rewind(fp);
    if (fread((void *) kernel_source, 1, size, fp ) < size) {
        return EXIT_FAILURE;
    }
    fclose(fp);
    cls->program = clCreateProgramWithSource(cls->context,
                                                        1,
                                           &kernel_source,
                                          &size, &status);
    free((void*)kernel_source);
    kernel_source = NULL;
    if (status != CL_SUCCESS) {
        errlog("Error in clCreateProgramWithSource (err = %d)\n", status);
         return EXIT_FAILURE;
    }

    status = clBuildProgram(cls->program, 1, &devices[ncpu], "-cl-fast-relaxed-math", NULL, NULL );
    //status = clBuildProgram(cls->program, 1, &devices[ncpu], NULL, NULL, NULL );
    if (status != CL_SUCCESS) {
        errlog( "ncpu-%d:Error in BuildProgram (error: %d)\n", ncpu, status);
        size_t logSize;

        /* get log buffer size in bytes */
        status = clGetProgramBuildInfo(cls->program,
                               devices[ncpu],
                               CL_PROGRAM_BUILD_LOG,
                               0, NULL, &logSize);
        /* allocate the memory to store log output */
        char *log = malloc(logSize);
        if (log) {
            status = clGetProgramBuildInfo(cls->program, devices[ncpu], CL_PROGRAM_BUILD_LOG,
                    logSize, log, NULL);

            printf("compilation results-/%d/%d/-{%s}-\n",logSize, status, log);
            free(log);
        }
        return EXIT_FAILURE;
    }

    cls->kernel[KL_CONVERT] = clCreateKernel(cls->program, "transpose_kernel", &status);
    if (status != CL_SUCCESS) {
        errlog("Error: (1) Creating read Kernel from program\n");
        return EXIT_FAILURE;
    }
    int i = 0;
    size_t mem_size = fold.nsamp*NBYTES/fold.nsubints;
    size_t mem_devsize = pitch_dim * fold.nchan * NBYTES;
    iolog(1, "mem_devsize: %ld", mem_devsize);
    //for (i = 0; i < MAX_SLOT; i ++) {
        //cls->in_buf[i] = clCreateBuffer(cls->context, CL_MEM_READ_ONLY, mem_size, NULL, &status);
        cls->in_buf = clCreateBuffer(cls->context, CL_MEM_READ_ONLY|CL_MEM_ALLOC_HOST_PTR , mem_size, NULL, &status);
        if (status != CL_SUCCESS) {
           errlog("opencl_cpu: Failed to allocate in_buf device memory! (%s) \n", CLErrString(status));
            return EXIT_FAILURE;
        }
        //cls->out_buf[i] = clCreateBuffer(cls->context, CL_MEM_READ_WRITE, mem_devsize, NULL, &status);
        cls->out_buf = clCreateBuffer(cls->context, CL_MEM_READ_WRITE|CL_MEM_ALLOC_HOST_PTR , mem_devsize, NULL, &status);
        if (status != CL_SUCCESS) {
            errlog("opencl_cpu: Failed to allocate %d bytes in CPU device memory! (%s) \n", mem_devsize,
                                                                                   CLErrString(status));
            return EXIT_FAILURE;
        }
        //iolog(1, "out_buf[%d]: %p", i, cls->out_buf[i]);
        iolog(1, "out_buf: %p in_buf: %p", cls->out_buf, cls->in_buf);
    //}
    return EXIT_SUCCESS;

}

int  clDeviceNum(void)
{
    cl_int err;
    // Trying to identify one platform:

    cl_uint num_platforms;
    cl_uint num_devices;
    cl_platform_id *platformList;
    cl_device_id *deviceList = 0;

    if ((err = clGetPlatformIDs(0, NULL, &num_platforms)) != CL_SUCCESS) {
        errlog("Error: Failed to get a platform id! (error: %d)\n", err);
        return EXIT_FAILURE;
    }
    iolog(1,"num_platforms: %d\n", num_platforms);
    platformList = malloc(sizeof(cl_platform_id) * num_platforms);
    if ((err = clGetPlatformIDs(num_platforms, platformList, NULL)) != CL_SUCCESS) {
        errlog("Unable to enumerate the platforms: %s\n", CLErrString(err));
        return EXIT_FAILURE;
    }

    unsigned int i;
    size_t size;
    char pbuff[256];
    for (i = 0; i < num_platforms; ++i) {
        err = clGetPlatformInfo(platformList[i], CL_PLATFORM_VENDOR, sizeof(pbuff), pbuff, &size);
        if (err != CL_SUCCESS) {
            errlog("Error: Getting Platform Info.\n");
            continue;
        }

        clGetPlatformInfo(platformList[i], CL_PLATFORM_NAME, sizeof(pbuff), pbuff, NULL);
        iolog(1, "Platform Name: %s\n", pbuff);
        clGetPlatformInfo(platformList[i], CL_PLATFORM_VENDOR, sizeof(pbuff), pbuff, NULL);
        iolog(1, "Platform Vendor: %s\n", pbuff);
        if (strncmp(pbuff, "Advanced Micro Devices", 20) == 0) {
            break;
        }
    }
    err = clGetDeviceIDs(platformList[i], CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
    if (err != CL_SUCCESS) {
        errlog("Failed to collect device list on this platform!\n");
        free(platformList);
        return EXIT_FAILURE;
    }
    printf("platform[%d]: Found %d device(s)\n", i,  num_devices);
    deviceList = malloc(num_devices * sizeof(cl_device_id));
    if ((err = clGetDeviceIDs(platformList[i], CL_DEVICE_TYPE_ALL, num_devices, deviceList, NULL)) != CL_SUCCESS) {
        errlog("platform[%d]: Unable to enumerate the devices: %s\n",
                                  i, CLErrString(err));
        free(deviceList);
    }
    free(platformList);
    int n;
#ifdef NO
    for (n = 0; n < num_devices; n ++) {
        PrintDevices(deviceList[n]);
    }
#endif
   if (clStates == NULL) {
       clStates = (_clState *) calloc(num_devices, sizeof(_clState));
       if (clStates == NULL) {
           errlog("initCl: (2) Error in memory allocation!\n");
           free(deviceList);
           return EXIT_FAILURE;
       }
    }
    for (n = 0; n < num_devices; n ++) {
        init_CL(n, num_devices, deviceList, &clStates[n]);
    }
    free(deviceList);
    return num_devices;
}


/*
 * int cpuTranspose_CL(int ncpu, int index, void *in_ptr, void* out_ptr, int size_x,  int size_y,
 *                     int nsamp_per_subint, int memsize, int mem_devsize)
 */
int cpuTranspose_CL(int ncpu, int index, void *in_ptr, void* out_ptr,  int size_x,  int size_y,
                    int nsamp_per_subint, int memsize, int mem_devsize)
{
    int i;
    cl_int ret;
    cl_event ev=0;
    cl_uint blockSize = 32;
    size_t localmem_size = blockSize * blockSize * sizeof(char);


    iolog(1, "cpuTranspose_CL mem_devsize: %d size_y: %d\n", mem_devsize, size_y);
    size_t global_work_size[2] = {size_x, size_y};
    //size_t local_work_size[2] = {32, 32};
    size_t local_work_size[2] = {blockSize, blockSize};
    _clState *cls = &clStates[ncpu];
    cl_kernel kernel = cls->kernel[KL_CONVERT];
    cl_command_queue queue = cls->commandQueue;
    //cl_mem in_dev = cls->in_buf[index];
    //cl_mem out_dev = cls->out_buf[index];

    //unsigned char *p = (unsigned char*)in_ptr;
    //if ((clEnqueueWriteBuffer(queue, in_dev, CL_TRUE, 0, memsize, in_ptr, 0, NULL, NULL)) != CL_SUCCESS) {
    iolog(1, "out_buf: %p in_buf: %p", cls->out_buf, cls->in_buf);
    cl_int err = 0;
    if ((err = clEnqueueWriteBuffer(queue, cls->in_buf, CL_TRUE, 0, memsize, in_ptr, 0, NULL, NULL)) != CL_SUCCESS) {
        errlog("Error: Failed to write to source array! (%d)", err);
        return EXIT_FAILURE;
    }
    /* provision for variable argouments number */
    i = 0;
    if (NULL != cls->in_buf) {
        iolog(-1," KernelExecute arg 1 = %p", cls->in_buf);
        //clSetKernelArg(kernel, i++, sizeof(void *),  (void *) &in_dev);
        clSetKernelArg(kernel, i++, sizeof(cl_mem),  (void *) &cls->in_buf);
    }
    if (NULL != cls->out_buf) {
        iolog(-1," KernelExecute arg 2 = %p", cls->out_buf);
        clSetKernelArg( kernel, i++, sizeof(cl_mem),  (void *) &cls->out_buf);
    }
    clSetKernelArg(kernel, i++, sizeof(cl_uint), (cl_uint *) &size_x);
    clSetKernelArg(kernel, i++, sizeof(cl_uint), (cl_uint *) &size_y);
    clSetKernelArg(kernel, i++, sizeof(cl_uint), (cl_uint *) &nsamp_per_subint);
    clSetKernelArg(kernel, i++, (size_t)localmem_size, NULL);
    ret = clEnqueueNDRangeKernel(queue,
                                 kernel,
                                 2,
                                 NULL,
                                 global_work_size,
                                 local_work_size,
                                 0, NULL, &ev );

    if (ret != CL_SUCCESS) {
         errlog("Error:  Failed to enqueue kernel! (err: %d)\n", ret);
         return EXIT_FAILURE;
    }

    ret = clEnqueueReadBuffer(queue,
                            //out_dev,
                           cls->out_buf,
                           CL_FALSE,
                                  0,
                           mem_devsize,
                             out_ptr,
                            0, NULL,
                               &ev);

    if (ret != CL_SUCCESS) {
        errlog("Error: Failed to readl! (err = %d)\n", ret);
        return EXIT_FAILURE;
    }
    clFlush(queue);
    clWaitForEvents( 1, &ev );
    cl_ulong startime;
    cl_ulong endtime;
    ret = clGetEventProfilingInfo(ev, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong),
                                                                         &startime, NULL);
    iolog(3, "krl startime :%lf\n" , (double)startime);
    ret |= clGetEventProfilingInfo(ev, CL_PROFILING_COMMAND_END, sizeof(cl_ulong),
                                                                         &endtime, NULL);
    if (ret != CL_SUCCESS) {
        fprintf(stderr, "oops! Error in clGetEventProfilingInfo (1)\n");
    }
    iolog(1, "Time for transpose parallel execution in CPU: %lf (msec)\n", (double)(endtime -startime)/1.e6);
    clReleaseEvent(ev);
    return EXIT_SUCCESS;
}

