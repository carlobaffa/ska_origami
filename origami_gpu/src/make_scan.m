% produce a scan array for candidates.txt file with all possibly triples
% buit with three input arrays: period, dm, pdot
% $Id: make_scan.m 947 2017-05-15 10:21:19Z baffa $
% 

function candidates =  make_scan(period, dm, pdot)

% check the number of arguments passes to the function
 if ((nargin() < 3) || (nargin() > 3))
      printf ("usage: candidates =  make_scan(period, dm, pdot)\n");
      return;
 endif
 cand = [];
 xdim = size(period)(2)
 ydim = size(dm)(2)
 zdim = size(pdot)(2)

 for (x = 1:xdim) 
     for (y = 1:ydim) 
         for (z = 1:zdim) 
            cand = [cand; [period(x) dm(y) pdot(z) ] ];
         endfor
     endfor
 endfor
 
 candidates = cand;
 return 
