% Mesh plot of profile processing in origami
% $Id: display_profile.m 1144 2018-11-05 10:26:50Z baffa $
% It assume we always have 64 subints!!!!!!
% 
% 

function max_z =  display_profile(filenumber)

filepath="/var/tmp/";
filepath="./";
% check the number of arguments passes to the function
 if ((nargin() < 1) || (nargin() > 3))
      printf ("usage: max_rms = display_profile ( file, [xrow,yrow])\n");
      return;
 endif
 if (nargin() < 2)
    xrow = 64;
 endif
%
 if ((filenumber < 0) || (filenumber > 128))
    printf("Inconsistent filenumber\n");
    return ;
 endif
 if ((xrow < 16) || (xrow > 64))
    printf("Inconsistent xrow\n");
    return ;
 endif

% we analyze profile_000.opt profile_000.out and reduced_profile_000.opt
% reduced_profile_000.out. Their mening is:
% profile_000.out           data folded into 64 subints
% profile_000.opt           data folded into 64 subints after optimization
% reduced_profile_000.out   data folded and summed over subints
% reduced_profile_000.opt   optimized data folded and summed over subints
%
%
filepro___ = sprintf("%sprofile_%03d.out",filepath,filenumber)
fileproopt = sprintf("%sprofile_%03d.opt",filepath,filenumber)
filered___ = sprintf("%sreduced_profile_%03d.out",filepath,filenumber)
fileredopt = sprintf("%sreduced_profile_%03d.opt",filepath,filenumber)
ap_ = load(filepro___);
apo = load(fileproopt);
ar_ = load(filered___);
aro = load(fileredopt);
% if called as  mesh_optimize(file) we get xrow,yrow from first row of file
whos
qp_ = size(ap_);
qpo = size(apo);
qr_ = size(ar_);
qro = size(aro);

 if ((qp_(1) != qpo(1)) || (qp_(2) != qpo(2)))

    printf ("Inconsistent dimensions in files\n");
    return ;
 endif

xrow = 64
yrow = qp_(1)/xrow

% profile_000.out           data folded into 64 subints
    subplot(2,2,1);
    a = ap_;
    x = a(:,1);
    y = a(:,2);
    z = a(:,3);
    %
    zz = reshape(z,yrow,xrow);
    %whos
    yy = y(1:yrow);
    xx = x(1:yrow:(xrow*yrow));
    deltax = 2*(max(xx)-min(xx))/(max(xx)+min(xx))
    xx = (xx - mean(xx))/mean(xx);
    yy = (yy - mean(yy))/mean(yy);
    mesh(xx,yy,zz);
    grid on
    xlabel('subint number');
    ylabel('phase number');
    zlabel('sygnal');
    title('data folded into 64 subints')

% profile_000.opt           data folded into 64 subints after optimization
    subplot(2,2,2);
    a = apo;
    x = a(:,1);
    y = a(:,2);
    z = a(:,3);
    %
    zz = reshape(z,yrow,xrow);
    %whos
    yy = y(1:yrow);
    xx = x(1:yrow:(xrow*yrow));
    deltax = 2*(max(xx)-min(xx))/(max(xx)+min(xx))
    xx = (xx - mean(xx))/mean(xx);
    yy = (yy - mean(yy))/mean(yy);
    mesh(xx,yy,zz);
    grid on
    xlabel('subint number');
    ylabel('phase number');
    zlabel('sygnal');
    title('optimized data folded into 64 subints')

% reduced_profile_000.out   data folded and summed over subints
    subplot(2,2,3);
    a = ar_;
    x = a(:,1);
    z = a(:,2);
    %
    plot(x,z,'r-');
    grid on
    xlabel('phase number');
    ylabel('sygnal');
    title('data folded and summed over subints')

% reduced_profile_000.out   data folded and summed over subints
    subplot(2,2,4);
    a = aro;
    x = a(:,1);
    z = a(:,2);
    %
    plot(x,z,'r-');
    grid on
    xlabel('phase number');
    ylabel('sygnal');
    title('optimized data folded and summed over subints')


max_z = (max(z));
