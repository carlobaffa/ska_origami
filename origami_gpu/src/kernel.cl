#if defined(cl_amd_printf)
#pragma OPENCL EXTENSION cl_amd_printf : enable
#endif


//__kernel void transpose_kernel(global unsigned char *idata, global unsigned char *odata, int width, int height, int nsamp)
__kernel void transpose_kernel(__global unsigned char *idata, __global unsigned char *odata, int width, int height, int nsamp, __local  unsigned char *tile)
{
   /* __local unsigned char tile[32][32]; */
    int tile_dimx = get_local_size(0);
    int tile_dimy = get_local_size(1);
    int tid_x = get_local_id(0);
    int tid_y = get_local_id(1);
    int gid_x = get_group_id(0);
    int gid_y = get_group_id(1);


    /* null the shared memory */
    //tile[tid_y][tid_x] = 0;
    tile[tid_y + tid_x * tile_dimx] = 0;
    barrier( CLK_LOCAL_MEM_FENCE );

    int xIndex = gid_x * tile_dimx + tid_x;
    int yIndex = gid_y * tile_dimy + tid_y;

    if ((xIndex < width) && (yIndex < nsamp)) {
        int index_in = xIndex + (yIndex)*width;
        //tile[tid_y][tid_x] = (float)idata[index_in];
        tile[tid_y + tile_dimx * tid_x] = (float)idata[index_in];
    }

    barrier( CLK_LOCAL_MEM_FENCE );

    xIndex = gid_y * tile_dimy + tid_x;
    yIndex = gid_x * tile_dimx + tid_y;
    int index_out = xIndex + (yIndex)*height;
    //odata[index_out] = tile[tid_x][tid_y]; 
    odata[index_out] = tile[tid_x + tile_dimy * tid_y]; 
}


