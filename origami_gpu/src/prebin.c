/* $Id: prebin.c 562 2016-07-25 15:23:23Z baffa $*/
/**
 * @file prebin.c
 * @brief Converts 4096 frequency channels data files into 1024 channels files.
 * @author Carlo Baffa
 * @version 1.0
 * @date 2016-07-19
 */

//#include <folding.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <stdarg.h>
#include <ctype.h>
#include <sched.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
//
#include <math.h>
#include "../config.h"

/* local variables */

#define GROUP 1024
#define GROUP4 4096

#ifdef NO
/*
 * void print_help(void)
 */
void print_help(void)
{
    printf("Usage:  %s options [ inputfile ... ]\n", program_name);
    printf( "  -h  --help              Display this usage information.\n"
            "  -v  --verbose  flag     Print verbose messages                   (Default 0)\n"
            "  -g  --ngpu     num      the number of gpu in use                 (Default 1)\n"
            "  -t  --tsamp    time     the sampling time (usec)                 (Default 50)\n"
            "  -T  --tobs     time     the observ. time (sec)          [1- 600] (Default 60)\n"
            "  -F  --input    file     the input data file                      (Default ska.dat)\n"
            "  -n  --nsamp    num      the number of samples                    (Default 12e5)\n"
            "  -c  --nchan    num      the number freq. channels      [64-4096] (Default 4096) \n"
            "  -C  --ncand    num      the number of pulsar candidates [1- 256] (Default 128) \n"
            "  -f  --fch1     val      the first channel freq (MHz)             (Default 1550)\n"
            "  -b  --foff     val      channel bandwith (MHz)                   (Default 0.075)\n"
            "  -p  --period   val      pulsar period (ms)                       (Default 1.1236)\n"
            "  -P  --pdot     val      pulsar pdot (ms/s)                       (Default 0.)\n"
            "  -d  --dm       val      dispersion measure                       (Default 12)\n"
            "  -B  --nbins    val      max folding bins                [16-256] (Default 128)\n"
            "  -r  --rebin    val      pre-folding rebin (power of 2)  [1 - 16] (Default 1)\n"
            "  -R  --Rebin    val      pre-folding rebin strategy      [1 -  2] (Default 1)\n"
            "  -i  --nsubint  val      folding sub integ               [64-256] (Default 64)\n"
            "  -s  --nsubband val      folding subbands                [1 -128] (Default 64) \n"
            "  -N  --nosplit  val      disable folding phase shift              (Default split on) \n"
            "  -e  --enable   val      enable data check for rebin val [1 - 16] (Default  1) \n"
            "  -l  --log               logging on file                          (Default off) \n"
            "  -o  --varop             enable variable step optimization        (Default off) \n"
            "  -w  --write             enable output profile writing            (Default off) \n"
          );
}
#endif
/*
 * int main(int argc, char **argv)
 */
int main(int argc, char **argv)
{
    long i;                        // loop
    int j, z;                      // loop
    float pozzo = 0.;              // intermediate sum
    unsigned char samples[GROUP4+2]; // read buffer
    unsigned char res[GROUP+2];      // result
    uint64_t nsamp = 0;            // number of data samples
    size_t chunk_size = 4;         // host memory chunk size (in bytes)
    size_t count = 0;              // data read or written
    struct stat file_stat;         // file structure to get dimension
    time_t now;
    int   InFptr = 0;              // pointer to In  file
    int   OutFptr = 0;             // pointer to Out file
    char * InputFile;              // pointer to input data file name
    char _Inputfile[250];          // custom input data file name

    fprintf(stderr, "\n");
    now = time(NULL);


    if ((0 >= strlen(argv[1])) || (100 < strlen(argv[1]))) {
        printf("Invalid input filename we use ska.dat\n");
        sprintf(_Inputfile, "%s", "ska.dat");
    } else {
        sprintf(_Inputfile, "%s", argv[1]);
    }
    InputFile = _Inputfile;
    InFptr = open(InputFile, O_RDONLY);
    if (InFptr < 0) {
        printf("Can't open in file (%d)\n", errno);
        exit(-1);
    }
    // get info about file dimension
    if (stat(InputFile, &file_stat) < 0) {
        if (errno == ENOENT) {
            printf("%s doesn't exist",InputFile);
            return EXIT_FAILURE;
        }
        printf("Failure in stat!(error = %d)", errno);
        //return EXIT_FAILURE;
    }
    printf( "file size: %ld (bytes) \n", (uint64_t)file_stat.st_size);

    printf( "out file ska_out.dat\n");
    if ((OutFptr = open("ska_out.dat", O_CREAT | O_RDWR , 0660)) < 0) {
        perror("Can't open out file\n");
        printf("Can't open out file (%d)", errno);
        exit(-1);
    }
    //
    nsamp = file_stat.st_size;
    // loop on data
    for (i = 0 ; i < nsamp ; i += GROUP4) {
        if (GROUP4 != (count = read(InFptr, samples, GROUP4))) {
            printf("error reading in file (%ld/%d)\n",i,(int)count);
            exit(-1);
        }
        unsigned char * punt;
        for (z = 0 ; z < GROUP ; z++) {
            punt = & (samples[z*4]);
            pozzo = 0.;
            for (j = 0; j < chunk_size; j++) {
                //pozzo += samples[j];
                pozzo += ((int) ((unsigned int) punt[j])) - 128.;
            }
            res[z] = (unsigned char) ((pozzo/4.) + 0.5 + 128.) ; // rounding
#ifdef NO
            if (4 > z) {
                printf("-samples[0-3+%d] = %hu %hu %hu %hu \n",
                        z, samples[0+z*4],  samples[1+z*4],  samples[2+z*4],  samples[3+z*4]);
                printf("-punt[0-3] = %hu %hu %hu %hu ; pozzo = %f ; res[%i] = %hu\n",
                        punt[0],  punt[1],  punt[2],  punt[3], pozzo, z, res[z]);
            }
#endif
        }

        //if (0==i) exit(0);
        if (GROUP != (count = write(OutFptr, &res, GROUP))) {
            perror("Can't write to file");
            printf("error writing file (%ld/%d)\n",i,(int)count);
            exit(-1);
        }
        if (0 == (i%(1024*1024*4))) {
            printf("%ld   \t%.1f   \r", i/1024/1024, (float)i/(float)nsamp*100);
        }
    }
    printf("\n");

    close(InFptr);
    close(OutFptr);
    printf("Total time = %ds\n", (int)(time(NULL) - now));

    exit(0);
}


