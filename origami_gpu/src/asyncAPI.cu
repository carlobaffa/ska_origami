#include <stdio.h>

// includes CUDA Runtime
#include <cuda_runtime.h>

__global__ void increment_kernel(int *g_data, int inc_value)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    g_data[idx] = g_data[idx] + inc_value;
}

__global__ void casting_kernel(unsigned char *data, float *input, int n)
{
    __device__ __shared__ char line;

    int i ;
    int idx = threadIdx.x + blockIdx.x * blockDim.x;

    for (i = idx; i < n; i += gridDim.x * blockDim.x) {
        line = data[i] - 128;
        input[i] = line;
    }
}

int correct_output(int *data, const int n, const int x)
{
    for (int i = 0; i < n; i++)
        if (data[i] != x)
        {
            printf("Error! data[%d] = %d, ref = %d\n", i, data[i], x);
            return 0;
        }

    return 1;
}

extern "C" int asyncAPI_old(int ndev)
{

    int n = 16 * 1024 * 1024;
    int nbytes = n * sizeof(int);
    int value = 26;

    cudaSetDevice(ndev);
    // allocate host memory
    int *a = 0;
    cudaMallocHost((void **)&a, nbytes);
    memset(a, 0, nbytes);

    // allocate device memory
    int *d_a=0;
    cudaMalloc((void **)&d_a, nbytes);
    cudaMemset(d_a, 255, nbytes);

    // set kernel launch configuration
    dim3 threads = dim3(512, 1);
    dim3 blocks  = dim3(n / threads.x, 1);

    // create cuda event handles
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

//    StopWatchInterface *timer = NULL;
//    sdkCreateTimer(&timer);
//    sdkResetTimer(&timer);

    cudaDeviceSynchronize();
    float gpu_time = 0.0f;

    // asynchronously issue work to the GPU (all to stream 0)
    //   sdkStartTimer(&timer);
    cudaEventRecord(start, 0);
    cudaMemcpyAsync(d_a, a, nbytes, cudaMemcpyHostToDevice, 0);
    increment_kernel<<<blocks, threads, 0, 0>>>(d_a, value);
    cudaEventRecord(stop, 0);
   // sdkStopTimer(&timer);

    // have CPU do some work while waiting for stage 1 to finish
    unsigned long int counter=0;

    while (cudaEventQuery(stop) == cudaErrorNotReady)
    {
        counter++;
    }
    cudaEventElapsedTime(&gpu_time, start, stop);

    // print the cpu and gpu times
    printf("time spent executing by the GPU: %.2f\n", gpu_time);
    //printf("time spent by CPU in CUDA calls: %.2f\n", sdkGetTimerValue(&timer));
    printf("CPU executed %lu iterations while waiting for GPU to finish\n", counter);

    // check the output for correctness
    bool bFinalResults = (bool)correct_output(a, n, value);

    // release resources
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    cudaFreeHost(a);
    cudaFree(d_a);

    cudaDeviceReset();

    return (bFinalResults ? EXIT_SUCCESS : EXIT_FAILURE);
}

extern "C" int asyncAPI(int ndev, unsigned char *d_in, int nsamp, float *d_out)
{

    cudaSetDevice(ndev);
    // set kernel launch configuration
    dim3 threads = dim3(512, 1);
    dim3 blocks  = dim3(nsamp / threads.x, 1);

    // create cuda event handles
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaDeviceSynchronize();
    float gpu_time = 0.0f;

    cudaEventRecord(start, 0);
    casting_kernel<<<blocks, threads, 0, 0>>>(d_in, d_out, nsamp);
    cudaEventRecord(stop, 0);

    // have CPU do some work while waiting for stage 1 to finish
    unsigned long int counter=0;

    while (cudaEventQuery(stop) == cudaErrorNotReady)
    {
        counter++;
    }
    cudaEventElapsedTime(&gpu_time, start, stop);

    // print the cpu and gpu times
    printf("time spent executing by the GPU: %.2f\n", gpu_time);
    //printf("time spent by CPU in CUDA calls: %.2f\n", sdkGetTimerValue(&timer));
    printf("CPU executed %lu iterations while waiting for GPU to finish\n", counter);

    // check the output for correctness
    //bool bFinalResults = (bool)correct_output(a, n, value);

    // release resources
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    return 0;
}
