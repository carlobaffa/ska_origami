% plot the results of FLDo). We assume candidates varie only on 
% a single axis (1=period, 2=dm, 3=pdot). Size of dots are sqrt(s/n)

function res_plot_1d(axis);

% check the number of arguments passes to the function
if (nargin() < 1) 
    axis = 1;
    printf("we assume variability only on period\n");
endif
if ((1 > axis) || (3 < axis)) 
    axis = 1;
    printf("we assume variability only on period\n");
endif

load candidates.txt
load result.txt
% variability along period
if (axis == 1) 
    p = candidates(:,1)*1000;
    p = result(:,8)*1000;
    f = result(:,5)*1000;
    siz = size(f)(1);
    p = p(1:siz);
    sn = result(:,3);
    scatter(p,f,sqrt(sn),"filled")
    xlabel('candidate period (msec)'); 
    ylabel('optimized period (msec)'); 
    title('Optimized value versus input value. Size is sqrt of S/N');
    std_per = std(f)
    mean_per= mean(f)

% variability along dm
elseif (axis == 2) 
    p = candidates(:,3);
    f = result(:,10);
    f = result(:,7);
    siz = size(f)(1);
    p = p(1:siz);
    sn = result(:,3);
    scatter(p,f,sqrt(sn),"filled")
    xlabel('candidate DM'); 
    ylabel('optimized DM'); 
    title('Optimized value versus input value. Size is sqrt of S/N');
    std_dm = std(f)
    mean_dm= mean(f)

% variability along pdot
elseif (axis == 3) 
    p = candidates(:,3);
    p = result(:,9);
    f = result(:,6);
    siz = size(f)(1);
    p = p(1:siz);
    sn = result(:,3);
    scatter(p,f,sqrt(sn),"filled")
    xlabel('candidate pdot'); 
    ylabel('optimized pdot'); 
    title('Optimized value versus input value. Size is sqrt of S/N');
    std_pdot = std(f)
    mean_pdot= mean(f)
endif

endfunction
