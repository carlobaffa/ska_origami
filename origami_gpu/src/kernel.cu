/* $Id: kernel.cu 1192 2019-04-24 09:40:32Z baffa $ */

#include <../config.h>

// constant and global device memory
__device__ __constant__ double _nu[MAXCANDIDATES];
__device__ __constant__ double _nudot[MAXCANDIDATES];
__device__ __constant__ double _delta_freq[MAXFREQUENCIES];
__device__ __constant__ double _time_sum[MAXSUBINT];
__device__ __constant__ double _freq_sum[MAXBANDS];
__device__ __constant__ float  _dm[MAXCANDIDATES];
__device__ __constant__ int    _nbins[MAXCANDIDATES];
__device__ __constant__ int    _nchan_per_band;
__device__ __constant__ int    _nsubints;
__device__ __constant__ double _tsamp;

//#define TRANSPOSE_SHFL  1     //enable this line to use __shfl cuda functions!!
float *d_out = 0;       // device global memory to store input values after conversion
float *D_folded = 0;    // device global memory with folded profiles
float *d_weight = 0;    // device global memory with phases weights
float *d_adjusted = 0;  // device global memory with adjusted folded profiles
float *D_outfprof = 0;  // device global memory with profiles summed in freq up and weightned
float *D_outprof = 0;   // device global memory with summed up profiles
float *D_bestprof = 0;  // device global array with final profile for each trial
float *D_best = 0;      // device global array with best S/N and width for each trial
double *d_phase_shift=0; // device global array to store phase shifts dduring optimization phase
float *H_outprof = 0;   // host global array to copy back D_outprof content (for TEST purpose!!)
float *H_outfprof = 0;  // host global array to copy back D_outprof content (for TEST purpose!!)
float *Host_mtx = 0;    // host global array to copy back d_out content (for TEST purpose!!)
float *H_folded = 0;    //host global array to copy back D_folded content (for TEST purpose!!)
float *Host_best = 0;

double *Opt_period = 0;
float  *Opt_dm     = 0;
double *Opt_pdot   = 0;


float mean = 0.;        // data mean value
float sigma= 0.;        // mean sigma raw value

/*
 * void copy_to_host(binning_t rebin)
 */
/**
 *
 * This function is called for test/check purpose.
 * It writes back to the host some useful results.
 *
 */
void copy_to_host(binning_t *rebin )
{
    cudaEvent_t stop;

    if (EnableCheck == MYTRUE) {
        int idx = -1;
        int first_bin_idx = first_valid_binidx(rebin);
        if ((idx = is_binvalue_valid(fold.prebin, rebin)) < 0) {
            idx = first_bin_idx;
        }
        iolog(1, "copy_to_host fold.prebin: %d idx: %d (%d)\n", fold.prebin, idx, rebin[idx].rebin);
        uint64_t mem_size = fold.nchan * rebin[idx].pitch_dim * sizeof(float);
        if (Host_mtx) {
            cudaMemcpyAsync(Host_mtx, rebin[idx].d_out, mem_size, cudaMemcpyDeviceToHost, 0);
        }
        cudaEventCreate(&stop);
        cudaEventRecord(stop, 0);

        // have CPU do some work while waiting for stage 1 to finish
        unsigned long int counter=0;

        while (cudaEventQuery(stop) == cudaErrorNotReady)
        {
            counter++;
        }
        cudaEventDestroy(stop);
    }
    if (EnableWrite == MYTRUE) {
        uint64_t mem_size = fold.nbins * fold.nsubints * fold.nsubbands * sizeof(float) * fold.ncand;
        cudaMemcpyAsync(H_folded, D_folded, mem_size, cudaMemcpyDeviceToHost, 0);
        cudaEventCreate(&stop);
        cudaEventRecord(stop, 0);

        // have CPU do some work while waiting for stage 1 to finish
        unsigned long int counter=0;

        while (cudaEventQuery(stop) == cudaErrorNotReady)
        {
            counter++;
        }
        cudaEventDestroy(stop);
    }
}

/*
 * int rebinInputData(int previous, int current,  binning_t *rebin)
 */
/*
 * Call the kernel that executes rebinning on the input data.
 * The input data are already transposed and converted to float.
 *
 * \param previous     the first rebin value to act upon
 * \param current      the current rebin value
 * \param rebin        the array of rebin structures
 *
 * \return EXIT_SUCCESS, on success, EXIT_FAILURE otherwise.
 */
int rebinInputData(int previous, int current,  binning_t *rebin)
{
    dim3 threadsPerBlock;       // number of kernel threads per block
    dim3 blocksPerGrid;         // number of kernel blocks for grid
    cudaError_t err = cudaSuccess;

    int tile_dimx = 32;         // tile dimension in x = num of threads in x
    int tile_dimy = 32;         // tile dimension in y = num of threads in y
    int nblock_y = (fold.nchan + (tile_dimy - 1))/tile_dimy;
    int nblock_x  = (rebin[previous].pitch_dim + (tile_dimx - 1))/tile_dimx;
    dim3 grid(nblock_x, nblock_y);
    dim3 threads(tile_dimx, tile_dimy);
    iolog(1, "[%lf] Call to bin input data: threads = (%d,%d), blocks =(%d, %d)",
                                          get_current_time(), tile_dimx, tile_dimy,
                                                               nblock_x, nblock_y);
    iolog(1, "rebinInputData: previous binning: %d (%d) current binning: %d (%d)", previous,
                                       rebin[previous].rebin, current, rebin[current].rebin);
    //get the ratio between the first and current prebin value
    int prebin = rebin[current].rebin/rebin[previous].rebin;
    // we handle the prebinning. (we hope it not end inn a mess!)

    // binInputKernel is launched in a stream different from the default one.
    // The folding on the candidates belonging to the current prebin list,
    // has to wait for the end of this kernel. In corrispondence of this
    // kernel it is registered and event to signal its ending.
#ifdef TRANSPOSE_SHFL
    binInputKernel_shfl<<<grid,threads, 0, rebin[current].stream>>>
                                                              (rebin[current].d_out,
                                                                 rebin[previous].d_out,
                                                             rebin[previous].pitch_dim,
                                                           rebin[current].pitch_dim,
                                                                            prebin);
#else
    int shared_memory_size = tile_dimx * tile_dimy * sizeof(float);
    binInputKernel<<<grid,threads, shared_memory_size, rebin[current].stream>>>
                                                              (rebin[current].d_out,
                                                                 rebin[previous].d_out,
                                                                         fold.nchan,
                                                             rebin[previous].pitch_dim,
                                                           rebin[current].pitch_dim,
                                                                            prebin);
#endif

    if ((err = cudaGetLastError()) == cudaSuccess) {
        // register an event with the current stream
        if ((err = cudaEventRecord(rebin[current].event, rebin[current].stream)) == cudaSuccess) {
            return EXIT_SUCCESS;
        }
    }
    errlog("Failed to launch binInputKernel (error code %s)!\n", cudaGetErrorString(err));
    return EXIT_FAILURE;
}

/*
 * int foldInputData(int ndev, int ncand, int isubint, binning_t * rebin, int phase_bin)
 */
/**
 *
 * Call the GPU kernel doing the real data folding.
 *
 * \param ndev         the current device in use
 * \param ncand        the candidate index
 * \param isubint      the subint index
 * \param rebin        the current rebin structure
 * \param phase_bin    the number of phase bins of the candidate
 *
 * \return EXIT_SUCCESS on success, otherwise EXIT_FAILURE
 */
extern "C" int foldInputData(int ndev, int ncand, int isubint, binning_t rebin, int phase_bin)
{
    dim3 threadsPerBlock;       // number of kernel threads per block
    dim3 blocksPerGrid;         // number of kernel blocks for grid
    cudaStream_t *streams = Gpus[ndev].streams;
    cudaError_t err = cudaSuccess;

    //threads in x represents the steps in time so threadsPerBlock.x < nsamp_subslot
    threadsPerBlock.x = 32;
    if (threadsPerBlock.x > rebin.pitch_dim) {
        threadsPerBlock.x = rebin.pitch_dim;  // should not happen!!
    }
    //threads in y represents the steps in freqs inside each band. So
    //threadsPerBlock.y <= number of chans/band
    threadsPerBlock.y = 8;          // good value (32 x 8 = 256 threads/block)
    if (fold.nchan/fold.nsubbands < 8) {
        threadsPerBlock.y = fold.nchan/fold.nsubbands;
    }
    blocksPerGrid.x = fold.nsubbands;
    blocksPerGrid.y = 1;
    // calculate the max number of threads in x
    int max_threads_x = Gpus[ndev].max_thread_per_block/threadsPerBlock.y;
    // here x axis is along time, while y axis is along frequencies (we
    // use x and y in a different way from the non-folding code 
    if (threadsPerBlock.x > max_threads_x) {
        threadsPerBlock.x = max_threads_x;
    }
    // take into account that the number of nsamp in each integration
    // is not a multiple of the thread dimension in x
    if ((rebin.pitch_dim % threadsPerBlock.x) != 0) {
        if (threadsPerBlock.x * threadsPerBlock.y < Gpus[ndev].max_thread_per_block) {
            threadsPerBlock.x += 1;
        } else {
            // too much threads for SM: can't run kernel
            errlog("folding kernel: Invalid kernel parameters: threads for each SM: %d (max %d)",
                                                           threadsPerBlock.x * threadsPerBlock.y,
                                                                Gpus[ndev].max_thread_per_block);
            return EXIT_FAILURE;
        }
    }
    // get the number of warp
    int warp_size = Gpus[ndev].warp_size;
    int warp_count = threadsPerBlock.x * threadsPerBlock.y/warp_size;
    if (((threadsPerBlock.x*threadsPerBlock.y) % warp_size) != 0) {
        warp_count += 1;
    }
    int max_phase = fold.nbins; //initiliaze the max number of phase bins
    //adjust the max_phase value on the nbins of the current candidate
    if (phase_bin < 15) {
        max_phase = 16;
    } else if (phase_bin < 31) {
        max_phase = 32;
    } else if (phase_bin < 63) {
        max_phase = 64;
    }
    // configure shared memory size
    int shared_memory_size = max_phase * warp_count * sizeof(float) * 2;
    if (shared_memory_size > Gpus[ndev].sharedMemPerBlock) {
        errlog("Shared memory requested size is too big (requested: %d available: %d)",
                                              shared_memory_size,
                                              Gpus[ndev].sharedMemPerBlock);
        return EXIT_FAILURE;
    }
    int threadblock_memory = max_phase * warp_count;
    uint64_t nsamp_subslot = fold.nsamp/(fold.nsubints * fold.nchan * rebin.rebin);
    //the folding kernel is executed on a different stream for each
    //candidate.
    //This kernel starts only when the data rebinning of the input data
    //(with the rebin of the current candidate) has ended
    if (EnableSplit == MYTRUE) {
        folding_worker<<< blocksPerGrid, threadsPerBlock, shared_memory_size, streams[ncand] >>>
                                                        (rebin.d_out,
                                                         D_folded, d_weight,
                                                         rebin.pitch_dim,
                                                         (int)nsamp_subslot,
                                                         ncand, rebin.rebin,
                                                         max_phase,
                                                         warp_count,
                                                         fold.tobs, fold.nbins, isubint,
                                                         threadblock_memory);
    } else {
        folding_worker_nosplit<<< blocksPerGrid, threadsPerBlock, shared_memory_size, streams[ncand] >>>
                                                        (rebin.d_out,
                                                         D_folded, d_weight,
                                                         rebin.pitch_dim,
                                                         (int)nsamp_subslot,
                                                         ncand, rebin.rebin,
                                                         max_phase,
                                                         warp_count,
                                                         fold.tobs, fold.nbins, isubint,
                                                         threadblock_memory);
    }
    if ((err = cudaGetLastError()) == cudaSuccess) {
        return EXIT_SUCCESS;
    }
    errlog("Failed to launch folding_worker kernel (error code %s)!", cudaGetErrorString(err));
    return EXIT_FAILURE;
}

/*
 * int folding(int ndev, int isubint, unsigned char *d_in, candidate_t *Candidate)
 */
/**
 * For each pulsar candidate call the GPU kernels to execute the data folding:
 *  1) the transpose kernel to rotate the input matrix of data (to have time along fast axis)
 *  2) the rebin kernel to sum up the sample in time (as needed)
 *  3) the folding kernel that really folds the data
 * Calculate the statistics (mean and standard deviation) of the input data
 * taking into account the number of padded samples (if any).
 *
 * \param ndev          the device number (if more than 1 gpu present)
 * \param isubint       the sub-integration number
 * \param d_in          the device pointer to mapped memory
 * \param candidate     the array of candidate structures
 * \param rebin         the rebin structure
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise
 */

extern "C" int folding(int ndev, int isubint, unsigned char *d_in, candidate_t *candidate, binning_t *rebin)
{
    int err_cnt = 0;                    // error counter
    dim3 threadsPerBlock;               // number of kernel threads per block
    dim3 blocksPerGrid;                 // number of kernel blocks for grid
    cudaError_t err = cudaSuccess;      //cuda function return code
    cudaStream_t *streams = Gpus[ndev].streams;
    uint64_t nsamp = fold.nsamp;

    // WARNING we do not chect for odd numbers!
    if ((err = cudaSetDevice(ndev)) != cudaSuccess) {
        errlog("Failed to enter folding routine 0 (error code %s)!\n", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }

    // create cuda event handles
    cudaEvent_t start_kernel, stop_kernel;
    if ((err = cudaEventCreate(&start_kernel)) != cudaSuccess) {
        errlog("Failed to create event start_kernel (%s)", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    if ((err = cudaEventCreate(&stop_kernel)) != cudaSuccess) {
        errlog("Failed to create event stop_kernel (%s)", cudaGetErrorString(err));
        cudaEventDestroy(start_kernel);
        return EXIT_FAILURE;
    }

    cudaDeviceSynchronize();
    float gpu_time = 0.0f;      // gpu execution time

    int first_bin_idx = 0;      // index of the first valid rebinning element
    int first_bin_val = 1;      // first valid rebinning value
    // start kernel timer
    if ((err = cudaEventRecord(start_kernel, 0)) != cudaSuccess) {
        errlog("Failed to record event start_kernel");
        cudaEventDestroy(start_kernel);
        cudaEventDestroy(stop_kernel);
        return EXIT_FAILURE;
    }
    // set kernel launch configuration
    int tile_dimx = 32;         //number of threads in x
    int tile_dimy = 32;         //number of threads in y

    //ATT:
    //the transpose operation has to be done on the original input
    //matrix. It means that even if there is no candidate with rebin = 1,
    //we have to take into account the original matrix dimension.
    //The matrix has:
    //nrows = fold.nchan
    //ncols = rebin[0].pitch_dim (>= nsamp_subslot)
    //rebin[0].pitch_dim is the matrix row length returned by the call to
    //cudaMallocPitch()
    //
    int nblock_x = (fold.nchan + (tile_dimx - 1))/tile_dimx;
    int nblock_y  = (rebin[0].pitch_dim + (tile_dimy - 1))/tile_dimy;
    dim3 grid(nblock_x, nblock_y);
    dim3 threads(tile_dimx, tile_dimy);
    //get the size of the shared memory
    int shared_memory_size = tile_dimx * tile_dimy * sizeof(char);
    if (shared_memory_size > Gpus[ndev].sharedMemPerBlock) {
        errlog("Shared memory requested size is too big (requested: %d available: %d)",
                                     shared_memory_size, Gpus[ndev].sharedMemPerBlock);
        cudaEventDestroy(start_kernel);
        cudaEventDestroy(stop_kernel);
        return EXIT_FAILURE;
    }
    // get the first valid binning value
    // 02/2019: no need to get it. This value is stored into a global
    // variable so that it is possible to handle start rebinning indexes > 5
    first_bin_idx = First_rebin_idx;
    first_bin_val = rebin[first_bin_idx].rebin;
    uint64_t nsamp_subslot = nsamp/(fold.nsubints*fold.nchan);
    iolog(2, "folding: first bin index: %d  val: %d nsamp_subslot: %lld isubint: %d\n",
            First_rebin_idx, first_bin_val, nsamp_subslot, isubint);
    // we handle the prebinning. (we hope it not end inn a mess!)
    // trasposePreBin kernel executes the matrix data transpose. In the
    // first binning value is > 1, it also rebin the input data.
    // The kernel is executed in a separate stream from the default one.
    // Next binning operations on data have to wait for the end of this
    // kernel.
#ifdef TRANSPOSE_SHFL
    if (first_bin_val > 1) {
        iolog(1, "[%lf] Call to transpose_shfl in GPU: threads:(%d,%d) blocks:(%d,%d) rebin: %d ",
                                                                           get_current_time(),
                                                                         tile_dimx, tile_dimy,
                                                                           nblock_x, nblock_y,
                                                                               first_bin_val);
        transposePreBin_shfl<<<grid,threads, shared_memory_size, rebin[first_bin_idx].stream>>>
                                                                   (rebin[first_bin_idx].d_out,
                                                                                          d_in,
                                                                                    fold.nchan,
                                                                rebin[first_bin_idx].pitch_dim,
                                                                            (int)nsamp_subslot,
                                                                                first_bin_val);
    } else {
        iolog(1, "[%lf] Call to transpose in GPU: threads:(%d,%d), blocks:(%d,%d) rebin: %d ",
                                                                               get_current_time(),
                                                                             tile_dimx, tile_dimy,
                                                                               nblock_x, nblock_y,
                                                                                   first_bin_val);

        transposeNoBankConflicts<<<grid,threads, shared_memory_size, rebin[first_bin_idx].stream>>>
                                                                        (rebin[first_bin_idx].d_out,
                                                                                               d_in,
                                                                                         fold.nchan,
                                                                     rebin[first_bin_idx].pitch_dim,
                                                                                (int)nsamp_subslot);
    }
#else
   iolog(1, "[%lf] Call to transpose in GPU: threads:(%d,%d) blocks:(%d, %d) rebin: %d ",
                                                                          get_current_time(),
                                                                        tile_dimx, tile_dimy,
                                                                          nblock_x, nblock_y,
                                                                              first_bin_val);
    transposePreBin<<<grid,threads, shared_memory_size, rebin[first_bin_idx].stream>>>(rebin[first_bin_idx].d_out,
                                                                                                             d_in,
                                                                                                       fold.nchan,
                                                                                   rebin[first_bin_idx].pitch_dim,
                                                                                               (int)nsamp_subslot,
                                                                                                   first_bin_val);
#endif
    err = cudaGetLastError();
    if (err == cudaSuccess) {
        //Records an event on the first rebin stream. The event is recorded after all preceding operations in
        //stream have been completed;
        err = cudaEventRecord(rebin[first_bin_idx].event, rebin[first_bin_idx].stream);
    } else {
        errlog("Failed to launch input_converter kernel (error code %s)!\n",
                                                   cudaGetErrorString(err));
        cudaEventDestroy(start_kernel);
        cudaEventDestroy(stop_kernel);
        return EXIT_FAILURE;
    }
    iolog(1, "[%lf] Call to folding_worker:isubint: %d max phases: %d", get_current_time(), isubint, fold.nbins);
    //execute folding
    int ncand = 0;                                      //current candidate to process
    //int valid_nbinning = first_valid_binidx(rebin);     //find the first prebin value
    // loop on all valid binning values.
    // For all the rebinning values but the first, we need to rebin the input matrix
    // (for the first rebin index the input data rebinning has already be done by the
    // transposePreBin kernel).
    //
    // OSS The first index can correspond to a non  valid rebinning value (one
    // with no available candidate) when all the candidates have rebinning
    // > 5. We need an intermediate step to perform the corner turner!
    // OSS: first we load all the rebin kernels and then folding one
    int valid_nbinning = First_rebin_idx;
    for (int nbinning = valid_nbinning; nbinning < MAX_NREBIN ; nbinning ++) {
        if (is_binentry_valid(nbinning, rebin) == MYFALSE) {
            // check if some higher binning values has no valid entry (i.e. candidates available)
            continue;
        }
        // for all the rebinning values but the first, we need to rebin the input matrix
        if (nbinning > valid_nbinning) {
            cudaStreamWaitEvent(rebin[nbinning].stream, rebin[valid_nbinning].event, 0);
            if (rebinInputData(valid_nbinning, nbinning, rebin) == EXIT_FAILURE) {
                cudaEventDestroy(start_kernel);
                cudaEventDestroy(stop_kernel);
                return EXIT_FAILURE;
            }
            valid_nbinning = nbinning;
        }
    }
#ifdef NO
    valid_nbinning = first_valid_binidx(rebin);     //find the first prebin value
    for (int nbinning = valid_nbinning; nbinning < MAX_NREBIN ; nbinning ++) {
        if (is_binentry_valid(nbinning, rebin) == MYFALSE) {
            // check if some higher binning values has no valid entry (i.e. candidates available)
            continue;
        }
        err_cnt = 0;        //errors counter
        //loop on the candidates with the same prebinning value
        for (ncand = rebin[nbinning].first; ncand <= rebin[nbinning].last; ncand ++) {
            iolog(2, "candidate %d for rebin val: %d nbins[%d]: %d ", ncand, rebin[nbinning].rebin, ncand,
                                                                              candidate[ncand].phase_bin);
            // for all the rebinning we need to wait the end of input data binning
            err = cudaStreamWaitEvent(streams[ncand], rebin[nbinning].event, 0);
            iolog(2, "folding kernel for candidate %d rebin: %d ", ncand, rebin[nbinning].rebin);
            // call kernel to do folding
            if (foldInputData(ndev, ncand, isubint, rebin[nbinning], candidate[ncand].phase_bin) == EXIT_FAILURE) {
                err_cnt ++;
                break;
            }
        }       // end of loop on candidate
    }           // end on rebinning
#endif
    err_cnt = 0;        //errors counter
    for (ncand = 0; ncand < fold.ncand; ncand ++) {
        int nbinning = get_binidx_candidate(ncand, rebin);
        if (nbinning > -1) {
            iolog(2, "candidate %d for rebin val: %d nbins[%d]: %d ", ncand, rebin[nbinning].rebin, ncand,
                                                                              candidate[ncand].phase_bin);
            // for all the rebinning we need to wait the end of input data binning
            err = cudaStreamWaitEvent(streams[ncand], rebin[nbinning].event, 0);
            iolog(2, "folding kernel for candidate %d rebin: %d ", ncand, rebin[nbinning].rebin);
            // call kernel to do folding
            if (foldInputData(ndev, ncand, isubint, rebin[nbinning], candidate[ncand].phase_bin) == EXIT_FAILURE) {
                err_cnt ++;
                break;
            }
        }
    }       // end of loop on candidate (prebinning)
    if (err_cnt == 0) {
        if ((err = cudaEventRecord(stop_kernel, 0)) == cudaSuccess) {
            if ((err = cudaEventSynchronize(stop_kernel)) == cudaSuccess) {
                cudaEventElapsedTime(&gpu_time, start_kernel, stop_kernel);
                Gpu_time += gpu_time;
                iolog(1, "[%lf] folding_worker takes %lf msec", get_current_time(), gpu_time);
            } else {
                errlog(" cudaEventSynchronize returned %d", err);
            }
            // for test purpose copy device memory to host memory
            copy_to_host(rebin);
        } else {
            errlog(" cudaEventRecord returned %d", err);
        }
    }
    // calculate the sum of the sub-integrations means
    float mean0 = 0;
    float sigma0= 0;
    if ((err = cudaEventRecord(start_kernel, 0)) != cudaSuccess) {
        errlog("Failed to record event start_kernel");
        cudaEventDestroy(start_kernel);
        cudaEventDestroy(stop_kernel);
        return EXIT_FAILURE;
    }
    //OSS: sigma0 and maen0 are the statistics of the matrix padded with 0,
    //so these values are to be fixed
    statistics_float(rebin[first_bin_idx].d_out, fold.nchan * rebin[first_bin_idx].pitch_dim, &mean0, &sigma0);
    // The allocated d_out array is memory aligned and it's padded with 0. We need to rescale
    // the mean value to take account of the difference between the real
    // number of data per subslot (nsamp_subslot/prebin) and the padded one (pitch_dim)
    float meanv = mean0 *  rebin[first_bin_idx].pitch_dim * rebin[first_bin_idx].rebin/nsamp_subslot;
    mean  += meanv;
    // true dimension of the data for statistix:  nsamp_subslot/rebin[first_bin_idx].rebin;
    // dimension used to calculate statistix: rebin[first_bin_idx].pitch_dim;
    float frac =
        ((float)rebin[first_bin_idx].pitch_dim*rebin[first_bin_idx].rebin)/nsamp_subslot;
    // calculate the variance of the true data
    float variance_r = frac * sigma0 * sigma0 - mean0*mean0 *(frac -1) -(meanv - mean0)*(meanv-mean0);
    iolog(1, "mean: %f sigma: %f meanv: %f variance_r: %f", mean0,sigma0, meanv, variance_r);
    sigma += variance_r;
    // stop the kernel timer
    // !! here all stream are synchronized!!
    if ((err = cudaEventRecord(stop_kernel, 0)) != cudaSuccess) {
        errlog("Failed to record event stop_kernel (%s)", cudaGetErrorString(err));
    }
    if ((err = cudaEventSynchronize(stop_kernel)) != cudaSuccess) {
        errlog("Failed to synchro stop_kernel (%s)", cudaGetErrorString(err));
    }
    cudaEventElapsedTime(&gpu_time, start_kernel, stop_kernel);
    // end of folding
    Gpu_time += gpu_time;
    Gpu_fold = Gpu_time;
    iolog(1, "[%lf] GPU matrix statistics takes %lf msec", get_current_time(), gpu_time);
    iolog(2, "mean0: %lf sigma0: %lf\n", mean0, sigma0);
    // release resources
    if ((err = cudaEventDestroy(start_kernel)) == cudaSuccess) {
        if ((err = cudaEventDestroy(stop_kernel)) == cudaSuccess) {
            return EXIT_SUCCESS;
        }
    }
    errlog("(1) Failed with message: %s(%d)", cudaGetErrorString(err), err);
    return EXIT_FAILURE;
}

/*
 * int profiles(int ndev)
 */
/**
 *
 * Execute normalization of the folded data and build the reduced profile.
 *
 * \param ndev  the gpu id (0, 1,..ngpu)
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise
 *
 */
extern "C" int profiles(int ndev)
{
    dim3 threadsPerBlock;       //number of threads for each block
    dim3 blocksPerGrid;         // number of blocks for each grid
    cudaStream_t *streams = Gpus[ndev].streams;
    cudaError_t err = cudaSuccess;
    cudaEvent_t start_kernel, stop_kernel;
    cudaEvent_t stop;

    if ((err = cudaSetDevice(ndev)) != cudaSuccess) {
        errlog("profiles: Failed to enter folding routine 0 (error code %s)!\n",
                cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    if ((err = cudaEventCreate(&start_kernel)) != cudaSuccess) {
        errlog("profiles: Failed to create event start_kernel (%s)",
                cudaGetErrorString(err));
        return EXIT_FAILURE;
    }

    //waits until all preceding commands in all streams of all host threads
    //have completed
    cudaDeviceSynchronize();
    float gpu_time = 0.0f;

    // launch kernel for normalization and profiles
    int shared_memsize = fold.nbins * sizeof(float);
    iolog(1, "[%lf] Calling kernel to build profiles: threads= (%d, %d) blocks= (%d, %d)", get_current_time(),
                                                                         threadsPerBlock.x, threadsPerBlock.y,
                                                                            blocksPerGrid.x, blocksPerGrid.y);
    if ((err = cudaEventCreate(&start_kernel)) == cudaSuccess) {
        if ((err = cudaEventRecord(start_kernel, 0)) != cudaSuccess) {
            errlog("profiles: Failed to record event start_kernel");
            return EXIT_FAILURE;
        }
    }
    //iolog(1, "raw time series mean: %f\n", mean/fold.nsubints);
    iolog(1, "raw time series mean: %f\n", mean);
    //loop on candidates: each candidate handled by a different CUDA stream
    for (int ncand = 0; ncand < fold.ncand; ncand ++) {
        // launch normalization kernel
        threadsPerBlock.x = fold.nbins;      // one thread for each bin (phase)
        threadsPerBlock.y = 1;
        blocksPerGrid.x = fold.nsubints;    // one block for each sub-integration
        blocksPerGrid.y = 1;    // one block for each sub-integration
        normalize_kernel<<<blocksPerGrid, threadsPerBlock, 0, streams[ncand] >>>(D_folded,
                                                                          d_weight, ncand,
                                                                       mean/fold.nsubints,
                                                                          fold.nsubbands);
        //check for kernel errors
        err = cudaGetLastError();
        if (err != cudaSuccess) {
            errlog("profiles: Failed to launch normalize_kernel (error code %s)!",
                                                           cudaGetErrorString(err));
            //destroy events
            cudaEventDestroy(start_kernel);
            return EXIT_FAILURE;
        }
        //launch kernel for profile
        threadsPerBlock.x = fold.nbins;      // one thread for each bin (phase)
        threadsPerBlock.y = 1;
        blocksPerGrid.x = fold.nsubints;    // one block for each sub-integration
        blocksPerGrid.y = 1;    // one block for each sub-integration
        profile_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize,
                          streams[ncand] >>>(D_folded, D_outfprof, ncand,
                                                         fold.nsubbands);
        if (err != cudaSuccess) {
            errlog("profiles: (1) Failed to launch profile_kernel (error code %s)!",
                    cudaGetErrorString(err));
            //destroy events
            cudaEventDestroy(start_kernel);
            return EXIT_FAILURE;
        }
        if (EnableWrite == MYTRUE) {
            uint64_t mem_size = fold.ncand * fold.nsubints * fold.nbins * sizeof(float);
            cudaMemcpyAsync(H_outfprof, D_outfprof, mem_size, cudaMemcpyDeviceToHost,0);
            cudaEventCreate(&stop);
            cudaEventRecord(stop, 0);

            // have CPU do some work while waiting for stage 1 to finish
            unsigned long int counter=0;

            while (cudaEventQuery(stop) == cudaErrorNotReady)
            {
                counter++;
            }
            cudaEventDestroy(stop);
        }
        //reduce kernel for profile
        threadsPerBlock.x = fold.nbins;
        threadsPerBlock.y = 1;
        blocksPerGrid.x = 1;
        blocksPerGrid.y = 1;
        profile_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize,
                          streams[ncand] >>>(D_outfprof, D_outprof, ncand,
                          fold.nsubints);
        if (err != cudaSuccess) {
            errlog("profiles: (2) Failed to launch profile_kernel (error code %s)!\n",
                                                             cudaGetErrorString(err));
            //destroy events
            cudaEventDestroy(start_kernel);
            return EXIT_FAILURE;
        }
    }
    if ((err = cudaEventCreate(&stop_kernel)) != cudaSuccess) {
        errlog("profiles: Failed to create event stop_kernel (%s)", cudaGetErrorString(err));
    }
    if ((err = cudaEventRecord(stop_kernel, 0)) != cudaSuccess) {
        errlog("profiles: Failed to record event stop_kernel (%s)", cudaGetErrorString(err));
    }
    if ((err = cudaEventSynchronize(stop_kernel)) != cudaSuccess) {
        errlog("profiles: Failed to synchro stop_kernel (%s)", cudaGetErrorString(err));
    }
    //get elapsed gup time for normalization + profile
    cudaEventElapsedTime(&gpu_time, start_kernel, stop_kernel);
    iolog(1, "[%lf] normalization + profile takes %f ms", get_current_time(), gpu_time);
    Gpu_time += gpu_time;

    //for test purpose we copy back to host the intermediate values
    if (EnableWrite == MYTRUE) {
        uint64_t mem_size = fold.ncand * fold.nbins * sizeof(float);
        cudaMemcpyAsync(H_outprof, D_outprof, mem_size, cudaMemcpyDeviceToHost,0);
        cudaEventCreate(&stop);
        cudaEventRecord(stop, 0);

        // have CPU do some work while waiting for stage 1 to finish
        unsigned long int counter=0;

        while (cudaEventQuery(stop) == cudaErrorNotReady)
        {
            counter++;
        }
        cudaEventDestroy(stop);
    }
    //release the allocated resources
    if (cudaEventDestroy(start_kernel) == cudaSuccess) {
        if (cudaEventDestroy(stop_kernel) == cudaSuccess) {
            return EXIT_SUCCESS;
        }
    }
    //check for errors
    err = cudaGetLastError();
    errlog("profiles: Failed with error %s", cudaGetErrorString(err));
    return EXIT_FAILURE;
}

/*
 *
 * void phase_shift_calc(int index, int ncand, float* phase_shift, double *nu, float delta_dm, float delta_nu,
 *                     double delta_nudot, double *time_sum, double  *freq_sum)
 */
/**
 *
 * For each candidate and for each perturbation of the pulsar parameters,
 * the function calculates the shift in phase and store it in an array.
 * The array elements are the copied into global memory and passed as
 * argument to the kernel that perturbates the profiles.
 *
 * \param index         the global index to address the list of three * parameters  (dm, nu, nudot)
 * \param ncand         the number of the pulsar candidate
 * \param phase_shift   the arry to store the shifts
 * \param nu            the array with freq. values of the candidates
 * \param delta_dm      the perturbation on DM
 * \param delta_nu      the perturbation on freq.
 * \param delta_nudot   the perturbation on nudot
 * \param time_sum      the array with sub-integration central time
 * \param freqe_sum     the array with sub-band cenetral freq.
 */
void phase_shift_calc(int index, int ncand, double* phase_shift, double *period, float delta_dm, float delta_nu,
                      double delta_nudot, double *time_sum, double *freq_sum)
{
    double phshift = 0.;

    for (int iband = 0; iband < fold.nsubbands; iband ++) {
            double time_shift = delta_dm * (1./(freq_sum[0] * freq_sum[0]) - 1./(freq_sum[iband] * freq_sum[iband])) *
                       1./period[ncand];
        for (int isub = 0; isub <fold.nsubints; isub ++) {
            phshift = time_shift - (delta_nu * time_sum[isub]  + time_sum[isub] * time_sum[isub] * delta_nudot * 0.5);
            if (phshift < 0) {
                phshift += (-1.*floor(phshift));
            }
            phase_shift[iband + isub * fold.nsubbands + ncand * fold.nsubbands * fold.nsubints] = phshift;
        }
    }
}

/*
 * int perturbed_profiles(int ndev, int verbose, candidate_t *candidates,
 *                        double *nu, double *nudot, binning_t *rebin,
 *                        double *time_sum, double *freq_sum,float period_range,
 *                        float period_step, float pdot_range, float pdot_step, float dm_range,
 *                        float dm_step)
 */
/**
 *
 * Build the profile of all candidates with perturbations delta_period,
 * delta_dm and delta_pdot.
 *
 * \param ndev          the gpu id (0, 1,..ngou)
 * \paran verbose       enable/disable optimization values printing
 * \param opt_dim       the optimization space dimension (2D/3D)
 * \param candidates    the array of candidates structure
 * \param nu            the array with the freq of the candidates
 * \param nudot         the array with the freq drift of the candidates
 * \param rebin         the array with the rebin structures
 * \param n_p           number of steps for period optimization
 * \param n_pdot        number of steps in pdot optimization
 * \param n_dm          number of steps in dm optimization
 * \param rf_p          reduce factor for the width of period step
 * \param rf_pdot       reduce factor for the width of the pdot step
 * \param rf_dm         reduce factor for the width of the dm step
 * \param res_filename  name of the file with results of optimization
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise
 *
 */
extern "C" int perturbed_profiles(int ndev, int verbose, int opt_dim, candidate_t * candidates,
                                  binning_t * rebin, double *time_sum, double *freq_sum,
                                  int n_p, int rf_p, int n_pdot, int rf_pdot, int n_dm, int rf_dm,
                                  char * res_filename)
{
    // int o int64 ??
    int ip, ipdot, idm;
    int nperiod = 0, npdot = 0, ndm = 0;
    int shared_memsize          = 0;    //dimension in bytes GPU shared memory dinamically allocated
    float *trial_param          = 0;    //array to store all the trial values
    float trial_period          = 0., trial_dm = 0.;
    float delta_dm              = 0.;
    float delta_nu              = 0.;
    double trial_pdot           = 0.;
    double delta_nudot          = 0.;
    float period_range          = 0.000202e-3; // period range and step for optimization phase
    float period_step           = 0.00002e-3;
    float pdot_range            = 0.0202e-6;   // pdot range and step for optimization phase
    float pdot_step             = 0.02e-7;
    float dm_range              = 1.01;        // dm range and step for optimization phase
    float dm_step               = 0.10;
    dim3 threadsPerBlock;                       //number of threads for each block
    dim3 blocksPerGrid;                         // number of blocks for each grid
    cudaStream_t *streams = Gpus[ndev].streams;
    cudaError_t err             = cudaSuccess;  //cuda functions return code
    cudaEvent_t start_kernel, stop_kernel;      //events to register kernels  execution time
    cudaEvent_t stop;                           // event to wait device->host memory copy

    nperiod = n_p * 2 - 1;                      //number of period trials
    npdot   = n_pdot * 2 - 1;                   //number of pdot trials
    ndm     = n_dm * 2 - 1;                     //number of dm trials
    int nelem = nperiod * npdot * ndm;          //total numer of trials

    //allocate a vector to store the phase shift for each candidate and trial
    int trial_size = fold.ncand * fold.nsubbands * fold.nsubints;
    double phase_shift[trial_size];

    trial_size *= sizeof(double);
    memset(phase_shift, 0, trial_size);

    //Note: the size of the array to allocate is = (nelem * 3) : for each
    //trial we store the trial  period, dm and pdot.
    trial_param = (float*)calloc(3 * nelem * fold.ncand, sizeof(float));

    //allocate memory to store results
    if (allocate_mem4best(nelem) != EXIT_SUCCESS) {
        errlog("allocate_mem4best: error in allocating memory (%s)", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }

    if (verbose) {
        printf("\nOptimizing.....             with variable steps \n");
    }
    iolog(1, "nperiod: %d npdot: %d ndm: %d fold.tobs: %f\n", nperiod,npdot, ndm,fold.tobs);
    if ((err = cudaSetDevice(ndev)) != cudaSuccess) {
        errlog("pert_prof: Failed to enter folding routine 0 (error code %s)!\n",
                cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    if ((err = cudaEventCreate(&start_kernel)) != cudaSuccess) {
        errlog("pert_prof: Failed to create event start_kernel (%s)",
                cudaGetErrorString(err));
        return EXIT_FAILURE;
    }

    cudaDeviceSynchronize();
    d_adjusted = d_weight ;             // we reuse d_weight area
    float gpu_time = 0.0f;              // null gpu execution time

    if ((err = cudaEventCreate(&start_kernel)) == cudaSuccess) {
        if ((err = cudaEventRecord(start_kernel, 0)) != cudaSuccess) {
            errlog("pert_prof: Failed to record event perturbate_kernel");
            return EXIT_FAILURE;
        }
    }
    int index = 0;                      //trial index

    for (ip = 0; ip < nperiod; ip++) {
        printf(" iteration %d/%d  \r",ip, nperiod);
        for (ipdot = 0; ipdot < npdot; ipdot++) {
            for (idm = 0; idm < ndm; idm++) {
                //loop on candidates: each candidate handled by a different CUDA stream
                for (int ncand = 0; ncand < fold.ncand; ncand ++) {
                    //float deltaperiod = period[ncand]/fold.tobs/5000.;
                    float deltaperiod = candidates[ncand].period/fold.tobs/5000.;
                    deltaperiod = candidates[ncand].period/fold.tobs/500.;
                    //iolog(0, "pert_prof: Variable step on Period. Period %.8g step %.8g #ratio %.8g",
                    //    candidates[ncand].period, deltaperiod, candidates[ncand].period/deltaperiod);
                    iolog(3, "pert_prof: Variable step on Period. Candidate #%d step %.8g grid# %d",
                                                                           ncand, deltaperiod, n_p);
                    period_step = deltaperiod;
                    if (rf_p > 1) {
                        period_step /= rf_p;
                    }
                    // to mantain the same number of optimization points, we update the period_range
                    // (not used hereafter)
                    period_range = n_p * period_step;
                    iolog(3, "period_range: %g period_step: %g\n", period_range, period_step);

                    //float deltapdot = period[ncand]/fold.tobs/fold.tobs/1000*2;
                    float deltapdot = candidates[ncand].period/fold.tobs/fold.tobs/1000.*2.;
                    iolog(3, "pert_prof: Variable step on pdot Candidate #%d step %.8g grid# %d",
                                                                               ncand, deltapdot, n_p);
                    pdot_step = deltapdot;
                    if (rf_pdot > 1) {
                        pdot_step /= rf_pdot;
                    }
                    // to mantain the same number of optimization points, we update the pdot_range
                    // (not used hereafter)
                    pdot_range = n_pdot * pdot_step;
                    iolog(3, "pdot_range: %g pdot_step: %g\n", pdot_range, pdot_step);

                    //float deltadm = period[ncand] * 90;
                    //float deltadm = candidates[ncand].period * 90.;
                    //deltadm = candidates[ncand].dm / 20.;
                    //deltadm = 80. * candidates[ncand].period ;
                    float deltadm = 80. * candidates[ncand].period * (candidates[ncand].period/1000.) ;
                    deltadm = ((deltadm > .5) ? .5: deltadm);
                    deltadm = ((deltadm < .001) ? .001: deltadm);
                    iolog(3, "pert_prof: Variable step on dm Candidate #%d step %.8g grid# %d",
                                                                               ncand, deltadm, n_p);
                    dm_step = deltadm;
                    // to mantain the same number of optimization points, we update the dm_range
                    // (not used hereafter)
                    dm_range = n_dm * dm_step;
                    if (rf_dm > 1) {
                        dm_step /= rf_dm;
                    }
                    iolog(3, "dm_range: %g dm_step: %g\n", dm_range, dm_step);

                    // the index into the p/dm and p/pdot planes. note that we use result->nperiod not n_p
                    // because of the factor of 2 above.
                    trial_period = (ip - n_p + 1) * period_step + Opt_period[ncand];
                    trial_pdot = (ipdot - n_pdot + 1) * pdot_step + Opt_pdot[ncand];
                    trial_dm = (idm - n_dm + 1) * dm_step + Opt_dm[ncand];
                    delta_dm = (candidates[ncand].dm - trial_dm) * K_DM;
                    //delta_nu = (1.0/trial_period) - 1./period[ncand];
                    delta_nu = (1.0/trial_period) - 1./candidates[ncand].period;
                    //delta_nudot = -trial_pdot /(trial_period * trial_period) -
                    //                         (-pdot[ncand]/(period[ncand]*period[ncand]));
                    delta_nudot = -trial_pdot /(trial_period * trial_period) -
                                 (-candidates[ncand].pdot/(candidates[ncand].period*candidates[ncand].period));
                    // phase shift calculate in CPU and stored into an
                    // array copied in GPU
                    phase_shift_calc(index, ncand, phase_shift, Opt_period, delta_dm, delta_nu, delta_nudot,
                                                                       time_sum, freq_sum);
                    int shift = ncand * fold.nsubbands * fold.nsubints;
                    if (cudaMemcpyAsync(d_phase_shift + shift, &phase_shift[shift], fold.nsubbands *
                                                                       fold.nsubints * sizeof(double),
                                            cudaMemcpyHostToDevice, streams[ncand]) != cudaSuccess) {
                        errlog("pert_prof: Failed to copy phase_shift array on device!");
                        cudaEventDestroy(start_kernel);
                        return EXIT_FAILURE;
                    }

                    //configure the perturbed kernel parameters
                    threadsPerBlock.x = 32;     // threadIdx.x -> bin phase
                    threadsPerBlock.y = 4;      // threadIdx.y -> subband
                    blocksPerGrid.x   = fold.nsubbands/threadsPerBlock.y;
                    blocksPerGrid.y   = fold.nsubints;
                    shared_memsize    = fold.nbins * sizeof(float) * threadsPerBlock.y;

                    // here we rotate the profiles to account for the small change in p/pdot/dm.
                    // we launch the perturbation kernel
                    perturbate_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize, streams[ncand] >>>
                                                                                                 (D_folded,
                                                                                                d_adjusted,
                                                                                             d_phase_shift,
                                                                                                     ncand,
                                                                                                fold.nbins,
                                                                                                    index);
                    if (err != cudaSuccess) {
                        errlog("pert_prof: (1) Failed to launch profile_kernel (error code %s)!",
                                                                       cudaGetErrorString(err));
                        //destroy events
                        cudaEventDestroy(start_kernel);
                        return EXIT_FAILURE;
                    }
                     //reduce kernel for profile
                     threadsPerBlock.x = fold.nbins;
                     threadsPerBlock.y = 1;
                     blocksPerGrid.x   = fold.nsubints;
                     blocksPerGrid.y   = 1;
                     profile_kernel<<< blocksPerGrid, threadsPerBlock, 0, streams[ncand] >>>
                                            (d_adjusted, D_outfprof, ncand, fold.nsubbands);
                     if (err != cudaSuccess) {
                         errlog("pert_prof: (1) Failed to launch profile_kernel (error code %s)!",
                                                                         cudaGetErrorString(err));
                         //destroy events
                         cudaEventDestroy(start_kernel);
                         return EXIT_FAILURE;
                     }
                     threadsPerBlock.x = fold.nbins;
                     threadsPerBlock.y = 1;
                     blocksPerGrid.x = 1;       // not better if fold.nsubints;
                     blocksPerGrid.y = 1;
                     best_profile_kernel<<< blocksPerGrid, threadsPerBlock, 0, streams[ncand] >>>(D_outfprof,
                                                                       D_bestprof,
                                                                       ncand,
                                                                       fold.nsubints,
                                                                       index,
                                                                       nelem);
                     if (err != cudaSuccess) {
                         errlog("pert_prof: (2) Failed to launch profile_kernel (error code %s)!\n",
                                                                           cudaGetErrorString(err));
                         //destroy events
                         cudaEventDestroy(start_kernel);
                         return EXIT_FAILURE;
                     }

                    //store the trial values into the allocated array
                    //
                    // arrangment of data stored into trial_param array
                    // --------------------------------------------------------------------------------
                    // |period0| pdot0| dm0 | period0 | pdot0 | dm1 | .........|periodN | pdotM | dmR |
                    // -------------------------------------------------------------------------------
                    //      index 0         |    index 1            | .........|     index N*M*R      |
                    //
                    //
                     trial_param[index * 3 + ncand * nelem * 3] = trial_period;
                     trial_param[index * 3 + ncand * nelem * 3 + 1] = trial_pdot;
                     trial_param[index * 3 + ncand * nelem * 3 + 2] = trial_dm;
                     iolog(2, "I: %d ip: %d ipdot: %d idm: %d trial_period: %g trial_pdot: %g trial_dm: %f\n",
                             index, ip, ipdot, idm, trial_period, trial_pdot, trial_dm);
                }    //end loop on candidates
                //!!synchronize kernels!! */
                cudaDeviceSynchronize();
                index ++;
            }
        }
        printf("                      \r");
    }

    // Note: if the input data used to calculate the statistics have been
    // rebinned, we take into account the square root of the first rebin value.
    // !! use local variable _sigma for recursive call of the current function
    double _sigma = sqrt(sigma / fold.nsubints);        //standard deviation of the population of raw time series
    int first_prebin_idx = First_rebin_idx;             // find the first valid prebin index
    int first_prebin = rebin[first_prebin_idx].rebin;   // find the first valide prebin value

    iolog(1, "perturbed_profile sigma: %lf first prebin: %d nsamp: %lld\n", sigma, first_prebin, fold.nsamp);
    for (int ncand = 0; ncand < fold.ncand; ncand ++) {
        // configure compute_sn kernel parameters
        //int max_threads = Gpus[ndev].max_thread_per_block;
        int nblock_y = 1;
        int nthread_x = 32;
        if (nelem > nthread_x) {
            nblock_y = nelem/nthread_x + 1;
        }
        threadsPerBlock.x = nthread_x;
        threadsPerBlock.y = 1;
        blocksPerGrid.x = nblock_y;
        //nelem * 2 -> for each trial, we store the S/N of the profile and width value
        shared_memsize = nthread_x * 2 * sizeof(float);
        iolog(3, "compute_sn kernel: nelem: %d thx: %d thy: %d blockx: %d blocky: %d\n",
                nelem, threadsPerBlock.x, threadsPerBlock.y, blocksPerGrid.x, blocksPerGrid.y);
        // to get the correct sigma value, we need to scale the calculated
        // sigma with the first prebin value. The first prebin value defines the input
        // data array dimension on which the statistics is evaluated
        compute_sn<<<blocksPerGrid, threadsPerBlock, shared_memsize, streams[ncand] >>>(D_bestprof,
                                      D_best,
                                      fold.nbins,
                                      ncand,
                                      nelem,
                                      _sigma*sqrt((float) first_prebin / fold.nsamp));
        // Warning!!! Last value should read sigma*sqrt((float)  (nbins[ncand] * first_prebin))
        // but here we do not have nbins[], so we make computation inside  compute_sn kernel
    }

    if ((err = cudaEventCreate(&stop_kernel)) != cudaSuccess) {
        errlog("pert_prof: Failed to create event stop_kernel (%s)", cudaGetErrorString(err));
    }
    if ((err = cudaEventRecord(stop_kernel, 0)) != cudaSuccess) {
        errlog("pert_prof: Failed to record event stop_kernel (%s)", cudaGetErrorString(err));
    }
    if ((err = cudaEventSynchronize(stop_kernel)) != cudaSuccess) {
        errlog("pert_prof: Failed to synchro stop_kernel (%s)", cudaGetErrorString(err));
    }
    //get elapsed gpu time for optimization + normalization + profile
    cudaEventElapsedTime(&gpu_time, start_kernel, stop_kernel);
    iolog(1, "[%lf] pert_prof: normalization + profile time: %f (ms)", get_current_time(), gpu_time);
    Gpu_time += gpu_time;

    //for test purpose we copy back to host the intermediate values
    uint64_t mem_size = nelem * 2 * fold.ncand * sizeof(float);
    cudaMemcpyAsync(Host_best, D_best, mem_size, cudaMemcpyDeviceToHost,0);

//
// arrangment of data stored in D_best (GPU mem) and Host_best (RAM)
//
// -------------------------------------------------------------------|
// sn_0 |sn_1 | sn_2| .......|sn_Ntrial| w_0|w_1| w_2|......|w_Ntrial |
// ____________________________________|______________________________|
//  <---- N trial elements          -->|<------ N trial elements    ->|

// D_best and Host_best have 2 * N_trial elements
// D_best is passed to the thrust routine to find the S/N max value and its corresponding
// array index. The positionof the max S/N corresponds to the index of the 3-tuple stored into trial_param array.
//

    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);

    // have CPU do some work while waiting for stage 1 to finish
    unsigned long int counter=0;

    while (cudaEventQuery(stop) == cudaErrorNotReady) {
        counter++;
    }
    cudaEventDestroy(stop);
    //release the allocated resources
    if (cudaEventDestroy(start_kernel) == cudaSuccess) {
        cudaEventDestroy(stop_kernel);
    }
    //check for errors
    if ((err = cudaGetLastError()) !=  cudaSuccess) {
        errlog("pert_prof: Failed with error %s", cudaGetErrorString(err));
        return EXIT_FAILURE;
    }
    // allocate memory to store results (sn_max and its index) for alla candidates
    float *sn_max = (float *) calloc(fold.ncand, sizeof(float));
    int *index_max = (int *) calloc(fold.ncand, sizeof(int));
    if (sn_max && index_max) {
        // find the max sn for each candidate
        for (int ncand = 0; ncand < fold.ncand; ncand ++) {
            // start is the index of the data relative to the ncand candidate
            // find max returs the position of the max S/N value relative to start
            int start = ncand * nelem  * 2 ;
            find_max(D_best, start, nelem, &sn_max[ncand], &index_max[ncand]);
        }
        FILE *res = 0;          /* pointer to result file */
        if (res_filename) {
            /* we store results in a machine readable form */
            res = fopen(res_filename, "w");
        } else {
            res = fopen("result.txt", "w");
        }
        //check for errors
        if (res == NULL) {
            errlog("pert_prof: Failed to open res file ");
            return EXIT_FAILURE;
        }
        if (verbose) {
            printf( "------------------------------------------------------------------\n");
            printf( "               %dD Optimized values (result files in /var/tmp)\n", opt_dim);
            printf( "------------------------------------------------------------------\n");
        }
        for (int ncand = 0; ncand < fold.ncand; ncand ++) {
            iolog(2, "index_max[%d]: %d\n", ncand, index_max[ncand]);
            // to get the value of S/N and width it's necessary to get the position from the start
            // of Host_best array. index_max[ncand]/ is the relative position (for the candidate)
            int position =  index_max[ncand] + ncand * nelem * 2;
            int idx = index_max[ncand]  * 3 + ncand * nelem * 3;
            Opt_period[ncand] = trial_param[idx];
            Opt_pdot[ncand] = trial_param[idx + 1];
            Opt_dm[ncand] = trial_param[idx + 2];
            if (verbose) {
                printf( "Candidate: %4d (%4d): index: %4d    S/N: %6.2f width: %f period: %12.9f  pdot: %g  dm: %10.7f\n",
                                                                                                ncand,
                                                                              candidates[ncand].index,
                                                                index_max[ncand], Host_best[position],
                                                                          Host_best[position + nelem],
                                                                                    Opt_period[ncand],
                                                                                      Opt_pdot[ncand],
                                                                                       Opt_dm[ncand]);
            }
            // we store results in a machine readable form. We include
            //  also origina (pre-optimization) values
            if (res) {
                fprintf(res, " %4d  %4d  %6.2f %f %12.9f %g %10.7f  %12.9f %g %10.7f \n", ncand,
                                                  candidates[ncand].index, Host_best[position],
                                                                   Host_best[position + nelem],
                                                                             Opt_period[ncand],
                                                                               Opt_pdot[ncand],
                                                                                 Opt_dm[ncand],
                                                                      candidates[ncand].period,
                                                                        candidates[ncand].pdot,
                                                                          candidates[ncand].dm);
            }
            //delta_dm = (dm[ncand] - Opt_dm[ncand]);
            delta_dm = (candidates[ncand].dm - Opt_dm[ncand]);
            delta_dm *= K_DM;
            delta_nu = (1.0/Opt_period[ncand]) - 1./candidates[ncand].period;
            delta_nudot = -Opt_pdot[ncand] /(Opt_period[ncand] * Opt_period[ncand]) -
                          (-candidates[ncand].pdot/(candidates[ncand].period*candidates[ncand].period));


            phase_shift_calc(0, ncand, phase_shift, Opt_period, delta_dm, delta_nu,
                                         delta_nudot, time_sum, freq_sum);
            int shift = ncand * fold.nsubbands * fold.nsubints;
            if (cudaMemcpyAsync(d_phase_shift + shift, &phase_shift[shift],
                                fold.nsubbands * fold.nsubints * sizeof(double),
                                cudaMemcpyHostToDevice, streams[ncand]) != cudaSuccess) {
                errlog("pert_prof: Failed to copy phase_shift array on device!");
                cudaEventDestroy(start_kernel);
                return EXIT_FAILURE;
            }
            // cudaMemcpyAsync(d_phase_shift, phase_shift, trial_size, cudaMemcpyHostToDevice, streams[ncand]);
            //configure the perturbed kernel parameters
            threadsPerBlock.x = fold.nbins;      // one thread for each bin (phase)
            threadsPerBlock.y = 1;
            blocksPerGrid.x =   fold.nsubbands; // one block per subband
            blocksPerGrid.y =   fold.nsubints;  // one block per subints
            shared_memsize = fold.nbins * sizeof(float);
            iolog(2, "last perturbate_kernel: delta_nu: %g delta_dm: %g delta_nudot: %g\n",
                                                     delta_nu, delta_dm/K_DM, delta_nudot);
             perturbate_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize, streams[ncand] >>>(D_folded,
                                              d_adjusted, d_phase_shift, ncand,
                                              fold.nbins,
                                              index_max[ncand]);
             if (err != cudaSuccess) {
                 errlog("pert_prof: (1) Failed to launch profile_kernel (error code %s)!",
                                                                cudaGetErrorString(err));
                 //destroy events
                 cudaEventDestroy(start_kernel);
                 return EXIT_FAILURE;
             }
            //launch kernel for profile
            threadsPerBlock.x = fold.nbins;      // one thread for each bin (phase)
            threadsPerBlock.y = 1;
            blocksPerGrid.x = fold.nsubints;    // one block for each sub-integration
            blocksPerGrid.y = 1;    // one block for each sub-integration
            profile_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize,
                              streams[ncand] >>>(d_adjusted, D_outfprof, ncand,
                              fold.nsubbands);
            if (err != cudaSuccess) {
                errlog("profiles: (1) Failed to launch profile_kernel (error code %s)!",
                        cudaGetErrorString(err));
                return EXIT_FAILURE;
            }
            cudaDeviceSynchronize();
            if (EnableWrite == MYTRUE) {
                uint64_t mem_size = fold.ncand * fold.nsubints * fold.nbins * sizeof(float);
                memset(H_outfprof, 0, mem_size);
                cudaMemcpyAsync(H_outfprof, D_outfprof, mem_size, cudaMemcpyDeviceToHost,0);
                cudaEventCreate(&stop);
                cudaEventRecord(stop, 0);

                // have CPU do some work while waiting for stage 1 to finish
                unsigned long int counter=0;

                while (cudaEventQuery(stop) == cudaErrorNotReady)
                {
                    counter++;
                }
                cudaEventDestroy(stop);
            }
            //reduce kernel for profile
            threadsPerBlock.x = fold.nbins;
            threadsPerBlock.y = 1;
            blocksPerGrid.x = 1;
            blocksPerGrid.y = 1;
            profile_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize,
                              streams[ncand] >>>(D_outfprof, D_outprof, ncand,
                              fold.nsubints);
            if (err != cudaSuccess) {
                errlog("profiles: (2) Failed to launch profile_kernel (error code %s)!\n",
                                                                 cudaGetErrorString(err));
                return EXIT_FAILURE;
            }
            cudaDeviceSynchronize();
            if (EnableWrite == MYTRUE) {
                uint64_t mem_size = fold.ncand * fold.nbins * sizeof(float);
                memset(H_outprof, 0, mem_size);
                cudaMemcpyAsync(H_outprof, D_outprof, mem_size, cudaMemcpyDeviceToHost,0);
                cudaEventCreate(&stop);
                cudaEventRecord(stop, 0);

                // have CPU do some work while waiting for stage 1 to finish
                unsigned long int counter=0;

                while (cudaEventQuery(stop) == cudaErrorNotReady)
                {
                    counter++;
                }
                cudaEventDestroy(stop);
            }
        }
        if (verbose) {
            iolog(0, "------------------------------------------------------------------\n");
        }
        if (res) {
            fclose(res);
            // temporary

            if (res_filename) {
                /* we store results in results.txt */
                char tmp[80];
                sprintf(tmp, "cp %s result.txt", res_filename);
                FILE *ttt = popen(tmp, "w");
                pclose(ttt);
            }
        }
        // write optimization trials on file
        write_optimization(nelem, nperiod, npdot, ndm, trial_param, Host_best);
        //free allocated memory for params.
        free(trial_param);
        free(sn_max);
        free(index_max);
    }
    return EXIT_SUCCESS;
}

