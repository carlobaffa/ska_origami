% Plot a 3d representation of results and S/N
% we read results.txt whith column meaning:
% ID opt_indx S/N width period pdot dm
% $Id: result3dplot.m 953 2017-05-18 09:51:10Z baffa $
% 
% 

subplot(3,1,1);

data = load("result.txt");
if (size(data)(:) < 20)
    printf ("not enogh data\n\n");
    return;
endif
c = colormap("default");
c = colormap("prism");
period = data(:, 5) * 1000.;
dm     = data(:, 7);
s_n    = data(:, 3);
#s_n = log(s_n);
pdot   = data(:, 6);
whos

hist(s_n);
ylabel("count number");
xlabel("signal/noise");
title("histogram of S/N values")

subplot(3,1,2);
%plot(period, s_n)
colore = floor(1 + (s_n - min(s_n)) / max(s_n) * 64);
%colore = 1:128;
siz    = 5 + 5*log10(colore);
scatter(period, dm, siz, colore, "filled")
%plot(period, dm)
title("color maps S/N");
xlabel("period");
ylabel("Dispersion Measure")

subplot(3,1,3);
scatter3(period, dm, pdot, siz, colore, "filled")
title("color maps S/N");
xlabel("period");
ylabel("Dispersion Measure")
zlabel("pdot")