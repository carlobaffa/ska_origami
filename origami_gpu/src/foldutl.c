#include <folding.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/*
 * utility to get svn version number    *
 *  from Brian Tanner
 */
char svnVersionString[256];
const size_t mylen = 80;


char* get_svn_version(void){
	int howMuchToCopy=0;
    char * t ;
	char *theVersion="$Revision: 1209 $      ";
	howMuchToCopy=strlen(theVersion+11) - 2;
	//assert(howMuchToCopy>0);
	memcpy(svnVersionString,  theVersion+11, howMuchToCopy);
        svnVersionString[howMuchToCopy] = '\0';
        if (NULL != (t = index(svnVersionString,'$'))) {
            t[0] = ' '; // eliminiamo il $
        }
	return svnVersionString;
}

/*
 * void iolog(int severity, const char *fmt, ...)
 */
/**
 *
 * iolog. Routine which print a log string in svbio window
 * we added a severity code as follows:
 * - -1 = always printed
 * - 0 = just a notice
 * - 1 = to show to the user
 * - 2 = more details
 * - 3 = very verbose
 *
 * \param severity the message severity level
 * \param fmt a standard printf() format string
 *
 */
void iolog(int severity, const char *fmt, ...)
{
    char buff[250], bufc[2];    /* buffers for excluding formatting chars */
    char message[250];
    int i,  len;
    va_list args_list;

    /* we produce the output */
    va_start(args_list,fmt);
    vsnprintf(message, 250, fmt, args_list);
    va_end(args_list);
    /* in case we log message */
    /* we log only the printable part */
    i = 0;
    buff[0] = '\0';
    if ((severity < -2) || (severity > 4)) { /* only a sanity check */
        severity = 0;
    }
    if (0 == severity) {
        sprintf(buff, "Warning: ");
    } else if (-1 == severity) {    // always printed
        sprintf(buff, "Msg-: ");
    } else {
        sprintf(buff, "Msg%1d: ", severity);
    }
    len = (int) strlen(message);
    len = (len <100) ? len :100;
    while (i < len) {
        /* filter away non printable characters */
        if (isprint(message[i])) {
            bufc[0] = message[i];
            bufc[1] = '\0';
            strncat(buff, bufc, 1);
        }
        i++;
    }
    if (Logging == MYTRUE) {
        /* a check for opened file */
        if (NULL != LogFptr ) {
            fprintf(LogFptr, "%s\n", buff);
        }
    }
    /* print to stdout only messages with severity >= 0 */
    if ((severity < -1 ) || (severity > Debug)) {
        return;
    }
    printf("%s\n", buff);
}

/*
 * void errlog(const char *fmt, ...)
 */
/**
 *
 * Routine which print an error log string on the standard output and
 * prints it in the log file.
 *
 * \param fmt a standard printf() format string
 *
 */
void errlog(const char *fmt, ...)
{
    char buff[250], bufc[2];    /* buffers for excluding formatting chars */
    char message[250];
    int i, len;
    va_list args;

    va_start(args,fmt);
    vsprintf(message,fmt,args);
    va_end(args);

    /* message */
    printf( "Err: %s\n", message);
    /* in case we log message */
    /* we log only the printable part */
    i = 0;
    buff[0] = '\0';
    len = (int) strlen(message);
    len = (len <100) ? len :100;
    sprintf(buff, "Err: _");
    //printf( "Iol: %d-%s-\n", len,buff);
    while (i < len) {
        if (isprint(message[i])) {
            bufc[0] = message[i];
            bufc[1] = '\0';
            strncat(buff, bufc, 1);
            //printf( "Err: %d-%s-\n", i,buff);
        }
    i++;
    }
    if (Logging == MYTRUE) {
        if (NULL != LogFptr ) {
            fprintf(LogFptr, "%s\n", buff);
        }
    }
    /* if important (!) errors we log next part */
    if ((NULL != strchr(message,'!')) && (80 < (len = (int) strlen(message))) ) {
        len = (len < 160) ? len : 1600;
        i = 80;
        sprintf(buff, "Err: -");
        while (i < len) {
            if (isprint(message[i])) {
                bufc[0] = message[i];
                bufc[1] = '\0';
                strncat(buff, bufc, 1);
            }
        i++;
        }
        if (Logging == MYTRUE) {
            if (NULL != LogFptr ) {
                fprintf(LogFptr, "%s\n", buff);
            }
        }
    }
    fflush(NULL);
}


/*
 * double get_current_time(void)
 */
/**
 * Uses the function clock_gettime() to get the current time.
 *
 * \return the current time in millisec.
 *
 */
double get_current_time()
{
    double t;
    struct timespec now;
#ifdef NO
    struct timeval now;

    gettimeofday(&now, NULL);
    t = (double) now.tv_sec * 1000 + (double)now.tv_usec *1e-3;
#endif
    clock_gettime(CLOCK_REALTIME, &now);
    t = (double) now.tv_sec * 1000 + (double)now.tv_nsec *1e-6;
    return t;
}

/*
 * int stick_this_thread_to_core(int core_id)
 */
int stick_this_thread_to_core(int core_id)
{
    int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
    if (core_id >= num_cores)
        return -1;

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_id, &cpuset);

    pthread_t current_thread = pthread_self();
    return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

/*
 * int isDirectory(const char *path)
 */
/**
 *
 * \param path          the directory to check 
 *
 * The function checks for existence of a directory defined in path.
 *
 * \return MYTRUE on success, MYFALSE otherwise
 */
int isDirectory(const char *path) {
    struct stat statbuf;
    if (stat(path, &statbuf) != 0)
        return MYFALSE;
    if (S_ISDIR(statbuf.st_mode) != 0)
        return MYFALSE;
    return MYTRUE;
}

/*
 * int myMakeDir(const char *path)
 */
/**
 *
 * \param path          the directory to check/create
 *
 * The function checks for existence of a directory defined in path.
 *
 * \return TRUE on success, FALSE otherwise
 */
int myMakeDir(const char *path) {
    if (MYFALSE == isDirectory(path))  {
        if(!mkdir(path, 0700))
	    return MYFALSE;
    }
    return MYTRUE;
}

/*
 * int write_profile(int isubint, int* maxbin, int reduced, int opt_phase)
 */
/**
 *
 * \param isubint       the sub-integration number
 * \param maxbin        the array with the number of phses of each * candidate
 * \param reduced       flag to signal if nsubints are summed up (1) or no (0)
 * \param opt_phase     flag to signal if optimization phase is done (1) or not (0)
 *
 * The function writes on disk the profiles built after the folding phase
 * (opt_phase = 0) and after the optimization phase (opt_phase = 1).
 * The after folding phase profiles are the phase profiles for each sub-integration
 * with all freq. channels summed up.
 * The after optimization phase profile is the final phase profile.
 *
 * \return EXIT_SUCCESS on success, EXIT_FAILURE otherwise
 */
int write_profile(int isubint, candidate_t* candidate, int reduced, int opt_phase)
{
    int nbin;           // loop variable for bins
    int ncand =0;       // loop variable for candidates
    FILE *fp = NULL;
    char filename[64];

    // we check and incase create the profile directory
    if (MYFALSE == myMakeDir(PROFILEDIR))
        return EXIT_FAILURE;
    //for (ncand = 0; ncand < fold.ncand; ncand ++) {
    for (ncand = 0; ncand < fold.ncand; ncand ++) {
        memset(filename, 0, sizeof(filename));
        if (reduced) {      // nsubints are summed up ?
            snprintf(filename, sizeof(filename), "%s/reduced_profile_%03d", PROFILEDIR, ncand);
        } else {            // no sum on nsubints
            snprintf(filename, sizeof(filename), "%s/profile_%03d", PROFILEDIR, ncand);
        }
        if (opt_phase == 0) {   //folding phase
            strcat(filename, ".out");
        } else {
            strcat(filename, ".opt");
        }
        if( 0 == isubint) {
            fp = fopen(filename, "w");
        } else {
            fp = fopen(filename, "a");
        }
        if (fp == NULL) {
            errlog("Can't open file %s in append mode!!", filename);
            return EXIT_FAILURE;
        }
        iolog(2, "writing output for subint %d candidate %d with maxbin %d", isubint, ncand,
                                                                candidate[ncand].phase_bin);
        for (nbin = 0; nbin < MAXPHASES; nbin ++) {
            if (nbin < candidate[ncand].phase_bin) {
                if (reduced == 0) {
                    fprintf(fp, "%d %d %f\n", isubint, nbin, H_outfprof[nbin + isubint * fold.nbins + ncand * fold.nsubints * fold.nbins]);
                } else {
                    fprintf(fp, "%d %f\n", nbin, H_outprof[nbin + ncand * fold.nbins]);
                }
            }
        }
        fprintf(fp, "\n");
        fclose(fp);
    }
    return EXIT_SUCCESS;
}

/*
 * void write_optimization(float * trial_param, float *best)
 */
/**
 *
 * \param nelem         number of optimization trials
 * \param nperiod       number of trials of period
 * \param npdot         number of trials of pdot
 * \param ndm           number of trials of dm
 * \param trial_param   array with the 3-tuple
 * \param best          array with the S/N values
 *
 * The function store into a file all the S/N values of the optimization phase.
 *
 * \return EXIT_SUCCESS on success, otherwise EXIT_FAILURE
 *
 */
int write_optimization(int nelem, int nperiod, int npdot, int ndm, float * trial_param, float *best)
{
    int i, j, k;        //index
    int index = 0;      // global trial index
    int ncand = 0;
    char filename[64];
    FILE *fp = NULL;

    // we check and incase create the profile directory
    if (MYFALSE == myMakeDir(PROFILEDIR))
        return EXIT_FAILURE;
    //
    iolog(1,"optimization data files in %s",PROFILEDIR);
    for (ncand = 0; ncand < fold.ncand; ncand ++) {
        snprintf(filename, sizeof(filename), "%s/optimization_%03d.out",PROFILEDIR, ncand);
        fp = fopen(filename, "w");
        if (fp == NULL) {
            errlog("Can't open file %s !!", filename);
            return EXIT_FAILURE;
        }
        index = 0;
        fprintf(fp, "0 0 %d %d %d\n", nperiod, npdot, ndm);
        for (i = 0; i < nperiod; i ++) {
            for (j = 0; j < npdot; j ++) {
                for (k = 0; k < ndm; k++) {
                    fprintf(fp, "%d %.7e %.8e %.8f %f\n", index,
                            trial_param[index * 3 + ncand * nelem * 3],
                            trial_param[index * 3 + ncand * nelem * 3 + 1],
                            trial_param[index * 3 + ncand * nelem * 3 + 2],
                            best[index + ncand * nelem* 2]);
                    index ++;

                }
            }
        }
        fprintf(fp, "\n");
        fclose(fp);
    }
    return EXIT_SUCCESS;
}

/*
 * int compare(const void * const _first, const void *const _second)
 */
/**
 * The callback function called by the qsort() method.
 * Compares two values.
 * 20181105 If periods are equal sort also on dm and on pdot
 *
 * \return 1 if the first value is larger than the second, 0 otherwise
 */
int compare(const void * const _first, const void *const _second)
{
    candidate_t *first = (candidate_t*)_first;
    candidate_t *second = (candidate_t*)_second;
    if (first->period > second->period) {
        return 1;
    } else if  (first->period < second->period) {
        return -1;
    }
    // if control reach here, periods are equal. We sort on dm
    if (first->dm > second->dm) {
        return 1;
    } else if  (first->dm < second->dm) {
        return -1;
    }
    // if control reach here, periods and dm are equal. We sort on pdot
    if (first->pdot > second->pdot) {
        return 1;
    } else if  (first->pdot < second->pdot) {
        return -1;
    }
    //
    // if control reach here, candidates are equal. No sort
    return 0;
}

/*
 * int rebin_candidate(binning_t *rebin, candidate_t * candidate)
 */
/**
 * The function populates the structures with the candidates whose period
 * satisfies the time intervalis reported here below.
 * All the candidates belonging to the same structure, rebins the input
 * data using the same value.
 *
 * Returns MYFALSE if something went wrong
 *
 */
int rebin_candidate(binning_t *rebin, candidate_t * candidate)
{
    int ncand = 0;      // the candidate number
    int index = 0;      // the prebin index [0,5]
    float FixedPeriod[MAX_NREBIN*5] = {  1.0008, 1.0016, 1.0032, 1.0064, 1.049  ,   // 1,
                                         0.0001, 1.0016, 1.0032, 1.0064, 1.049  ,   //    2,
                                         0.0001, 0.0001, 1.0032, 1.0064, 1.049  ,   //       4,
                                         0.0001, 0.0001, 0.0001, 1.0064, 1.049  ,   //          8,
                                         0.0001, 0.0001, 0.0001, 0.0001, 1.049  };  //             16
    //float _period[MAX_NREBIN] = {0.0008, 0.0016, 0.0032, 0.0064, 0.049 };   // 1, 2, 4, 8, 16
    float _period[MAX_NREBIN] = {0.0008, 0.0016, 0.0032, 0.0064, 0.049 };   // 1, 2, 4, 8, 16
    float *period = _period;    // to have a variable table
    //to calculate the max spin period, we use the default value of the
    //number of phases (128) and not the max one (256) because the default
    //is the better compromise between performance and period time. Also we
    //use O_PHASES - 2 because the folding kernel use the last to phases
    //(127 and 128) to store the phase 0 and 1 to not use the modulo
    //operator (see device_folding.cu)
    float max_period = (O_PHASES - 2)* fold.tsamp * MAXPREBIN * 0.5;
// 15/04/2015: implement the rebinning limit on period depending on the
// sampling time

    if (1 == RebinStrategy) { // standard strategy : fastest possible.
        for (index = 0; index < MAX_NREBIN - 1; index ++) {
            _period[index] = MINPHASES * (2 << index) * fold.tsamp * 0.5;
            iolog(1, "period[%d]: %f", index, _period[index]);
        }
    } else if (2 == RebinStrategy) { // slowest strategy : min possible rebin
        for (index = 0; index < MAX_NREBIN - 1; index ++) {
            _period[index] = (O_PHASES-2) * (1 << index) * fold.tsamp * 0.5;
            iolog(1, "period[%d]: %f", index, _period[index]);
        }
    } else {
        errlog("Invalid RebinStrategy = %d", RebinStrategy);
        return MYFALSE;
    }
    _period[MAX_NREBIN - 1] = max_period;
    iolog(1, "period[%d]: %f", MAX_NREBIN - 1, _period[MAX_NREBIN - 1]);
        // provision for forced prebinning
        if (MYTRUE == FixedRebin) {
            iolog(1, "rebinning factor: %d ", Rebin);
            period = &FixedPeriod[MAX_NREBIN*(Rebin-1)];
            iolog(1, "period[] = %.4f,  %.4f,  %.4f,  %.4f,  %.4f",
                    period[0], period[1], period[2], period[3], period[4]);
        }

    ncand = 0;
    // initialize the rebin structures
    for (index = 0; index < MAX_NREBIN; index ++) {
        if (MYTRUE == FixedRebin) {
            rebin[index].rebin = Rebin;
        } else {
            rebin[index].rebin = 1 << index;
        }
        rebin[index].first         = -1;
        rebin[index].last          = -1;
        rebin[index].pitch_dim     = -1;
        rebin[index].msize         = -1;
    }
    //loop on rebin values to identify the candidates whose period falls
    //into current rebin time interval
    // NOTE the index is in reality the exponent of 2 of the pre-folding factor
    // (number of folding). Range 1-16
    //A rebin entry is valid if the element first and last are != -1
    for (index = 0; index < MAX_NREBIN; index ++) {
        printf("fold.ncand: %d index: %d rebin: %d period[%d]: %f candidate[%d].period: %f\n", fold.ncand, index,
                                       rebin[index].rebin, index, period[index], ncand, candidate[ncand].period);
        while (((candidate[ncand].period < period[index]) && (ncand < fold.ncand))) {
            if (rebin[index].first > -1) {
                rebin[index].last ++;
            } else {
                if (MYTRUE == FixedRebin) {
                    rebin[index].rebin = Rebin;
                } else {
                    rebin[index].rebin = 1 << index;
                }
                rebin[index].first = ncand;
                rebin[index].last = ncand;
            }

            iolog(1, "rebinning factor: %d candidate[%d].period: %f rebin[%d].first: %d rebin[%d].last: %d\n",
                                                                                         rebin[index].rebin,
                                                                                                      ncand,
                                                                                    candidate[ncand].period,
                                                                                  index, rebin[index].first,
                                                                                                      index,
                                                                                         rebin[index].last);
            ncand ++;
        }
        if (ncand == fold.ncand) break;
    }
    for (index = 0; index < MAX_NREBIN; index ++) {
        iolog(2, "%d) first: %d rebin: %d\n", index, rebin[index].first, rebin[index].rebin);
    }
    return EXIT_SUCCESS;
}

/*
 * int first_valid_binidx(binning_t *rebin)
 */
/**
 * Finds the first rebin entry index with a number of candidates > 0
 *
 * \return the rebin entry index (>= 0) if exists, otherwise < 0
 *
 */
int first_valid_binidx(binning_t *rebin)
{
    int nbinning = 0;

    for (nbinning = 0; nbinning < MAX_NREBIN; nbinning ++) {
        if (rebin[nbinning].first > -1) {
            return nbinning;
        }
    }
    return -1;
}

/*
 * int is_binentry_valid(int nbinning, binning_t *rebin)
 */
/**
 * Checks if the rebin entry is valid, i.e. if it has a number of candidates
 * > 0
 *
 * \return MYTRUE if the entry is valid, otherwise MYFALSE
 */
int is_binentry_valid(int nbinning, binning_t *rebin)
{
    if (rebin[nbinning].first == -1) {
        return MYFALSE;
    }
    return MYTRUE;
}

/*
 * int is_binvalue_valid(int binval, binning_t *rebin)
 */
/**
 * Checks if the rebin value corresponds to an existing entry
 *
 * \return the rebin structure array index (>= 0)  if the bin value
 * is valid, otherwise < 0
 */
int is_binvalue_valid(int binvalue, binning_t *rebin)
{
    int i = 0;
    for (i = 0; i < MAX_NREBIN; i ++) {
        if ((rebin[i].rebin == binvalue) && (rebin[i].first > -1)) {
            return i;
        }
    }
    return -1;
}

/*
 * int get_prebin_candidate(int ncand, binning_t *rebin)
 */
/**
 * Returns the rebinning factor of the specified candidate
 *
 * \return the candidate rebinning factor (> 0) on success, < 0 otherwise
 */
int get_prebin_candidate(int ncand, binning_t *rebin)
{
    int i = 0;
    for (i = 0; i < MAX_NREBIN; i ++) {
        if (is_binentry_valid(i, rebin) == MYTRUE) {
            if (ncand <= rebin[i].last) {
                return rebin[i].rebin;
            }
        }
    }
    return -1;
}

/*
 * int get_binidx_candidate(int ncand, binning_t *rebin)
 */
/**
 * Returns the rebinning index of the specified candidate
 *
 * \return the candidate index (>=0) on success, < 0 otherwise
 */
int get_binidx_candidate(int ncand, binning_t *rebin)
{
    int i = 0;
    for (i = 0; i < MAX_NREBIN; i ++) {
        if (is_binentry_valid(i, rebin) == MYTRUE) {
            if (ncand <= rebin[i].last) {
                return i;
            }
        }
    }
    return -1;
}

/* read a string from the input which looks like nchars-char[1-nchars] */
/* return the bytes read or -1 if error */
int get_string(int inputfile, int *nbytes, char string[])
{
    int nchar;
    int res = 0;  // result code
    memset(string,0,mylen);
    strcpy(string,"ERROR"); // init of output string
    if (0 > read(inputfile, &nchar, sizeof(int))) { // read field size
        return -1;
    }
    *nbytes=sizeof(int); // initial read
    // size zero handling: we read a single byte at time
    if (nchar>80 || nchar<1) {
        *nbytes = 1;
        return 0;
    }
    // real read
    res = read(inputfile, string, nchar);
    string[nchar]='\0'; // terminate string
    *nbytes+=nchar;   // add the string lenght
    return res;
}


/* attempt to read in the general header info from a pulsar data file */
int read_header(int inputfile)
{
    char string[mylen];
    int nbytes = 0;
    int totalbytes = 0;
    int itmp = 0;
    int res = 0;
    //int nbytes,totalbytes,expecting_rawdatafile=0,expecting_source_name=0;
    //int expecting_frequency_table=0,channel_index;
    // to decode double/int values
    int * decoded_int = (int *)  string;
    double * decoded_double = (double *) string;
    //float * decoded_float = (float *) string;

    nbytes = 0;
    /* try to read in the first line of the header */
    get_string(inputfile, &nbytes, string);
    iolog(1," string at %d is >%s< %d",totalbytes,string, strlen(string));
    if (0 != strcmp(string,"HEADER_START")) {
	/* the data file is not in standard format, rewind and return */
	    lseek(inputfile, 0, SEEK_SET);
	    return -1;
    }
    /* store total number of bytes read so far */
    totalbytes = nbytes;

    /* loop over and read remaining header lines until HEADER_END reached */
    while (itmp++ < 100) {
        get_string(inputfile, &nbytes, string);
        iolog(1," string at %d is >%s<",totalbytes,string);
        totalbytes += nbytes;
        if (0 == strcmp(string,"HEADER_END")) {
            break;
        }
        // we handle arguments

        if (0 == strcmp(string,"az_start")) {
            res = read(inputfile, string, sizeof(double));
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"za_start")) {
            res = read(inputfile, string, sizeof(double));
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"src_raj")) {
            res = read(inputfile, string, sizeof(double));
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"src_dej")) {
            res = read(inputfile, string, sizeof(double));
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"tstart")) {
            res = read(inputfile, string, sizeof(double));
            iolog(0,"tstart from inputfile = %lf ", res, *decoded_double);
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"tsamp")) {
            res = read(inputfile, string, sizeof(double));
            iolog(0,"tsamp from inputfile = %lf ", res, *decoded_double);
            head_fold.tsamp = *decoded_double;
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"period")) {
            res = read(inputfile, string, sizeof(double));
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"fch1")) {
            res = read(inputfile, string, sizeof(double));
            iolog(0,"freq_ch1 from inputfile = %lf ", res, *decoded_double);
            head_fold.freq_ch1 = *decoded_double*1.e6;
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"fchannel")) {
            res = read(inputfile, string, sizeof(double));
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"foff")) {
            res = read(inputfile, string, sizeof(double));
            iolog(0,"freq_off from inputfile = %lf ", res, *decoded_double);
            head_fold.freq_off = *decoded_double*1.e6;
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"nchans")) {
            res = read(inputfile, string, sizeof(int));
            iolog(0,"nchan from inputfile = %d ", *decoded_int);
            head_fold.nchan = *decoded_int;
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"telescope_id")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"machine_id")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"data_type")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"ibeam")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"nbeams")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"nbits")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"barycentric")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"pulsarcentric")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"nbins")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"nsamples")) {
          /* res = read this one only for backwards compatibility */
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"nifs")) {
            res = read(inputfile, string, sizeof(int));
            totalbytes +=sizeof(int);
        } else if (0 == strcmp(string,"npuls")) {
            res = read(inputfile, string, sizeof(long int));
            totalbytes +=sizeof(long int);
        } else if (0 == strcmp(string,"refdm")) {
            res = read(inputfile, string, sizeof(double));
            totalbytes +=sizeof(double);
        } else if (0 == strcmp(string,"signed")) {
            res = read(inputfile, string, sizeof(char));
            totalbytes +=sizeof(char);
        } else if (0 == strcmp(string,"source_name")) {
            get_string(inputfile, &nbytes, string);
            totalbytes +=nbytes;
            iolog(1,"source_name from inputfile = %s", string);
        } else {
          iolog(0,"res = %d read_header - unknown parameter: %s\n",res, string);
        }
    }
    /* return total number of bytes res = read */
    return   totalbytes ;
}

