% $Id: phase_sim.m 947 2017-05-15 10:21:19Z baffa $
%
% phase_sim.m simulation of phase differences on double/float computations
%
% input values:
%       tobs    Sampling time in sec (default 1)
%       nchan   the number freq. channels (default 4096)
%       dm      dedispersion measure (default 12)
%
%   Assumed internal values:
%       period   1.1236e-3  pulsar period in sec.
%       tsamp    50e-6      Sampling time in sec
%       pdot     0          acceleratio on period
%       freq_ch1 1550e6     first channel freq. in Hz
%       freq_off -0.075e6   delta freq on channels
%       nchan    4096       numbers of frequebncy channels
%
%   derived
%       nsamp    tobs/tsamp the number of samples (20000 for 1 sec)
%       nbins    period/tsamp number of phases for binning
%
function differences = phase_sim (tobs=1, nchan=4096, dm=12)
%
% arguments
if (nargin() < 1)
    printf(" usage phase_sim (tobs, nchan, dm)\n");
    tobs    = 1;
    nchan   = 4096;
    dm      = 12.0;
endif
%

% default values

    differences = 0;
    period   = 1.1236e-3 ; %pulsar period in sec.
    tsamp    = 50e-6     ; %Sampling time in sec
    pdot     = 0         ; %acceleratio on period
    freq_ch1 = 1550e6    ; %first channel freq. in Hz
    freq_off = -0.075e6  ; %delta freq on channels
    nchan    = 4096      ; %numbers of frequebncy channels

% derived values
    nsamp    = tobs/tsamp;
    nbins    = period/tsamp;
    nu       = 1/period  ;
    nudot    = pdot/period/period;

% phase 0 frequecy array
    freq_ch1_2 = freq_ch1*freq_ch1;
    freq_chan  = [freq_ch1:freq_off:(freq_ch1+(nchan-1)*freq_off)];
    delta_freq = dm * ( ones(1,nchan) / freq_ch1_2 - ones(1,nchan) ./ (freq_chan .* freq_chan) );
    whos
    %plot(freq_chan,delta_freq)

% physical values.
    dm_delay   = dm * delta_freq;
    dm_phase   = dm_delay * nu;

% time computations
    time_idx   = [0:(nsamp-1)];
    time_s     = -tobs * 0.5 + time_idx * tsamp; % we do not use subint 
    phase      = time_s * nu + (time_s .* time_s) * (nudot * 0.5 + dm_phase(1));

    frac       = (phase - floor(phase)) * nbins;

    %plot(time_s, phase, time_s, frac)
    %plot(time_s, frac)
    %pause
    %whos
    %single precision values

% physical values.
    dm_delay_s = single(dm * single(delta_freq));
    dm_phase_s = single(dm_delay * nu);

% time computations

    time_s_s     = single(-tobs * 0.5 + single(time_idx * tsamp)); % we do not use subint 
    phase_s      = single(time_s_s * nu + (time_s_s .* time_s_s) * 
                         single(nudot * 0.5 + dm_phase_s(1)));

    frac_s       = single((phase_s - single(floor(phase_s))) * nbins);
    %plot(time_s, frac, time_s_s, frac_s)
    %plot(1:nsamp,time_s-time_s_s, 1:nsamp, frac-frac_s)
    subplot(2,1,1)
    plot(1:nsamp,time_s-double(time_s_s))
    title("Rounding errors: Differences in computed sample time (double-float)")
    ylabel("differences in seconds")
    %xlabel("sample sequential number")
    %pause
    subplot(2,1,2)
    plot(1:nsamp, phase - double(phase_s));
    title("Rounding errors: Differences in computed phase (double-float)")
    ylabel("fractional difference")
    xlabel("sample sequential number")
    %pause
    %whos
    print("differences.gif","-S800,640")

return ;
endfunction; 
