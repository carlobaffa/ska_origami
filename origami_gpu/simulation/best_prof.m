% $Id: best_prof.m 947 2017-05-15 10:21:19Z baffa $
%
% best_prof.m quick and dirty S/N computation on folding optimization phase
% output.
%
% input values:
%       tobs    Observing time in sec (default 60)
%
%   Assumed internal values:
%       period   1.1236e-3  pulsar period in sec.
%       tsamp    50e-6      Sampling time in sec
%       pdot     0          acceleratio on period
%       freq_ch1 1550e6     first channel freq. in Hz
%       freq_off -0.075e6   delta freq on channels
%       nchan    4096       numbers of frequebncy channels
%       row      361        19*19
%       column   44         phases of candidate
%
%   derived
%       nsamp    tobs/tsamp the number of samples (20000 for 1 sec)
%       nbins    period/tsamp number of phases for binning
%
function best_prof  = phase_sim (tobs=60, phases=44)
%
% arguments
if (nargin() < 1)
    printf(" usage best_prof (tobs)\n");
    tobs    = 60;
    phases  = 44;
endif
%

% default values

    differences = 0;
    period   = 1.1236e-3 ; %pulsar period in sec.
    tsamp    = 50e-6     ; %Sampling time in sec
    pdot     = 0         ; %acceleratio on period
    freq_ch1 = 1550e6    ; %first channel freq. in Hz
    freq_off = -0.075e6  ; %delta freq on channels
    nchan    = 4096      ; %numbers of frequebncy channels
    row      = 361       ;

% derived values
    nsamp    = tobs/tsamp;
    nbins    = period/tsamp;
    nu       = 1/period  ;
    nudot    = pdot/period/period;
    column   = phases;

    %plot(time_s, phase, time_s, frac)
    %plot(time_s, frac)
    %pause
    %whos
    %single precision values

    file = sprintf("bestprofile_0_%d.out",floor(tobs+0.1));
    %data = sprintf("bestprofile_0_%d",floor(tobs+0.1));
    data = load( file) ;

% we reshape matrix
    q_data = reshape(data,column,row);
    whos

% mesh axis
    [XX, YY] = meshgrid (1:row, 1:column);

% first display
    %mesh(XX,YY,q_data)
    mesh(XX(:,100:200),YY(:,100:200),q_data(:,100:200))

return ;
endfunction; 
