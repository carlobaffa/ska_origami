//
// C++/C API routines to use C++ Thread class 
//
#include "../include/XaThread.h"
#include "../include/XaMutex.h"
#include "../include/XaSemaphore.h"
#include "XaCAPI.h"

/* Thread handling */

/*
 * Define virtual method run
 */
void MyThread::run() {
    (this->func)(argument);
    return;
}
 
/**
 * void * cXaThread_new(int arg)
 *
 * Create a new thread 
 * \param arg   argument to pass to creation function
 *
 */
extern "C" void * cXaThread_new(void *arg, void f(void *))
{
    MyThread *thr = new MyThread(arg, f);
    return (void*)thr;
}

//extern "C" void  cXaThread_join(void *arg)
extern "C" int cXaThread_join(void *arg)
{
    MyThread *thr = (MyThread*)arg;
    //thr->wait();
    return pthread_join(thr->ID(), NULL);
    //return;
}

extern "C" void cXaThread_delete(void *arg)
{
    MyThread *thr = (MyThread*) arg;
    delete thr;
    return;
}

extern "C" pthread_t cXaThread_ID(void *arg)
{
    MyThread *thr = (MyThread*) arg;
    return thr->ID();
}


extern "C" void * cXaMutex_new(int locked, int attr)
{
    Mutex *mtx = new Mutex(locked, (Mutex::Attribute)attr);
    return (void*)mtx;
}

extern "C" void cXaMutex_lock(void *arg)
{
    Mutex *mtx = (Mutex *)arg;
    mtx->lock();
    return;
}

extern "C" void cXaMutex_unlock(void *arg)
{
    Mutex *mtx = (Mutex *)arg;
    mtx->unlock();
    return;
}

extern "C" bool cXaMutex_trylock(void *arg)
{
    Mutex *mtx = (Mutex *)arg;

    return mtx->trylock();
}

extern "C" void cXaMutex_delete(void *arg)
{
    Mutex *mtx = (Mutex*)arg;

    delete mtx;
}

extern "C" void * cXaSemaphore_new(int num)
{
    Semaphore *sem = new Semaphore(num);
    return (void*)sem;
}

extern "C" int cXaSemaphore_acquire(void *arg, int num)
{
    Semaphore *sem = (Semaphore *)arg;
    return sem->acquire();
}

extern "C" int cXaSemaphore_release(void *arg, int num)
{
    Semaphore *sem = (Semaphore *)arg;
    return sem->release();
}

extern "C" int cXaSemaphore_wait(void *arg, int num)
{
    Semaphore *sem = (Semaphore *)arg;
    return sem->CondWait();
}

extern "C" int cXaSemaphore_available(void *arg)
{
    Semaphore *sem = (Semaphore *)arg;
    return sem->available();
}

extern "C" void cXaSemaphore_delete(void *arg)
{
    Semaphore *sem = (Semaphore*)arg;

    delete sem;
}
