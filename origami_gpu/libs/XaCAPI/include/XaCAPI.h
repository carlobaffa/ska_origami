#ifndef HEADER_H
#define HEADER_H

#include <pthread.h>

#ifdef __cplusplus
#include "XaThread.h"
class MyThread : public Thread
{
    private:
        void *argument;
        void (*func)(void *);
    public:
        MyThread(void *arg, void f(void *)):Thread(Thread::NotDetached)
        {
            func = f;
            argument = arg;
        }
        void run();
};


extern "C" void *cXaThread_new(void *arg, void func(void *));
//extern "C" void cXaThread_join(void * arg);
extern "C" int cXaThread_join(void * arg);
extern "C" void cXaThread_delete(void *arg);
extern "C" pthread_t cXaThread_ID(void *arg);
extern "C" void *cXaMutex_new(int locked, int attr);
extern "C" void cXaMutex_lock(void *arg);
extern "C" void cXaMutex_unlock(void *arg);
extern "C" bool cXaMutex_trylock(void *arg);
extern "C" void cXaMutex_delete(void *arg);
extern "C" void *caXaSemaphore_new(int num);
extern "C" int cXaSemaphore_acquire(void *arg, int num);
extern "C" int cXaSemaphore_release(void *arg, int num);
extern "C" int cXaSemaphore_wait(void *arg, int num);
extern "C" int cXaSemaphore_available(void *arg);
extern "C" void cXaSemaphore_delete(void *arg);

#else
typedef enum {
    false = 0,
    true = 1
} bool;
extern void *cXaThread_new(void *arg, void f(void *));
//extern void cXaThread_join(void *arg);
extern int cXaThread_join(void *arg);
extern void cXaThread_delete(void *arg);
pthread_t cXaThread_ID(void *arg);
extern void *cXaMutex_new(int locked, int attr);
extern void cXaMutex_lock(void *arg);
extern void cXaMutex_unlock(void *arg);
extern bool cXaMutex_trylock(void *arg);
extern void cXaMutex_delete(void *arg);
extern void *cXaSemaphore_new(int num);
extern int cXaSemaphore_acquire(void * arg, int num);
extern int cXaSemaphore_release(void *arg, int num);
extern int cXaSemaphore_wait(void * arg, int num);
extern int cXaSemaphore_available(void *arg);
extern void cXaSemaphore_delete(void *arg);

#endif

#ifdef __cplusplus
extern "C" {
#endif
//void crun(void *arg1, void *arg2);
#ifdef __cplusplus
}
#endif
#endif
