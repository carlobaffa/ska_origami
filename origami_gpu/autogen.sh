#!/bin/sh
echo "Running aclocal"
aclocal
echo "Running autoheader..."
autoheader
echo "Running automake --add-missing --foreign..."
automake --add-missing --foreign
echo "Running autoconf ..."
autoconf
