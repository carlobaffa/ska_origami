#ifndef _FOLDING_H
#define _FOLDING_H

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <stdarg.h>
#include <ctype.h>
#include <sched.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cuda_runtime.h>
#include <cuda.h>

#include "define.h"
#include <XaCAPI.h>

#ifdef __CUDACC__

#define ALIGN(x) __align__(x)

#else
#if defined(__GNUC__)

// GCC

#define ALIGN(x) __attribute__ ((aligned (x)))
// all other compilers
#else
#define ALIGN(x)
#endif
#endif
typedef struct _args_option_t args_option_t;
typedef struct _candidate_t candidate_t;
typedef struct _binning_t binning_t;

struct _args_option_t { 
    uint64_t nsamp;             //number of samples (in freq and time) used for folding
    uint64_t time_nsamp;        //number of total time samples 
    int nchan;                  //number of freq channels
    int ncand;                  //number of candidates
    int nbins;                  //number of phase bins
    int nsubints;               //number of sub integrations 
    int nsubbands;              //number of freq. sub-bands
    int prebin;                 //prebinning value 
    double tsamp;               //sampling time (in sec)
    double tobs;                //obserbing time (in sec)
    double period;              //pulsar period (in sec)
    double pdot;                //pulsar period drift (sec/sec)
    double freq_ch1;            //freq. of the first channel (MHz)
    double freq_off;            //freq. offset between channels (MHz)
    float dm;                   //pulsar DM (in cm(-3) pc)
    char* infile;               //data input filename
};

//
// struct _candidate_t
// structure with candidate info. It's used to re-order the candidate 
//
struct _candidate_t {
    int index;          //candidate index 
    int phase_bin;      //number of phase bins
    float dm;           //candidate dm (in cm(-3) pc)
    double period;      //candidate period (in sec)
    double pdot;        //candidate period drift (sec/sec)
    double nu;          //candidate rotation freq.
    double nudot;       //candidate rotation freq. drift
};

//
// struct _binning_t
//
// It collects the data of the candidates using the same rebinning factor
// on the input data  
struct _binning_t {
    uint64_t msize;     // device global memory size        
    int rebin;          // rebinning factor: 1, 2, 4, 8 (16 ?)
    int first;          // index of the first candidate
    int last;           // index of the last candidate
    int pitch_dim;      // the pitch or width of a 3D memory block. Can be different from the optimal one.
    float *d_out;       // pointer to the memory with transposed rebinned matrix 
    cudaStream_t stream;
    cudaEvent_t event;
//} __attribute__((aligned(0x0008)));
}ALIGN(8);

struct gpu_nvml {
        char name[256];
    int id;
    unsigned int temp;
        unsigned int fan_speed;
};

//
//
// struct gpu_info 
//
// structure with all the property of the device in use
struct gpu_info {
    int id;
    struct gpu_nvml nvml;
        int max_thread_per_block;       //max number of threads/block
        int max_thread_dim[3];          //max number of threads per dimension
        int max_grid_size[3];           //max grid dimension
        int warp_size;                  //number of threads in 1 warp
        size_t sharedMemPerBlock;       //max dimension of the shared memory/SM
        cudaStream_t *streams;          
};

//
// struct thr_info 
//
// structure with info to pass to the CPU threads
struct thr_info_t { 
    int id;                     // the cpu thread id
    int ch_num;                 // not used
    int ch_elem;                // not used
    uint64_t pitch_dim;         // the initial pitch_dim
    uint64_t chunk_size;        // the start dimension (in bytes) of the data in one subint
    pthread_t pth;              // the thread handle
    struct gpu_info *gpu;       // pointer to the gpu structure
};

//
// struct _Shared
// semaphore structure to access memory
typedef struct _Shared Shared_t;
struct _Shared{                         
    int front, rear;
    void *mutex;
    void *empty;
    void *full;                         
};

extern FILE *LogFptr;                   // pointer to log file 
extern char * InputFile;                // pointer to input data file name 
extern char _Inputfile[220];            // custom input data file name
extern int Keepgoing;                   
extern int Logging;                     // flag to enable/disable logging on file 
extern int Tobs_forced;                 // flag to enable forcing of tobs
extern int EnableCheck;                 // flag to enable/disable output data check
extern int EnableWrite;                 // flag to enable/disable output profiles  
extern int EnableSplit;                 // flag to enable/disable folding phase shift
extern int Header;                      // (flag) Header is present? 
extern int HeaderOffset;                // Offset due to header presence
extern int Debug;                       // flag to enable verbose output
extern int FixedRebin;                  // flag to enable/disable variable rebinning
extern int RebinStrategy;               // selecto of pre-computed rebinning strategy
extern int Ngpu;                        // number of devices (default=1)
extern int Nthread;                     // number of threads for device (default=1 )
extern int Rebin;                       // fixed rebinning if MYTRUE == FixedRebin
extern int First_rebin_idx;             // first rebinning index used
extern int Optimization;                // optimization sapce dimension (2D/3D)
extern float Gpu_time;
extern float Gpu_fold;
extern args_option_t fold;              // folding parameters in use
extern args_option_t head_fold;         // data header folding parameters
//host and device arrays
extern unsigned char *Ska_in[MAX_SLOT]; // input data in natural order
extern unsigned char *H_in[MAX_SLOT];   // host memory to map (transposed order)
extern unsigned char *D_in[MAX_SLOT];   // device pointer to mapped memory
extern float *D_folded;         // device memory to store folded profiles of candidates
extern float *Host_mtx;         // host memory to store output data to check
extern float *H_outfprof;       // host memory to store output profiles summed up in freq.    
extern float *H_outprof;        // host memory to store output profiles     
extern float *H_folded;         // host memory to store output folded   
extern struct gpu_info Gpus[MAX_GPUDEVICES];

// declare optimization output arrays for candidate period, dm and pdot
extern double *Opt_period;      // optimized periods
extern float  *Opt_dm;          // optimized dm
extern double *Opt_pdot;        // optimized pdot

/* Function declarations */

#ifdef __CUDACC__
/* to export C function to nvcc */
/* foldutl.c */
extern "C" void iolog(int severity, const char *fmt, ...);
extern "C" void errlog(const char *fmt, ...);
extern "C" double get_current_time(void);
extern "C" int write_optimization(int nelem, int nperiod, int npdot, int ndm, float * trial_param, float *best);
extern "C" int first_valid_binidx(binning_t *rebin);
extern "C" int is_binentry_valid(int nbinning, binning_t *rebin);
extern "C" int get_prebin_candidate(int ncand, binning_t *rebin);
extern "C" int get_binidx_candidate(int ncand, binning_t *rebin);
extern "C" int is_binvalue_valid(int binvalue, binning_t *rebin);
extern "C" int read_header(int infile);

/* host_folding.cu */
extern int allocate_mem4best(int nelem);
extern "C" void release_memory(int ndev, binning_t * rebin);
/* device_folding.cu */
extern __global__ void input_converter(unsigned char *g_in, float *g_out, int pitch_dim, int local_measures, int nchan);
extern __global__ void reduce_kernel(double *output, int isubint, int local_mesaures, double tobs);
extern __global__ void folding_worker(float *g_in, float *g_out, float *g_weight, int pitch_dim, 
                                      int local_measures, int candidate, int rebin, int shared_phases, 
                                      int warp_count, double tobs, int max_phases, int isubint, 
                                      int threadblock_memory);
extern __global__ void folding_worker_nosplit(float *g_in, float *g_out, float *g_weight, int pitch_dim, 
                                      int local_measures, int candidate, int rebin, int shared_phases, 
                                      int warp_count, double tobs, int max_phases, int isubint, 
                                      int threadblock_memory);
extern __global__ void normalize_kernel(float *folded, float *weight, int ncand, float mean, int nsubband);
extern __global__ void profile_kernel(float *in, float *out, int ncand, int nloop);
extern __global__ void best_profile_kernel(float *in, float *out, int ncand, int nloop, int index, int ntrial);
extern __global__ void perturbate_kernel(float *g_in, float *g_out, double *d_phase_shift, 
                                         int ncand, int max_phases, int index);
extern __global__ void compute_sn(float* profile, float*best, int max_phases, int ncand, int nelem, float rms);
extern __global__ void transposeNoBankConflicts(float *odata, unsigned char *idata, int width, int height, int nsamp);
extern __global__ void transposePreBin(float *odata, unsigned char *idata, int width, int height, int nsamp, int prebin);
extern __global__ void transposePreBin_shfl(float *odata, unsigned char *idata, int in_width, 
                                                                        int nsamp, int out_width, int prebin);
extern __global__ void binInputKernel(float *odata, float *idata, int height, int in_width, int out_width, int prebin);
extern __global__ void binInputKernel_shfl(float *odata, float *idata, int in_width, int out_width, int prebin);

/* summary_statistics.cu */
extern void statistics(unsigned char *raw_in, int N, float *mean, float *rms);
extern void statistics_float(float *raw_in, int N, float *mean, float *rms);
extern void find_max(float *raw_in, int start, int N, float *max_val, int *index);
/* kernel.cu */
extern int rebinInputData(int start, int current,  binning_t *rebin);
#else
/* foldutl.c */
extern void iolog(int severity, const char *fmt, ...);
extern void errlog(const char *fmt, ...);
extern double get_current_time(void);
extern int stick_this_thread_to_core(int core_id);
extern int write_profile(int subint, candidate_t* candidate, int reduced, int opt_phase);
extern int compare(const void * const first, const void *const second);
extern int rebin_candidate(binning_t *rebin, candidate_t * candidate);
extern int first_valid_binidx(binning_t *rebin);
extern int is_binentry_valid(int nbinning, binning_t *rebin);
extern int is_binvalue_valid(int binvalue, binning_t *rebin);
extern int read_header(int infile);

/* kernel.cu */
extern int profiles(int ndev);
extern int perturbed_profiles(int ndev, int verbose, int opt_dim, candidate_t *candidates, 
                              binning_t * rebin, double *time_sum, double *freq_sum, int n_p, int rf_p, 
                              int n_pdot, int rf_pdot, int n_dm, int fr_dm, char *res_filename); 
extern int folding(int ndev, int isubint, unsigned char *d_in, candidate_t *candidate, binning_t *rebin);
/* host_folding.cu */
extern int gpu_init(void);
//extern int folding(int ndev, unsigned char *d_in, candidate_t *candidate, binning_t* rebin, int isubint);
extern int allocate_memory(int ndev, binning_t *rebin, double *delta_freq, candidate_t* candidate, 
                           double *time_sum, double *freq_sum);
extern void release_memory(int ndev, binning_t * rebin);
extern int init_streams(int ndev, int nstreams);
extern void release_streams(int ndev, int nstreams);
extern int init_rebindev_resources(int ndev, binning_t *rebin);
extern void release_rebindev_resources(int ndev, binning_t *rebin);
extern int reset_device(int ndev);
/* nvml.c */
extern int gpu_temp(int gpu);
extern int gpu_fanspeed(int gpu);
extern int init_nvml(void);
extern int shutdown_nvml(void);
/* opencl_cpu.c */
extern int  clDeviceNum(void);
extern int cpuTranspose_CL(int ncpu, int index, void *in_ptr, void* out_ptr,  int size_x,  int size_y, 
                                                 int nsamp_per_sub_int, int memsize, int mem_devsize);
#endif  // __CUDACC__

#endif //_FOLDING_H
