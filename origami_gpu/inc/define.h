#ifndef _DEFINE_H
#define _DEFINE_H

#ifndef MAX
#define MAX(a,b) (a > b ? a : b)
#endif

#define MAX_GPUDEVICES  32
#define MYTRUE           1
#define MYFALSE         -1

#define MAX_SLOT        4
#define NBYTES          sizeof(char)
#define INPUTFILE       "ska.dat"
#define PROFILEDIR      "./profiles"

// Input data definitions
#define MAXFREQUENCIES  4096
#define MINFREQUENCIES  64
#define O_FREQUENCIES   MAXFREQUENCIES
#define MAXSUBINT       256
#define O_SUBINT        64
#define MAXSEQMEASURES  187500
#define O_SEQMEASURES   MAXSEQMEASURES
#define MAXCANDIDATES   256
#define O_CANDIDATES    128

#define O_TIMESPLIT     MAX_SLOT

#define optimization strategy 2D/3D
#define O_2D            2       // only 2D optimization
#define O_3D            3       // only 3D optimization
#define O_2D3D          5       // both 2D and 3D
// intermediate data definitions
#define MINPHASES       16
#define MAXPHASES       256
#define O_PHASES        128
#define MAX_PHASERECOMP 1   // recompute phases at each iteration
#define O_PHASERECOMP   MAX_PHASERECOMP
#define MINBANDS        64
#define MAXBANDS        128
#define O_BANDS         MINBANDS
#define O_TSAMPLING     50.e-6
#define MAXTIMEOBS      600
#define O_TIMEOBS       60
#define MAX_NREBIN      10 // 1 + max real rebin !!!
#define MAXPREBIN       (1 << (MAX_NREBIN - 1))
#define O_PREBIN        1
/* the maximum period without rebin is 128*(TSAMPLINGi*2) (with 64 usec is 4ms).
 * with a rebin 8 we can fold periods up to 2^8*4ms = 1s 
 */
//#define MAX_PERIOD      0.049
//#define MAX_PERIOD      0.5
#define MAX_PERIOD      2.0

#define K_DM            4.1493775933609e15      // dispersion constant (Hz^2 pc ^ (-1) cm ^3 s)


#endif
